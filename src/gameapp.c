#include "gameapp.h"
#include "gameplay.h"
#include "raymath.h"
#include "utils.h"
#include "string.h"
#include "stdbool.h"
#include "time.h"
#include "stdlib.h"
#include "stdio.h"
#include "math.h"
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

AppState_t app = {
	.configPath = "options.txt"
};
GameState_t game;

#ifndef DEDICATED_SERVER
	ClientState_t client = {0};
	static const bool isDedicatedServer = false;
#else
	static const bool isDedicatedServer = true;
#endif

static void DefaultSettings(){
	app.config = (AppConfig_t){
		.fullscreen = 0,
		.vsync = 0,
		.resolution = {1024, 720},
		.windowPos = {-1, -1},
		.soundVolume = 0.725f,
		.musicVolume = 0.675f,
        .mouseEdgeSlide = 1,

		.tickrate = DEFAULT_TICKRATE
	};
}

static void* FindCfgKey(char* cfg, size_t len, const char* key){
	size_t keyLen = strlen(key);
	size_t p = 0;
	while(p < len){
		if(!strncmp(key, cfg, keyLen)){
			while(p < len){
				p++; cfg++;
				if(cfg[-1] == '=') break;
			}
			size_t valLen = 0;
			while((p + valLen) < len){
				valLen++;
				if(cfg[valLen] == 0x0A) break;
				if(cfg[valLen] == 0)    break;
			}
			cfg[valLen] = 0;
			return (void*)cfg;
		}
		while(p < len){
			p++; cfg++;
			if(cfg[-1] == 0x0A) break;
			if(cfg[-1] == 0)   break;
		}
	}
	return NULL;
}

static void GetCfgOption_i(char* cfg, int len, const char* key, int* out){
	void* val = FindCfgKey(cfg, len, key);
	if(val) *out = atoi(val);
}

void LoadSettings(){
	DefaultSettings();
	
	FILE* file = fopen(app.configPath, "r");
	if(file){
		char cfg[8192];
		size_t len = fread(cfg, 1, sizeof(cfg)-1, file);
		cfg[len] = 0;
		fclose(file);

		GetCfgOption_i(cfg, len, "fullscreen", &app.config.fullscreen);
		GetCfgOption_i(cfg, len, "vsync",      &app.config.vsync);
		GetCfgOption_i(cfg, len, "width",      &app.config.resolution[0]);
		GetCfgOption_i(cfg, len, "height",     &app.config.resolution[1]);
		GetCfgOption_i(cfg, len, "windowpos_x", &app.config.windowPos[0]);
		GetCfgOption_i(cfg, len, "windowpos_y", &app.config.windowPos[1]);
        GetCfgOption_i(cfg, len, "edgeslide",   &app.config.mouseEdgeSlide);
		int volume = 70;
		GetCfgOption_i(cfg, len, "sound_volume", &volume);
		app.config.soundVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "music_volume", &volume);
		app.config.musicVolume = volume/100.0f;
	}
}

static void WriteCfgOption_i(const char* key, int value, FILE* file){
	char line[1024];
	int len = sprintf(line, "%s=%i\n", key, value);
	fwrite(line, 1, len, file);
}

void SaveSettings(){
	FILE* file = fopen(app.configPath, "w");
	if(!app.config.fullscreen){
		Vector2 windowPos = GetWindowPosition();
		app.config.windowPos[0] = (int)windowPos.x;
		app.config.windowPos[1] = (int)windowPos.y;
		app.config.resolution[0] = GetScreenWidth();
		app.config.resolution[1] = GetScreenHeight();
	}
	if(file){
		WriteCfgOption_i("fullscreen", app.config.fullscreen,    file);
		WriteCfgOption_i("vsync",      app.config.vsync,         file);
		WriteCfgOption_i("width",      app.config.resolution[0], file);
		WriteCfgOption_i("height",     app.config.resolution[1], file);
		WriteCfgOption_i("windowpos_x", app.config.windowPos[0], file);
		WriteCfgOption_i("windowpos_y", app.config.windowPos[1], file);
        WriteCfgOption_i("edgeslide",   app.config.mouseEdgeSlide, file);
		WriteCfgOption_i("sound_volume", (int)(app.config.soundVolume*100), file);
		WriteCfgOption_i("music_volume", (int)(app.config.musicVolume*100), file);
		fclose(file);
	}
}

void InitializeApp(){
	#ifndef __EMSCRIPTEN__
		LoadSettings();
	#else
		DefaultSettings();
	#endif

	#ifndef DEDICATED_SERVER
		SetConfigFlags(FLAG_WINDOW_RESIZABLE);
		if(app.config.fullscreen) SetConfigFlags(FLAG_FULLSCREEN_MODE);
		if(app.config.vsync) SetConfigFlags(FLAG_VSYNC_HINT);
		InitWindow(app.config.resolution[0], app.config.resolution[1], GAME_TITLE);
		SetExitKey(0);
		SetWindowMinSize(700, 700);
		if((!app.config.fullscreen) && (app.config.windowPos[0] != -1)){
			SetWindowPosition(app.config.windowPos[0], app.config.windowPos[1]);
		}
		HideCursor();
		InitAudioDevice();
		client.cam.zoom = 2;
		client.cam.target = (Vector2){16 * TILE_SIZE, 16 * TILE_SIZE};
	#endif

	StartGame();

	if(1){
		printf("\n game state: %i kb. app state: %i kb.", sizeof(game)/1024, sizeof(app)/1024);
		#ifndef DEDICATED_SERVER
			printf(" client state: %i kb.", sizeof(client)/1024);
		#endif
		printf("\n\n");
	}
}

static void GameTick(){
	if(app.shouldRestartRound){
		ResetGame();
		app.shouldRestartRound = 0;
	}
	UpdateBuildings();
	UpdatePlayers();
	UpdateUnits();
	GameplayLogicTick();
	game.frame++;
}

static void DrawGraphics(){
	#ifndef DEDICATED_SERVER
		client.width = GetScreenWidth();
		client.height = GetScreenHeight();
		client.halfWidth = client.width / 2;
		client.halfHeight = client.height / 2;
		client.cam.offset.x = 0;
		client.cam.offset.y = 0;
		client.appTime = (float)GetTime();
		client.deltaTime = GetFrameTime();

		Vector2 topLeft = GetScreenToWorld2D(Vector2Zero(), client.cam);
		client.viewBounds[0] = (int)(topLeft.x / TILE_SIZE) - 2;
		client.viewBounds[1] = (int)(topLeft.y / TILE_SIZE) - 2;
		client.viewBounds[2] = (int)ceilf(client.width / (float)TILE_SIZE / client.cam.zoom) + 4;
		client.viewBounds[3] = (int)ceilf(client.height / (float)TILE_SIZE / client.cam.zoom) + 4;

		BeginDrawing();
		ClearBackground(BLACK);
		BeginMode2D(client.cam);
		DrawWorld();
		DrawBuildings();
		DrawUnits();
		UpdateParticles();
		DrawParticles();
		EndMode2D();
		DrawUI();
		Vector2 mousePos = GetMousePosition();
		DrawTileEx(726, (Vector2){mousePos.x+1, mousePos.y+1}, 0, 2, (Color){0,0,0, 100});
		DrawTileEx(726, mousePos, 0, 2, WHITE);
		EndDrawing();
	#endif
}

static void UpdateGame(){
	static unsigned int timeLast;
	unsigned int frameBudget;
	unsigned int timeNow;
	unsigned int timeDiff;
	
	DrawGraphics();

	frameBudget = (1000 / GET_GAME_TICKRATE());
	timeNow = Millisecs();
	if(timeLast){
		timeDiff = timeNow - timeLast;
	}else{
		timeDiff = frameBudget;
	}
	while(timeDiff >= frameBudget){
		GameTick();
		timeDiff -= frameBudget;
		timeLast = timeNow;
		if(timeDiff > 9000) break;
	}
	if(isDedicatedServer || (!app.config.vsync)){
		Delay(1);
	}
}

void MainLoop(){
	#ifdef __EMSCRIPTEN__
		emscripten_set_main_loop(UpdateGame, 0, 0);
	#else
		while(isDedicatedServer || (!WindowShouldClose())){
			UpdateGame();
		}
		if(!isDedicatedServer){
			SaveSettings();
		}
	#endif
}

static void InitializeGameState(){
	memset(&game, 0, sizeof(game));

}

void StartGame(){
	InitializeGameState();
	client.localPlayerId = CreatePlayer(0);
	ResetGame();
}

void ResetGame(){
	game.state = STATE_UNINITIALIZED;
	game.frame = 0;
	game.timer = 0;

	if(game.initializedOnce){
		RemoveBots();
		CloseGame();
		app.gamesPlayed++;
	}else{
		game.initializedOnce = 1;
	}
}

void CloseGame(){
	
	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = GetPlayer(playerId);
		if(!player->active) continue;
		ClearPlayerStats(playerId);
	}

	for(int unitId=0; unitId < MAX_UNITS; unitId++){
		Unit_t* unit = GetUnit(unitId);
		if(unit->active) RemoveUnit(unitId);
	}

	for(int worldId=0; worldId < MAX_WORLDS; worldId++){
		World_t* world = GetWorld(worldId);
		if(world->active){
			for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
				Building_t* building = &world->buildings[buildingId];
				if(building->active){
					building->active = 0;
				}
			}
			world->active = 0;
		}
	}

}


