#pragma once
#include "stdint.h"

//modulo operator, but no negative numbers
int mod2(int a, int b);

//current time in milliseconds
int Millisecs();

//sleep
void Delay(int ms);

typedef uint64_t FileModTime_t;
FileModTime_t FileModificationTime(const char* path);
