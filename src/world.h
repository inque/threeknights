#pragma once
#include "buildings.h"
#include "stdbool.h"
#include "capacities.h"

typedef enum{
	GND_EMPTY,
	GND_DIRT,
	GND_GRASS,
	GND_TREE,
	GND_ROCK,
	GND_FARMLAND,
	GND_WATER,
	GND_WORLD_BORDER,
	GND_ZONE_TRADE_FOOD,
	GND_ZONE_TRADE_ORE,
	GND_ZONE_LEAVE,
	GND_GRAVE,
	GND_RUBBLE
} GroundTileType_e;

typedef struct{
    int active;
	int width, height;
	char ground[MAX_WORLD_SIZE * MAX_WORLD_SIZE];
    Building_t buildings[MAX_BUILDINGS];
} World_t;

int GenerateWorld(int playerId);
void DrawWorld();

bool IsGroundTypeWalkable(GroundTileType_e ground);
int GetBuildingOnTile(World_t* world, int x, int y, bool clear);
int GetUnitOnTile(int worldId, int x, int y);
bool IsTileWalkable(int worldId, int x, int y, bool clear);
bool RectInView(int x, int y, int width, int height);

inline int TileOffset(World_t* world, int x, int y){
	return (x & (MAX_WORLD_SIZE-1)) + (y * MAX_WORLD_SIZE);
}

inline bool TileInBounds(World_t* world, int x, int y){
	return ((x > 0) && (y > 0) && (x < world->width) && (y < world->height));
}

