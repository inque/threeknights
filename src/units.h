#pragma once
#include "pathfinding.h"

#define COIN_PRICE 4

typedef enum{
	UNIT_KING,
	UNIT_PEASANT,
	UNIT_WARRIOR,
    UNIT_KNIGHT,
    UNIT_MAGE,
    UNIT_DRUID,
    UNIT_DEVIL,
    UNIT_MONSTER_SCORPION,
    UNIT_MONSTER_SPIDER,
    UNIT_MONSTER_SKELETON,
	_UNIT_TYPE_COUNT
} UnitType_e;

typedef struct{
	char name[32];
	int walkingDelay;
	int maxHealth;
	int attackDamage;
	int attackCharge;
	int attackMaxDistance;
	int attackMinDistance;
	int attackAccuracy;
    int sight;
	int canConstructBuildings;
	int iconTileId;
    int priceGold;
    char description[128];
} UnitType_t;

typedef enum{
	UNIT_ACTION_WHATEVER,
	UNIT_ACTION_IDLE,
	UNIT_ACTION_WALKING,
	UNIT_ACTION_FORTIFY,
	UNIT_ACTION_ATTACK_UNIT,
	UNIT_ACTION_ATTACK_BUILDING,
    UNIT_ACTION_CAPTURE_UNIT
} UnitActionType_e;

typedef struct{
	UnitActionType_e type;
	union{
		struct{
			int dest[2];
		} walking;
		struct{
			int worldId;
			int buildingId;
		} fortify;
		struct{
			int targetUnitId;
		} attackUnit;
		struct{
			int worldId;
			int buildingId;
		} attackBuilding;
        struct{
            int unitId;
        } captureUnit;
	};
} UnitAction_t;

typedef enum{
	ITEM_NONE,
	ITEM_FRUIT,
	ITEM_RUBY,
	ITEM_COIN
} UnitWearableType_e;

typedef struct{
	int active;
	UnitType_e type;
	int playerId;
	int health;
    int healTimer;
	int worldId;
	int pos[2];
	int orientation[2];
	int fortifiedBuildingId;
    int beingTransportedByUnitId;
    int transportingUnitId;
	UnitAction_t action;
	int attentionTimer;
    int lastCheckedForEnemiesTimer;
	PathNode_t path[MAX_PATHFIND_DEPTH];
	int pathLength;
	int pathProgress;
	int pathDest[2];
	int pathBuildingTimer;
	int walkingDest[2];
	int walkingTimer;
    int walkingObsctructedFrames;
    int obstructingOtherUnit;
	float walkingDelta;
	int isRafted;
	int raftBuildingProgress;
	int homeWorldId;
	int homeBuildingId;
	int isAttacking;
	int attackCharge;
	UnitWearableType_e carryingItem;
	int lastTakenDamageFrame;
	int lastAnimatedHealthValue;
} Unit_t;

extern const UnitType_t UnitTypes[_UNIT_TYPE_COUNT];

int DistanceFromUnitToBuilding2(int unitId, int worldId, int buildingId);
int SpawnUnit(UnitType_e type, int playerId, int worldId, int pos_x, int pos_y);
void RemoveUnit(int unitId);
void UnitTakeDamage(int unitId, int dmg);
void UpdateUnits();
void DrawUnits();
