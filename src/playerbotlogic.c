#include "playerbotlogic.h"
#include "gameapp.h"
#include "stdlib.h"
#include "stdio.h"

void PlayerAI(int playerId){
	Player_t* player = GetPlayer(playerId);
	PlayerBotState_t* ai = &player->ai;
	AI_Perception_t* ownPerc = &ai->perception[playerId];
	World_t* ownWorld = GetWorld(player->ownWorldId);
	if((!player->active) || player->hasLost || (player->ownWorldId == -1)) return;
	
	if(!ai->initialized){
		ai->state = AI_STATE_CALM;
		ai->mainEnemyPlayerId = -1;
		ai->perceptionUpdateTimer = playerId*2;
		ai->tacticsUpdateTimer = playerId*3;
		ai->initialized = 1;

		// ai->state = AI_STATE_ATTACK;
		// ai->mainEnemyPlayerId = 0;
	}

	//research technologies if available
	for(int techId=0; techId < _TECH_COUNT; techId++){
		if(MayPlayerResearch(playerId, techId)){
			player->command.type = COM_RESEARCH;
			player->command.research.techId = techId;
			return;
			//printf("Player %i is researching %i.\n", playerId, techId);
		}
	}

	//========= update tactics ==============================

	if(ai->tacticsUpdateTimer){
		ai->tacticsUpdateTimer--;
	}else{
		ai->tacticsUpdateTimer = 2*10;

		int peasantCount = 0;
		int unitsAbroadCount = 0;
		for(int unitId=0; unitId < MAX_UNITS; unitId++){
			Unit_t* unit = GetUnit(unitId);
			if(!unit->active) continue;
			if(unit->playerId != playerId) continue;

			if((unit->worldId != player->ownWorldId) || ((unit->action.type == UNIT_ACTION_WALKING) && (unit->action.walking.dest[1] >= (ownWorld->height-3)))){
				unitsAbroadCount++;
			}

			// if(unit->type == UNIT_KING){
			// 	if((unit->fortifiedBuildingId == -1) && (unit->action.type == UNIT_ACTION_WHATEVER)){
			// 		int bestFoundBuildingId=-1;
			// 		int bestFoundBUildingDist;
			// 		for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
			// 			Building_t* building = &ownWorld->buildings[buildingId];
			// 			BuildingType_t* buildingType = &BuildingTypes[building->type];
			// 			if((!building->active) || (!building->isBuilt)) continue;
			// 			if(building->playerId != playerId) continue;
			// 			if(!buildingType->fortificationLimit) continue;

			// 			int dist = DistanceFromUnitToBuilding2(unitId, player->ownWorldId, buildingId);
			// 			if((bestFoundBuildingId == -1) || (dist < bestFoundBUildingDist)){
			// 				bestFoundBuildingId = buildingId;
			// 				bestFoundBUildingDist = dist;
			// 			}
			// 		}
			// 		if(bestFoundBuildingId != -1){
			// 			Building_t* building = &ownWorld->buildings[bestFoundBuildingId];
			// 			player->command.type = COM_AI_MOVE_UNIT;
			// 			player->command.aiMoveUnit.worldId = player->ownWorldId;
			// 			player->command.aiMoveUnit.softly = 0;
			// 			player->command.aiMoveUnit.unitId = unitId;
			// 			player->command.aiMoveUnit.dest[0] = building->pos[0];
			// 			player->command.aiMoveUnit.dest[1] = building->pos[1];
			// 		}
			// 	}
			// }

			if(unit->type == UNIT_PEASANT) peasantCount++;
		}

		switch(ai->state){
			case AI_STATE_DEFENSE: {
				int trespassing = 0;
				for(int i=0; i < MAX_PLAYERS; i++){
					trespassing += ai->perception[i].trespassing;
				}
				if(trespassing < 20){
					ai->state = AI_STATE_CALM;
				}

				if(ai->mainEnemyPlayerId != -1){
					AI_Perception_t* perc = &ai->perception[ai->mainEnemyPlayerId];
					if(ownPerc->attackPower > perc->defensePower){
						ai->state = AI_STATE_ATTACK;
					}else{
						ai->state = AI_STATE_CALM;
						ai->mainEnemyPlayerId = -1;
					}
				}
			} break;
			case AI_STATE_ATTACK: {
				Player_t* mainEnemy = GetPlayer(ai->mainEnemyPlayerId);
				if((ai->mainEnemyPlayerId == -1) || (!mainEnemy->active) || (mainEnemy->hasLost)){
					ai->state = AI_STATE_CALM;
					break;
				}

				if(player->stats.unitsInDeckTotal){
					for(int i=0; i < _UNIT_TYPE_COUNT; i++){
						player->command.type = COM_SUMMON_FROM_DECK;
						player->command.summonFromDeck.unitType = i;
						player->command.summonFromDeck.worldId = mainEnemy->ownWorldId;
						return;
					}
				}

				if(unitsAbroadCount < (int)(player->stats.unitCount * 0.625f)){
					for(int unitId=0; unitId < MAX_UNITS; unitId++){
						Unit_t* unit = GetUnit(unitId);
						UnitType_t* unitType = &UnitTypes[unit->type];
						if(!unit->active) continue;
						if(unit->playerId != playerId) continue;
						if(!unitType->attackDamage) continue;
						if(unit->action.type != UNIT_ACTION_WHATEVER) continue;

						if(unit->worldId == player->ownWorldId){
							player->command.type = COM_AI_MOVE_UNIT;
							player->command.aiMoveUnit.softly = 0;
							player->command.aiMoveUnit.worldId = player->ownWorldId;
							player->command.aiMoveUnit.unitId = unitId;
							player->command.aiMoveUnit.dest[0] = unit->pos[0];
							player->command.aiMoveUnit.dest[1] = ownWorld->height-2;
							return;
						}else if(unit->worldId == mainEnemy->ownWorldId){
							if(!(rand()&7)){
								//find main enemy's king
								for(int otherUnitId=0; otherUnitId < MAX_UNITS; otherUnitId++){
									Unit_t* otherUnit = GetUnit(otherUnitId);
									if(!otherUnit->active) continue;
									if(otherUnit->playerId != ai->mainEnemyPlayerId) continue;
									if(otherUnit->type != UNIT_KING) continue;
									if(otherUnit->worldId != unit->worldId) continue;

									player->command.type = COM_AI_MOVE_UNIT;
									player->command.aiMoveUnit.softly = 1;
									player->command.aiMoveUnit.worldId = unit->worldId;
									player->command.aiMoveUnit.unitId = unitId;
									player->command.aiMoveUnit.dest[0] = otherUnit->pos[0];
									player->command.aiMoveUnit.dest[1] = otherUnit->pos[1];
									return;
								}
							}
						}
					}
				}

			} break;
		}

		int underConstructionCount = 0;
		for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
			Building_t* building = &ownWorld->buildings[buildingId];
			if(!building->active) continue;
			if(building->playerId != playerId) continue;

			if(!building->isBuilt) underConstructionCount++;
		}

		int playersCounted = 0;
		int averageAttackPower = 0;
		int averageDefensePower = 0;
		for(int otherPlayerId=0; otherPlayerId < MAX_PLAYERS; otherPlayerId++){
			Player_t* otherPlayer = GetPlayer(otherPlayerId);
			AI_Perception_t* perc = &ai->perception[otherPlayerId];
			if(!otherPlayer->active) continue;

			if(otherPlayerId != playerId){
				averageAttackPower += perc->attackPower;
				averageDefensePower += perc->defensePower;
				playersCounted++;
			}
		}
		if(playersCounted){
			averageAttackPower /= playersCounted;
			averageDefensePower /= playersCounted;
		}

		bool needMoreDefense = (ai->state == AI_STATE_DEFENSE) || (ownPerc->defensePower < (averageAttackPower+10)) || (ownPerc->defensePower < 150);
		bool needMoreAttack = (ai->state == AI_STATE_ATTACK) || (ownPerc->attackPower < (averageDefensePower+10)) || (ownPerc->attackPower < 100);
		int foodSurplus = player->stats.food - peasantCount;
		int foodSurplusMax = 1;
		if(needMoreDefense) foodSurplusMax += 1;
		if(needMoreAttack) foodSurplusMax += 1;
		bool needMoreFood = (foodSurplus < foodSurplusMax);
		
		int shouldConstructBuildingOfType = -1;
		bool shouldConstructBuildingOnBottomHalf = false;
		bool shouldHireAttackUnit = false;
		int shouldHireUnitOfType = -1;
		bool needMoreGold = false;

		if(needMoreFood || (player->stats.buildingCountByTypeCountingUnbuilt[BUILDING_HOUSE] < 5)){
			shouldConstructBuildingOfType = BUILDING_HOUSE;
		}else if(needMoreDefense){
			if(!(rand()&3)){
				shouldConstructBuildingOfType = (player->stats.technologiesResearched[TECH_DEFENSIVE_TACTICS]) ? BUILDING_TOWER : BUILDING_OUTPOST;
				shouldConstructBuildingOnBottomHalf = true;
			}else{
				shouldHireAttackUnit = true;
			}
		}else if(needMoreAttack){
			shouldHireAttackUnit = true;
		}

		if(peasantCount > 4){
			if(shouldHireAttackUnit){
				if(player->stats.buildingCountByType[BUILDING_BARRACKS] && player->stats.technologiesResearched[TECH_IRON_ARMOR] && (rand()&3)){
					shouldHireUnitOfType = UNIT_KNIGHT;
				}else if(player->stats.buildingCountByType[BUILDING_ORACLE] && (rand()&3)){
					shouldHireUnitOfType = UNIT_DRUID;
				}else if(player->stats.buildingCountByType[BUILDING_BARRACKS] && (rand()&3)){
					shouldHireUnitOfType = UNIT_WARRIOR;
				}else if(player->stats.buildingCountByType[BUILDING_ACADEMY]){
					shouldHireUnitOfType = UNIT_MAGE;
				}
			}
			if(shouldHireUnitOfType == -1){
				if((!player->stats.buildingCountByTypeCountingUnbuilt[BUILDING_BARRACKS]) && (rand()&1)){
					shouldConstructBuildingOfType = BUILDING_BARRACKS;
				}else if(!player->stats.buildingCountByTypeCountingUnbuilt[BUILDING_ACADEMY] && (rand()&1)){
					shouldConstructBuildingOfType = BUILDING_ACADEMY;
				}else if(!player->stats.buildingCountByTypeCountingUnbuilt[BUILDING_WORKSHOP] && (rand()&1)){
					shouldConstructBuildingOfType = BUILDING_WORKSHOP;
				}else if((!player->stats.buildingCountByTypeCountingUnbuilt[BUILDING_ORACLE]) && (player->stats.technologiesResearched[TECH_ADVANCED_MAGIC])){
					shouldConstructBuildingOfType = BUILDING_ORACLE;
				}
			}else{
				UnitType_t* unitType = &UnitTypes[shouldHireUnitOfType];
				if(player->stats.gold < unitType->priceGold){
					needMoreGold = true;
				}else{
					player->command.type = COM_HIRE;
					player->command.hire.worldId = -1;
					player->command.hire.buildingId = -1;
					player->command.hire.unitType = shouldHireUnitOfType;
					//printf("Player %i decided to hire %i.\n", playerId, shouldHireUnitOfType);
					return;
				}
			}
		}

		if((player->stats.gold < 40) && (foodSurplus > 10) && (!(rand()&31))){
			shouldConstructBuildingOfType = BUILDING_MINE;
			shouldConstructBuildingOnBottomHalf = true;
		}

		if((player->stats.buildingCountByType[BUILDING_HOUSE] >= 3) && (!player->stats.buildingCountByTypeCountingUnbuilt[BUILDING_BARRACKS])){
			shouldConstructBuildingOfType = BUILDING_BARRACKS;
			shouldConstructBuildingOnBottomHalf = false;
		}

		if((shouldConstructBuildingOfType != -1) && (underConstructionCount < 1+(peasantCount / 10))){
			BuildingType_t* buildingType = &BuildingTypes[shouldConstructBuildingOfType];
			if(player->stats.gold < buildingType->priceGold){
				needMoreGold = true;
			}else{
				player->command.type = COM_BUILD;
				player->command.build.buildingType = shouldConstructBuildingOfType;
				player->command.build.worldId = player->ownWorldId;
				player->command.build.pos[0] = (rand() % ownWorld->width);
				if(shouldConstructBuildingOnBottomHalf){
					player->command.build.pos[1] = (ownWorld->height/3) + (rand() % (ownWorld->height - ownWorld->height/3 - 20));
				}else{
					player->command.build.pos[1] = (rand() % (ownWorld->height/3) + 4);
				}
				//printf("Player %i decided to build %i.\n", playerId, shouldConstructBuildingOfType);
				//printf("surplus: %i. need defense: %i. need attack: %i\n", foodSurplus, needMoreDefense, needMoreAttack);
				return;
			}
		}

		//make sure peasants are busy making money
		// for(int unitId=0; unitId < MAX_UNITS; unitId++){
		// }
		
	}

	//====== update perception of other players ==============	

	if(ai->perceptionUpdateTimer){
		ai->perceptionUpdateTimer--;
	}else{
		ai->perceptionUpdateTimer = 8*10;

		for(int otherPlayerId=0; otherPlayerId < MAX_PLAYERS; otherPlayerId++){
			Player_t* otherPlayer = GetPlayer(otherPlayerId);
			AI_Perception_t* perc = &ai->perception[otherPlayerId];
			World_t* otherPlayerWorld = GetWorld(otherPlayer->ownWorldId);
			if((!otherPlayer->active) || otherPlayer->hasLost) continue;

			perc->attackPower = 0;
			perc->defensePower = 0;
			if(perc->trespassing > 2) perc->trespassing -= 2;

			for(int unitId=0; unitId < MAX_UNITS; unitId++){
				Unit_t* unit = GetUnit(unitId);
				UnitType_t* unitType = &UnitTypes[unit->type];
				if(!unit->active) continue;
				if(unit->playerId != otherPlayerId) continue;

				int unitPower = unitType->attackDamage;
				if(unitPower) unitPower += (unit->health / 20);
				
				perc->attackPower += unitPower;
				if(unit->worldId == otherPlayer->ownWorldId){
					perc->defensePower += unitPower;
				}else if(unit->worldId == player->ownWorldId){
					perc->trespassing += (unit->pos[1] < (ownWorld->height - (ownWorld->height / 4))) ? 4 : 1;
				}
			}

			if(otherPlayer->ownWorldId != -1){
				for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
					Building_t* building = &otherPlayerWorld->buildings[buildingId];
					BuildingType_t* buildingType = &BuildingTypes[building->type];
					if(!building->active) continue;

					int buildingPower = buildingType->attackDamage + (building->health / 20);
					if(!building->isBuilt) buildingPower /= 4;
					perc->defensePower += buildingPower;
				}
			}

			if(perc->trespassing > 45){
				if(perc->attackPower > perc->defensePower){
					if(ai->mainEnemyPlayerId == -1){
						ai->state = AI_STATE_DEFENSE;
					}
				}
			}
			if(perc->trespassing > 70){
				perc->trespassing = 70;
				if(ai->mainEnemyPlayerId == -1){
					perc->relation = RELATION_ENEMY;
					player->stats.autoAttackPlayer[otherPlayerId] = 1;
				}
			}

		}

	}

}