#include "units.h"
#include "gameapp.h"
#include "string.h"
#include "pathfinding.h"
#include "stdlib.h"

const UnitType_t UnitTypes[] = {
	[UNIT_KING] = {
		.name = "The King",
		.walkingDelay = 5,
		.maxHealth = 100,
		.iconTileId = 779,
		.sight = 20,
		.description = "\n King. \n (that is, you)"
	},
	[UNIT_PEASANT] = {
		.name = "Peasant",
		.walkingDelay = 3,
		.maxHealth = 40,
		.canConstructBuildings = 1,
		.iconTileId = 344,
		.sight = 20,
		.description = "\n Peasant. \n Constructs buildings. \n Earns you money. \n Convertible to other units."
	},
	[UNIT_WARRIOR] = {
		.name = "Warrior",
		.walkingDelay = 4,
		.maxHealth = 60,
		.attackDamage = 8,
		.attackCharge = 8*CHARGE_MULTIPLIER,
		.attackMinDistance = 1,
		.attackMaxDistance = 1,
		.attackAccuracy = 80,
		.iconTileId = 706,
		.priceGold = 30,
		.sight = 18,
		.description = "\n Warrior. \n -30 gold. \n (requires BARRACKS)"
	},
	[UNIT_KNIGHT] = {
		.name = "Knight",
		.walkingDelay = 5,
		.maxHealth = 80,
		.attackDamage = 12,
		.attackCharge = 9*CHARGE_MULTIPLIER,
		.attackMinDistance = 1,
		.attackMaxDistance = 1,
		.attackAccuracy = 75,
		.iconTileId = 708,
		.priceGold = 50,
		.sight = 18,
		.description = "\n Knight. \n -50 gold. \n (requires BARRACKS and 'IRON ARMOR')"
	},
	[UNIT_MAGE] = {
		.name = "Mage",
		.walkingDelay = 4,
		.maxHealth = 40,
		.attackDamage = 5,
		.attackCharge = 14*CHARGE_MULTIPLIER,
		.attackMinDistance = 2,
		.attackMaxDistance = 8,
		.attackAccuracy = 72,
		.iconTileId = 832,
		.priceGold = 35,
		.sight = 22,
		.description = "\n Mage. \n -35 gold. \n Casts fireballs from a distance. \n (requires ACADEMY)"
	},
	[UNIT_DRUID] = {
		.name = "Druid",
		.walkingDelay = 4,
		.maxHealth = 40,
		.attackDamage = 6,
		.attackCharge = 8*CHARGE_MULTIPLIER,
		.attackMinDistance = 2,
		.attackMaxDistance = 9,
		.attackAccuracy = 60,
		.iconTileId = 835,
		.priceGold = 50,
		.sight = 22,
		.description = "\n Druid. \n -50 gold. \n Can move very fast in the forest. \n (requires ORACLE'S HUT)"
	},
	[UNIT_DEVIL] = {
		.name = "Devil",
		.walkingDelay = 4,
		.maxHealth = 666,
		.attackDamage = 10,
		.attackCharge = 3*CHARGE_MULTIPLIER,
		.attackMinDistance = 1,
		.attackMaxDistance = 6,
		.attackAccuracy = 90,
		.iconTileId = 91,
		.sight = 45,
		.description = "\n The devil. \n (that is, you)"
	},
	[UNIT_MONSTER_SCORPION] = {
		.name = "Scorpion",
		.walkingDelay = 3,
		.maxHealth = 25,
		.attackDamage = 2,
		.attackCharge = 4*CHARGE_MULTIPLIER,
		.attackMinDistance = 1,
		.attackMaxDistance = 2,
		.attackAccuracy = 75,
		.iconTileId = 184,
		.sight = 18,
		.description = ""
	},
	[UNIT_MONSTER_SCORPION] = {
		.name = "Spider",
		.walkingDelay = 2,
		.maxHealth = 15,
		.attackDamage = 3,
		.attackCharge = 6*CHARGE_MULTIPLIER,
		.attackMinDistance = 1,
		.attackMaxDistance = 1,
		.attackAccuracy = 70,
		.iconTileId = 188,
		.sight = 18,
		.description = ""
	},
	[UNIT_MONSTER_SKELETON] = {
		.name = "Skeleton",
		.walkingDelay = 10,
		.maxHealth = 30,
		.attackDamage = 40,
		.attackCharge = 25*CHARGE_MULTIPLIER,
		.attackMinDistance = 1,
		.attackMaxDistance = 1,
		.attackAccuracy = 85,
		.iconTileId = 221,
		.sight = 18,
		.description = ""
	},
};

int SpawnUnit(UnitType_e type, int playerId, int worldId, int pos_x, int pos_y){
	const UnitType_t* unitType = &UnitTypes[type];
	for(int unitId=0; unitId < MAX_UNITS; unitId++){
		Unit_t* unit = GetUnit(unitId);
		if(unit->active) continue;

		unit->active = 1;
		unit->type = type;
		unit->playerId = playerId;
		unit->health = unitType->maxHealth;
		unit->lastAnimatedHealthValue = unit->health;
		unit->healTimer = 0;
		unit->worldId = worldId;
		unit->pos[0] = pos_x;
		unit->pos[1] = pos_y;
		unit->pathLength = 0;
		unit->pathProgress = 0;
		unit->pathBuildingTimer = PATHFIND_DELAY;
		unit->attentionTimer = 0;
		unit->lastCheckedForEnemiesTimer = 0;
		unit->walkingDest[0] = pos_x;
		unit->walkingDest[1] = pos_y;
		unit->walkingObsctructedFrames = 0;
		unit->obstructingOtherUnit = 0;
		unit->orientation[0] = 0;
		unit->orientation[1] = 1;
		unit->fortifiedBuildingId = -1;
		unit->beingTransportedByUnitId = -1;
		unit->transportingUnitId = -1;
		unit->action.type = UNIT_ACTION_WHATEVER;
		unit->walkingTimer = 0;
		unit->walkingDelta = 0;
		unit->isRafted = 0;
		unit->raftBuildingProgress = 0;
		unit->homeWorldId = worldId;
		unit->homeBuildingId = -1;
		unit->carryingItem = ITEM_NONE;
		unit->isAttacking = 0;
		unit->attackCharge = 0;
		unit->lastTakenDamageFrame = 0;
		if(playerId != -1){
			Player_t* player = GetPlayer(playerId);
			player->stats.unitCount++;
		}
		return unitId;
	}
	return -1;
}

void RemoveUnit(int unitId){
	Unit_t* unit = GetUnit(unitId);
	if(unit->fortifiedBuildingId != -1){
		World_t* world = GetWorld(unit->worldId);
		Building_t* building = &world->buildings[unit->fortifiedBuildingId];
		building->unitsFortified--;
	}
	if(unit->playerId != -1){
		Player_t* player = GetPlayer(unit->playerId);
		player->stats.unitCount--;
	}
	unit->active = 0;
}

void UnitTakeDamage(int unitId, int dmg){
	Unit_t* unit = GetUnit(unitId);
	if(!unit->active) return;

	SpawnParticles(EFFECT_DAMAGE, unit->worldId, (Vector2){(0.5f+unit->pos[0])*TILE_SIZE, (0.5f+unit->pos[1])*TILE_SIZE}, Vector2Zero(), dmg);
	if(unit->type != UNIT_KING){
		unit->health -= dmg;
	}

	if(unit->type == UNIT_DEVIL){
		Player_t* player = GetPlayer(game.devilPlayerId);
		if(player->stats.minionCount){
			player->stats.minionCount--;
			SpawnUnit(UNIT_MONSTER_SCORPION+(rand()%3), game.devilPlayerId, unit->worldId, unit->pos[0], unit->pos[1]);
		}
		if(unit->health <= 0){
			player->hasLost;
		}
	}

	if(unit->health <= 0){
		if((unit->type == UNIT_KING) && (unit->playerId != -1)){
			Player_t* player = GetPlayer(unit->playerId);
			if(unit->playerId == game.devilPlayerId){
				RegisterDevilReveal();
				return;		
			}else{
				player->hasLost = 1;
			}
		}
		RemoveUnit(unitId);
		if(unit->worldId != -1){
			World_t* world = GetWorld(unit->worldId);
			int offset = TileOffset(world, unit->pos[0], unit->pos[1]);
			if(IsGroundTypeWalkable(world->ground[offset])){
				world->ground[offset] = GND_GRAVE;
			}
		}
	}else{
		unit->lastTakenDamageFrame = game.frame;
	}
}

static int FindHomeCastle(Unit_t* unit){
	World_t* world = GetWorld(unit->homeWorldId);
	for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
		Building_t* building = &world->buildings[buildingId];
		if(building->active && (building->type == BUILDING_CASTLE) && (building->playerId == unit->playerId)){
			return buildingId;
		}
	}
	return -1;
}

static int DistanceFromUnitToBuilding(Unit_t* unit, Building_t* building){
	int diff[] = {
		unit->pos[0] - building->pos[0],
		unit->pos[1] - building->pos[1]
	};
	int dist[] = {
		abs((diff[0] <= 0) ? diff[0] : (unit->pos[0] - (building->pos[0] + building->size[0]-1))),
		abs((diff[1] <= 0) ? diff[1] : (unit->pos[1] - (building->pos[1] + building->size[1]-1)))
	};
	return dist[0]+dist[1];
}

int DistanceFromUnitToBuilding2(int unitId, int worldId, int buildingId){
	World_t* world = GetWorld(worldId);
	return DistanceFromUnitToBuilding(GetUnit(unitId), &world->buildings[buildingId]);
}


static bool IsUnitTouchingBuilding(Unit_t* unit, Building_t* building){
	bool aabb = (
		   ((unit->pos[0]+2) > building->pos[0])
		&& ((unit->pos[1]+2) > building->pos[1])
		&& ((unit->pos[0]-1) < (building->pos[0] + building->size[0]))
		&& ((unit->pos[1]-1) < (building->pos[1] + building->size[1]))
	);
	if(!aabb) return false;
	if(building->pixmap){
		//todo: non-solid cells
	}
	return true;
}

void UpdateUnits(){
	for(int unitId=0; unitId < MAX_UNITS; unitId++){
		Unit_t* unit = GetUnit(unitId);
		if(!unit->active) continue;
		UnitType_t* unitType = &UnitTypes[unit->type];
		World_t* world = GetWorld(unit->worldId);
		
		if(unit->health < unitType->maxHealth){
			if(unit->healTimer){
				unit->healTimer--;
			}else{
				unit->healTimer = (unit->fortifiedBuildingId != -1) ? 25 : 50;
				unit->health++;
			}
		}

		
		if(unit->transportingUnitId != -1){
			//mechanics of carrying the king
			Unit_t* targetUnit = GetUnit(unit->transportingUnitId);
			if(!targetUnit->active){
				unit->transportingUnitId = -1;
			}else if((targetUnit->type == UNIT_KING) && (unit->playerId != -1)){
				Player_t* player = GetPlayer(unit->playerId);
				World_t* ownWorld = GetWorld(player->ownWorldId);
				if(unit->worldId == -1){
					//overworld, go to target world
					player->stats.unitsInDeckCount[unit->type]--;
					player->stats.unitsInDeckTotal--;
					unit->worldId = player->ownWorldId;
					targetUnit->worldId = unit->worldId;
					unit->pos[0] = 2+(rand() % (ownWorld->width-4));
					unit->pos[1] = ownWorld->height-5+(rand()&3);
					targetUnit->pos[0] = unit->pos[0];
					targetUnit->pos[1] = unit->pos[1];
				}else if(unit->worldId == player->ownWorldId){
					//own world, set king on fire
					if(!(game.frame&31)){
						for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
							Building_t* building = &ownWorld->buildings[buildingId];
							if(!building->active) continue;
							if(building->type != BUILDING_BONFIRE) continue;

							unit->action.type = UNIT_ACTION_FORTIFY;
							unit->action.fortify.worldId = player->ownWorldId;
							unit->action.fortify.buildingId = buildingId;
							break;
						}
					}
				}else{
					//other world, go to the bottom
					if(!(game.frame&31)){
						if(unit->action.type != UNIT_ACTION_WALKING){
							unit->action.type = UNIT_ACTION_WALKING;
							unit->action.walking.dest[1] = world->height-2;
						}
					}
				}
			}
		}
		if(unit->worldId == -1){
			//..
			continue;
		}
		if(unit->beingTransportedByUnitId != -1){
			Unit_t* carrier = GetUnit(unit->beingTransportedByUnitId);
			if(!carrier->active){
				unit->beingTransportedByUnitId = -1;
			}else{
				unit->pos[0] = carrier->pos[0];
				unit->pos[1] = carrier->pos[1];
				unit->worldId = carrier->worldId;
			}
			continue;
		}

		//special zones
		GroundTileType_e groundUnderUnit = world->ground[TileOffset(world, unit->pos[0], unit->pos[1])];
		if(groundUnderUnit == GND_ZONE_TRADE_FOOD){
			if(unit->carryingItem == ITEM_FRUIT){
				unit->carryingItem = ITEM_COIN;
				int castleId = FindHomeCastle(unit);
				if(castleId != -1){
					unit->action.type = UNIT_ACTION_FORTIFY;
					unit->action.fortify.worldId = unit->homeWorldId;
					unit->action.fortify.buildingId = castleId;
				}
			}
		}else if(groundUnderUnit == GND_ZONE_TRADE_ORE){
			if(unit->carryingItem == ITEM_RUBY){
				unit->carryingItem = ITEM_COIN;
				int castleId = FindHomeCastle(unit);
				if(castleId != -1){
					unit->action.type = UNIT_ACTION_FORTIFY;
					unit->action.fortify.worldId = unit->homeWorldId;
					unit->action.fortify.buildingId = castleId;
				}
			}
		}else if(groundUnderUnit == GND_ZONE_LEAVE){
			if((unit->playerId != -1) && (unit->type != UNIT_KING)){
				Player_t* player = GetPlayer(unit->playerId);
				player->stats.unitsInDeckCount[unit->type]++;
				player->stats.unitsInDeckTotal++;
				unit->worldId = -1;
			}
		}

		//attacking

		if(unit->isAttacking){			
			if(unit->action.type == UNIT_ACTION_ATTACK_UNIT){
				int targetUnitId = unit->action.attackUnit.targetUnitId;
				Unit_t* targetUnit = GetUnit(targetUnitId);
				if((!targetUnit->active) || (targetUnit->worldId != unit->worldId) || (targetUnit->fortifiedBuildingId != -1)){
					unit->isAttacking = 0;
					unit->attackCharge = 0;
				}else if(targetUnit->type == UNIT_KING){
					unit->isAttacking = 0;
					unit->attackCharge = 0;
					unit->action.type = UNIT_ACTION_CAPTURE_UNIT;
					unit->action.captureUnit.unitId = targetUnitId;
				}else{
					int distance = abs(targetUnit->pos[0] - unit->pos[0]) + abs(targetUnit->pos[1] - unit->pos[1]);
					if((distance > unitType->attackMaxDistance) || (distance < unitType->attackMinDistance)){
						unit->isAttacking = 0;
						unit->attackCharge = 0;
					}else{
						if(unit->attackCharge++ > unitType->attackCharge){
							Vector2 pos = {TILE_SIZE*unit->pos[0], TILE_SIZE*unit->pos[1]};
							PlayAudioEffect((unitType->attackMaxDistance > 1) ? SFX_MAGIC : SFX_SWORD, unit->worldId, pos);
							SpawnParticles(EFFECT_ATTACK, targetUnit->worldId, pos, (Vector2){unit->orientation[0], unit->orientation[1]}, 0);
							
							unit->attackCharge = 0;

							bool missed = ((rand()%100) > unitType->attackAccuracy);
							if(!missed){
								RegisterPlayerAttack(unit->playerId, targetUnit->playerId);
								UnitTakeDamage(targetUnitId, unitType->attackDamage);
								if(targetUnit->health <= 0){
									unit->isAttacking = 0;
									unit->action.type = UNIT_ACTION_WHATEVER;
								}
							}
						}
					}
				}
			}else if(unit->action.type == UNIT_ACTION_ATTACK_BUILDING){
				if(unit->worldId != unit->action.attackBuilding.worldId){
					unit->isAttacking = 0;
					unit->attackCharge = 0;
				}else{
					Building_t* building = &world->buildings[unit->action.attackBuilding.buildingId];
					if((!building->active) || (building->type == BUILDING_BONFIRE)){
						unit->isAttacking = 0;
						unit->attackCharge = 0;
					}else{
						int distance = DistanceFromUnitToBuilding(unit, building);
						if((distance > unitType->attackMaxDistance) || (distance < unitType->attackMinDistance)){
							unit->isAttacking = 0;
							unit->attackCharge = 0;
						}else{
							if(unit->attackCharge++ > unitType->attackCharge){
								unit->attackCharge = 0;
								RegisterPlayerAttack(unit->playerId, building->playerId);
								BuildingTakeDamage(unit->action.attackBuilding.worldId, unit->action.attackBuilding.buildingId, unitType->attackDamage);
								Vector2 pos = {TILE_SIZE*unit->pos[0], TILE_SIZE*unit->pos[1]};
								PlayAudioEffect((unitType->attackMaxDistance > 1) ? SFX_MAGIC : SFX_SWORD, unit->worldId, pos);
							}
						}
					}
				}
			}else{
				unit->isAttacking = 0;
				unit->attackCharge = 0;
			}
		}

		//walking

		if(unit->walkingTimer){
			unit->walkingTimer--;
			continue;
		}
		unit->walkingTimer = unitType->walkingDelay;
		if((unit->type == UNIT_DRUID) && (groundUnderUnit == GND_TREE)){
			unit->walkingTimer = 1;
		}

		bool doMove = false;
		int moveDirection[2] = {0, 0};
		int dest[2] = {unit->walkingDest[0], unit->walkingDest[1]};

		switch(unit->action.type){
			case UNIT_ACTION_WHATEVER: {

				if((unit->type == UNIT_PEASANT) && (unit->homeBuildingId == -1)){
					World_t* unitHomeWorld = GetWorld(unit->homeWorldId);
					for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
						Building_t* building = &unitHomeWorld->buildings[buildingId];
						if(!building->active) continue;
						if(building->playerId != unit->playerId) continue;
						if(building->unitsFortified) continue;
						
						if((building->type == BUILDING_HOUSE) || (building->type == BUILDING_MINE)){
							unit->homeBuildingId = buildingId;
							unit->action.type = UNIT_ACTION_FORTIFY;
							unit->action.fortify.worldId = unit->homeWorldId;
							unit->action.fortify.buildingId = buildingId;
							break;
						}
					}

				}

				unit->attentionTimer++;
				if(unit->attentionTimer >= 100){
					unit->attentionTimer = 0;
				}else if(unit->attentionTimer > 50){
					doMove = true;
				}else if(unit->attentionTimer == 50){
					unit->walkingDest[0] = unit->pos[0] + ((rand() % 8) - 4);
					unit->walkingDest[1] = unit->pos[1] + ((rand() % 8) - 4);
				}
			} //break;
			case UNIT_ACTION_IDLE: {
				if(unit->action.type == UNIT_ACTION_IDLE){
					if(!unit->attentionTimer){
						unit->action.type = UNIT_ACTION_WHATEVER;
					}else{
						unit->attentionTimer--;
					}
				}

				if(unit->lastCheckedForEnemiesTimer){
					unit->lastCheckedForEnemiesTimer--;
				}else{
					unit->lastCheckedForEnemiesTimer = 6;
					int bestUnitToAttackId = -1;
					int bestUnitToAttackScore;
					
					for(int otherUnitId=0; otherUnitId < MAX_UNITS; otherUnitId++){
						Unit_t* otherUnit = GetUnit(otherUnitId);
						UnitType_t* otherUnitType = &UnitTypes[otherUnit->type];
						if(!otherUnit->active) continue;
						if(otherUnit->worldId != unit->worldId) continue;
						if(otherUnit->playerId == unit->playerId) continue;
						if(otherUnit->fortifiedBuildingId != -1) continue;
						if((otherUnit->playerId != -1) && (unit->playerId != -1)){
							Player_t* player = GetPlayer(unit->playerId);
							if(!player->stats.autoAttackPlayer[otherUnit->playerId]){
								continue;
							}
						}
						int dist = abs(unit->pos[0] - otherUnit->pos[0]) + abs(unit->pos[1] - otherUnit->pos[1]);
						if(dist > unitType->sight) continue;
						
						if(unitType->attackDamage && (unit->health > otherUnitType->attackDamage*3)){
							int score = dist*2 + (unit->health/10) + (otherUnitType->attackDamage/10);
							if(otherUnit->type == UNIT_KING){
								score -= 10;
								if(dist < 6) score /= 2;  
							};
							if((bestUnitToAttackId == -1) || (score < bestUnitToAttackScore)){
								bestUnitToAttackId = otherUnitId;
								bestUnitToAttackScore = score;
							}
						}else{
							//run somewhere safe?
							for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
								Building_t* building = &world->buildings[buildingId];
								if((!building->active) || (!building->isBuilt)) continue;
								if(building->playerId != unit->playerId) continue;
								
								unit->action.type = UNIT_ACTION_FORTIFY;
								unit->action.fortify.worldId = unit->worldId;
								unit->action.fortify.buildingId = buildingId;
								break;
							}
							if(unit->action.type != UNIT_ACTION_FORTIFY){
								unit->action.type = UNIT_ACTION_WALKING;
								unit->action.walking.dest[0] = unit->pos[0];
								unit->action.walking.dest[1] = world->height-2;
							}
							break;
						}
					}
					if(bestUnitToAttackId != -1){
						unit->action.type = UNIT_ACTION_ATTACK_UNIT;
						unit->action.attackUnit.targetUnitId = bestUnitToAttackId;
					}else if(unitType->attackDamage){
						//look for buildings instead
						int bestFoundBuildingId = -1;
						int bestFoundBuildingScore;
						for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
							Building_t* building = &world->buildings[buildingId];
							if(!building->active) continue;
							if(building->playerId == unit->playerId) continue;
							if((building->playerId != -1) && (unit->playerId != -1)){
								Player_t* player = GetPlayer(unit->playerId);
								if(!player->stats.autoAttackPlayer[building->playerId]){
									continue;
								}
							}
							int dist = DistanceFromUnitToBuilding(unit, building);
							if(dist > unitType->sight) continue;
							BuildingType_t* buildingType = &BuildingTypes[building->type];

							int score = dist + (building->health / 7) - buildingType->attackDamage*2;
							if((bestFoundBuildingId == -1) || (score < bestFoundBuildingScore)){
								bestFoundBuildingId = buildingId;
								bestFoundBuildingScore = score;
							}
						}
						if(bestFoundBuildingId != -1){
							unit->action.type = UNIT_ACTION_ATTACK_BUILDING;
							unit->action.attackBuilding.worldId = unit->worldId;
							unit->action.attackBuilding.buildingId = bestFoundBuildingId;
						}
					}
				}

			} break;
			case UNIT_ACTION_WALKING: {
				if( (dest[0] != unit->action.walking.dest[0])
				 || (dest[1] != unit->action.walking.dest[1]) ){
					memcpy(&unit->walkingDest, &unit->action.walking.dest, sizeof(unit->walkingDest));
					memcpy(&dest, &unit->action.walking.dest, sizeof(dest));
				}

				int dist = abs(unit->pos[0] - dest[0]) + abs(unit->pos[1] - dest[1]);

				if((!dist) || ((dist < 10) && (unit->walkingObsctructedFrames > dist))){
					unit->action.type = UNIT_ACTION_IDLE;
					unit->attentionTimer = 30*10;
				}else{
					doMove = true;
				}
			} break;
			case UNIT_ACTION_FORTIFY: {
				if(unit->action.fortify.worldId != unit->worldId){
					//not supposed to happen
					unit->action.type = UNIT_ACTION_WHATEVER;
					break;
				}
				if((unit->fortifiedBuildingId == -1) || (unit->fortifiedBuildingId != unit->action.fortify.buildingId)){
					Building_t* building = &world->buildings[unit->action.fortify.buildingId];
					if(unit->type == UNIT_PEASANT){
						if((unit->homeWorldId == unit->action.fortify.worldId) && (unit->homeBuildingId == unit->action.fortify.buildingId)){
							if(building->unitsFortified){
								unit->homeBuildingId = -1;
								unit->action.type = UNIT_ACTION_WHATEVER;
								break;
							}
						}
					}
					if(IsUnitTouchingBuilding(unit, building)){
						BuildingType_t* buildingType = &BuildingTypes[building->type];
						if(building->type == BUILDING_BONFIRE){
							if(unit->transportingUnitId != -1){
								Unit_t* targetUnit = GetUnit(unit->transportingUnitId);
								targetUnit->health -= 5;
								UnitTakeDamage(unit->transportingUnitId, 5);
							}
						}
						int closestPoint[] = {unit->pos[0], unit->pos[1]};
						if(closestPoint[0] < building->pos[0]){
							closestPoint[0]++;
						}else if(closestPoint[1] < building->pos[1]){
							closestPoint[1]++;
						}else if(closestPoint[0] >= (building->pos[0] + building->size[0])){
							closestPoint[0]--;
						}else if(closestPoint[1] >= (building->pos[1] + building->size[1])){
							closestPoint[1]--;
						}
						//if(building->isBuilt && ( (!building->pixmap) || (building->pixmap->tileSolid[PixmapOffset(building->pixmap, closestPoint[0]-building->pos[0], closestPoint[1]-building->pos[1])]) ) ){
						if(building->isBuilt){
							if(building->unitsFortified < buildingType->fortificationLimit){
								//actually fortify
								unit->fortifiedBuildingId = unit->action.fortify.buildingId;
								unit->pos[0] = closestPoint[0];
								unit->pos[1] = closestPoint[1];
								building->unitsFortified++;
								if(unit->homeBuildingId == -1){
									if((unit->type != UNIT_PEASANT) || (building->type == BUILDING_HOUSE)){
										unit->homeBuildingId = unit->action.fortify.buildingId;
									}
								}
								if(unit->carryingItem && (unit->playerId != -1)){
									Player_t* player = GetPlayer(unit->playerId);
									if(building->type == BUILDING_CASTLE){
										if(unit->carryingItem == ITEM_COIN){
											unit->carryingItem = ITEM_NONE;
											player->stats.gold += COIN_PRICE;
											if((unit->type == UNIT_PEASANT) && (unit->homeBuildingId != -1)){
												unit->action.type = UNIT_ACTION_FORTIFY;
												unit->action.fortify.worldId = unit->homeWorldId;
												unit->action.fortify.buildingId = unit->homeBuildingId;
											}
										}
									}
								}
								//doMove = true;
							}else{
								unit->action.type = UNIT_ACTION_WHATEVER;
							}
						}else{
							if(unitType->canConstructBuildings){
								BuildingConstruct(unit->action.fortify.worldId, unit->action.fortify.buildingId, 1);
								if(!(building->buildProgress&3)){
									SpawnParticles(EFFECT_ICON, unit->worldId, (Vector2){unit->pos[0]*TILE_SIZE, unit->pos[1]*TILE_SIZE}, Vector2Zero(), 933);
								}
								if(building->isBuilt){
									unit->action.type = UNIT_ACTION_WHATEVER;	
								}
							}else{
								unit->action.type = UNIT_ACTION_WHATEVER;
							}
						}
					}else{
						memcpy(&dest, &building->pos, sizeof(dest));
						if((unit->pos[0] >= building->pos[0]) && (unit->pos[0] < (building->pos[0] + building->size[0]))){
							dest[0] = unit->pos[0];
						}else if((unit->pos[1] >= building->pos[1]) && (unit->pos[1] < (building->pos[1] + building->size[1]))){
							dest[1] = unit->pos[1];
						}

						doMove = true;
					}
				}
			} break;
			case UNIT_ACTION_ATTACK_UNIT: {
				Unit_t* targetUnit = GetUnit(unit->action.attackUnit.targetUnitId);
				if((!targetUnit->active) || (!targetUnit->health)){
					unit->action.type = UNIT_ACTION_WHATEVER;
					break;
				}
				if(unit->worldId == targetUnit->worldId){
					int distance = abs(unit->pos[0]-targetUnit->pos[0])+abs(unit->pos[1]-targetUnit->pos[1]);
					if((distance <= unitType->attackMaxDistance) && (distance >= unitType->attackMinDistance)){
						unit->isAttacking = 1;
					}else{
						dest[0] = targetUnit->pos[0];
						dest[1] = targetUnit->pos[1];
						doMove = true;
					}
				}else{
					unit->action.type = UNIT_ACTION_WALKING;
					unit->action.walking.dest[0] = unit->pos[0];
					unit->action.walking.dest[1] = world->height-2;
				}
			} break;
			case UNIT_ACTION_ATTACK_BUILDING: {
				if(unit->action.attackBuilding.worldId != unit->worldId){
					//not supposed to happen
					unit->action.type = UNIT_ACTION_WHATEVER;
					break;
				}
				Building_t* building = &world->buildings[unit->action.attackBuilding.buildingId];
				if(!building->active){
					unit->action.type = UNIT_ACTION_WHATEVER;
					break;
				}
				int distance = DistanceFromUnitToBuilding(unit, building);
				if((distance <= unitType->attackMaxDistance) && (distance >= unitType->attackMinDistance)){
					unit->isAttacking = 1;
				}else{
					dest[0] = building->pos[0];
					dest[1] = building->pos[1];
					doMove = true;
				}
			} break;
			case UNIT_ACTION_CAPTURE_UNIT: {
				int targetUnitId = unit->action.captureUnit.unitId;
				Unit_t* targetUnit = GetUnit(targetUnitId);
				if((unit->transportingUnitId != -1) || (!targetUnit->active) || (targetUnit->worldId != unit->worldId) || (targetUnit->fortifiedBuildingId != -1)){
					unit->action.type = UNIT_ACTION_WHATEVER;
					break;
				}
				
				int dist = abs(unit->pos[0] - targetUnit->pos[0]) + abs(unit->pos[1] - targetUnit->pos[1]);
				if(dist <= 1){
					RegisterPlayerAttack(unit->playerId, targetUnit->playerId);
					unit->action.type = UNIT_ACTION_WHATEVER;
					targetUnit->beingTransportedByUnitId = unitId;
					unit->transportingUnitId = targetUnitId;
				}else{
					memcpy(&unit->walkingDest, &targetUnit->pos, sizeof(unit->walkingDest));
					doMove = true;
				}
			} break;
		}

		//follow a path
		if( (dest[0] != unit->pos[0]) || (dest[1] != unit->pos[1]) ){
			if(unit->pathLength){
				if((unit->walkingObsctructedFrames < 5) && (unit->pathDest[0] == dest[0]) && (unit->pathDest[1] == dest[1])){
					PathNode_t* node = &unit->path[unit->pathProgress];
					if( (unit->pos[0] == node->x) && (unit->pos[1] == node->y) ){
						node = &unit->path[++unit->pathProgress];
						//if(unit->playerId == 0) printf("progress: %i\n", unit->pathProgress);
					}
					if(unit->pathProgress >= unit->pathLength){
						unit->pathLength = 0;
						unit->pathProgress = 0;
					}
					dest[0] = node->x;
					dest[1] = node->y;
				}else{
					unit->pathLength = 0;
					unit->pathProgress = 0;
				}
			}else{
				if(!unit->pathBuildingTimer){
					if(BuildUnitPath(unitId, dest)){
						memcpy(&unit->pathDest, dest, sizeof(dest));
						dest[0] = unit->path[1].x;
						dest[1] = unit->path[1].y;
						unit->pathProgress = 1;
						//if(unit->playerId == 0) printf("Built new path out of %i nodes.", unit->pathLength);
					}
					
					unit->pathBuildingTimer = PATHFIND_DELAY;
				}else{
					unit->pathBuildingTimer--;
				}
			}
		}

		//determine movement direction
		int diff[2] = {dest[0] - unit->pos[0], dest[1] - unit->pos[1]};
		if(diff[0] != 0) moveDirection[0] = (diff[0] < 0) ? -1 : 1;
		if(diff[1] != 0) moveDirection[1] = (diff[1] < 0) ? -1 : 1;
		if(moveDirection[0] && moveDirection[1]){
			if(unit->walkingDelta > 0.5f){
				moveDirection[0] = 0;
				unit->walkingDelta = 0;
			}else if(diff[0] != 0){
				unit->walkingDelta += fabsf(diff[1] / diff[0]);
				moveDirection[1] = 0;
			}
		}else{
			unit->walkingDelta = 0;
		}
		if(((unit->walkingObsctructedFrames > 7) && (!unit->raftBuildingProgress)) || (unit->obstructingOtherUnit && ((!unit->walkingObsctructedFrames) || (unit->walkingObsctructedFrames > 5)) && (!unit->raftBuildingProgress))){
			unit->obstructingOtherUnit = 0;
			moveDirection[rand()&1] = (rand()&1)*2-1;
			doMove = true;
		}

		if(doMove){
			int newPos[] = {unit->pos[0]+moveDirection[0], unit->pos[1]+moveDirection[1]};
			GroundTileType_e ground = world->ground[TileOffset(world, newPos[0], newPos[1])];
			int buildingId = GetBuildingOnTile(world, newPos[0], newPos[1], false);
			bool groundWalkable = IsGroundTypeWalkable(ground);
			if(!groundWalkable){
				if(ground == GND_WATER){
					if(unit->isRafted){
						groundWalkable = true;
					}else{
						bool fastBoating = false;
						if(unit->playerId != -1){
							Player_t* player = GetPlayer(unit->playerId);
							if(player->stats.technologiesResearched[TECH_ADVANCED_TECHNOLOGY]){
								fastBoating = true;
							}
						}
						unit->raftBuildingProgress++;
						if( (unit->raftBuildingProgress >= 10) || fastBoating ){
							unit->isRafted = 1;
							unit->raftBuildingProgress = 0;
						}
					}
				}else{
					unit->walkingObsctructedFrames++;
				}
			}else{
				if(unit->isRafted){
					unit->isRafted = 0;
				}
				unit->raftBuildingProgress = 0;
			}
			if(((buildingId == -1) || (unit->fortifiedBuildingId != -1)) && groundWalkable){
				int unitOnTileId = GetUnitOnTile(unit->worldId, newPos[0], newPos[1]);
				if(unitOnTileId == -1){
					unit->walkingObsctructedFrames = 0;
					if(unit->fortifiedBuildingId != -1){
						Building_t* building = &world->buildings[unit->fortifiedBuildingId];
						building->unitsFortified--;
						unit->fortifiedBuildingId = -1;
					}
					unit->pos[0] = newPos[0];
					unit->pos[1] = newPos[1];
					unit->orientation[0] = moveDirection[0];
					unit->orientation[1] = moveDirection[1];

					if(ground != GND_WATER){
						PlayAudioEffect(SFX_STEP, unit->worldId, (Vector2){unit->pos[0]*TILE_SIZE, unit->pos[1]*TILE_SIZE});
					}
				}else{
					Unit_t* unitOnTile = GetUnit(unitOnTileId);
					unitOnTile->obstructingOtherUnit = 1;
					unit->walkingObsctructedFrames++;
				}
			}else{
				unit->walkingObsctructedFrames++;
			}
		}

	}
}

void DrawUnits(){
	for(int unitId=0; unitId < MAX_UNITS; unitId++){
		Unit_t* unit = GetUnit(unitId);
		UnitType_t* unitType = &UnitTypes[unit->type];
		if(!unit->active) continue;
		if(unit->worldId != client.currentWorldId) continue;
		if(unit->fortifiedBuildingId != -1) continue;
		if(!RectInView(unit->pos[0], unit->pos[1], 1, 1)) continue;

		Vector2 pos = {unit->pos[0]*TILE_SIZE, unit->pos[1]*TILE_SIZE};
		int tileId;
		int rotate=0;
		bool flip_h=false, flip_v=false;
		Color tint;

		if(unit->playerId == -1){
			tint = WHITE;
		}else{
			Player_t* player = GetPlayer(unit->playerId);
			tint = player->color;
		}

		if(unit->carryingItem){
			int iconTileId;
			switch(unit->carryingItem){
				case ITEM_FRUIT:
					iconTileId = 914;
					break;
				case ITEM_RUBY:
					iconTileId = 722;
					break;
				case ITEM_COIN:
					iconTileId = 809;
			}
			DrawTileEx(iconTileId, (Vector2){pos.x-2, pos.y-7}, 0.5f, 0.9f, WHITE);
		}

		switch(unit->type){
			case UNIT_KING:
				tileId = 124;
				break;
			case UNIT_PEASANT:
				tileId = 57;
				break;
			case UNIT_WARRIOR:
				tileId = 27;
				break;
			case UNIT_KNIGHT:
				tileId = 29;
				break;
			case UNIT_MAGE:
				tileId = 88;
				break;
			case UNIT_DRUID:
				tileId = 63;
				break;
			case UNIT_DEVIL:
				tileId = 91;
				if(!((((game.frame>>3)+(game.frame*5))+game.frame)&7)){
					tileId = rand() % 666;
				}
				break;
			case UNIT_MONSTER_SCORPION:
				tileId = 184;
				break;
			case UNIT_MONSTER_SPIDER:
				tileId = 188;
				break;
			case UNIT_MONSTER_SKELETON:
				tileId = 221;
				break;
		}
		if(unit->isRafted){
			if(unit->orientation[0]){
				tileId = 619;
				flip_h = (unit->orientation[0] < 0);
			}else{
				tileId = (unit->orientation[1] < 0) ? 617 : 618;
			}
		}else if(unit->raftBuildingProgress){
			DrawRectangleLinesEx((Rectangle){pos.x-2, pos.y-6, 4+TILE_SIZE, 6}, 2, DARKBLUE);
			DrawRectangleRec((Rectangle){pos.x, pos.y-4, (unit->raftBuildingProgress / (float)10)*TILE_SIZE, 2}, GREEN);
		}

		if(unit->beingTransportedByUnitId != -1){
			//tint.a = 190;
			DrawTileEx(tileId, (Vector2){(unit->pos[0]-0.18f)*TILE_SIZE, (unit->pos[1]-0.2f)*TILE_SIZE}, -35, 0.7f, tint);
		}else{
			DrawTilePro(tileId, unit->pos[0], unit->pos[1], rotate, flip_h, flip_v, tint);
		}

		if(unit->attackCharge){
			float angle = -75 + 0.5f*(unit->attackCharge*12.5f + 2.25f*unit->attackCharge*unit->attackCharge);
			DrawTileEx((unitType->attackMaxDistance > 1) ? 834 : 931, (Vector2){pos.x + 14, pos.y - 12 - 10*sinf(0.35f * unit->attackCharge) }, angle, 0.5f, (Color){255,255,255, 140+unit->attackCharge*5});
		}

		if(unit->lastTakenDamageFrame){
			int elapsed = game.frame - unit->lastTakenDamageFrame;
			if((elapsed < 60) || ((elapsed < 80) && ((game.frame>>1)&1))){
				if(unit->health <= 0){
					unit->lastAnimatedHealthValue = 0;
				}else{
					int diff = unit->health - unit->lastAnimatedHealthValue;
					if(diff){
						diff = (int)ceilf((float)diff / 20.0f);
						unit->lastAnimatedHealthValue += diff;
					}
					if(unit->lastAnimatedHealthValue > unitType->maxHealth) unit->lastAnimatedHealthValue = unitType->maxHealth;
				}
				DrawRectangle(pos.x-5, pos.y-10, TILE_SIZE+10, 4, DARKGRAY);
				DrawRectangle(pos.x-3, pos.y-8, (int)(((float)unit->lastAnimatedHealthValue / unitType->maxHealth)*(TILE_SIZE+6)), 2, RED);
			}
		}

		// if(unit->pathLength){
		// 	char ntxt[1024];
		// 	for(int i=0; i < unit->pathLength; i++){
		// 		PathNode_t* node = &unit->path[i];
		// 		sprintf(ntxt, "%i", i);
		// 		DrawText(ntxt, node->x*TILE_SIZE, node->y*TILE_SIZE, 14, (Color){0,0,0, 100});
		// 	}
		// }

	}
}
