#include "graphics.h"
#include "gameapp.h"
#include "raylib.h"
#include "raymath.h"
#include "rlgl.h"
#define _USE_MATH_DEFINES
#include "math.h"
#include "string.h"

Rectangle TilemapSourceRect(int id){
	return (Rectangle){
		(TILE_MARGIN+TILE_SIZE) * (id & (TILES_PER_LINE-1)),
		(TILE_MARGIN+TILE_SIZE) * (id / TILES_PER_LINE),
		TILE_SIZE,
		TILE_SIZE
	};
}

void DrawTile(int id, int pos_x, int pos_y){
	Rectangle srcRec = TilemapSourceRect(id);
	DrawTextureRec(client.assets.tilemap, srcRec, (Vector2){pos_x * TILE_SIZE, pos_y * TILE_SIZE}, WHITE);
}

void DrawTilePro(int id, int pos_x, int pos_y, int rotate, bool flip_h, bool flip_v, Color tint){
	Rectangle srcRec = TilemapSourceRect(id);
	Vector2 pos = {pos_x * TILE_SIZE, pos_y * TILE_SIZE};
	Texture2D* texture = &client.assets.tilemap;

	Rectangle dstRec = {pos_x*TILE_SIZE, pos_y*TILE_SIZE, TILE_SIZE, TILE_SIZE};
	// if(flip_h) { srcRec.x += srcRec.width; srcRec.width *= -1; }
	// if(flip_v) { srcRec.y += srcRec.height; srcRec.height *= -1; }
	if(flip_h) { srcRec.width *= -1; }
	if(flip_v) { srcRec.height *= -1; }
	DrawTexturePro(*texture, srcRec, dstRec, Vector2Zero(), (float)rotate*90, tint);

	// float vertsPos[] = {
	// 	0, 0,
	// 	TILE_SIZE, 0,
	// 	TILE_SIZE, TILE_SIZE,
	// 	0, TILE_SIZE
	// };

	// float vertUVs[] = {
	// 	srcRec.x, srcRec.y,
	// 	srcRec.x+srcRec.width, srcRec.y,
	// 	srcRec.x+srcRec.width, srcRec.y+srcRec.height,
	// 	srcRec.x, srcRec.y+srcRec.height
	// };

	// int indices_pos[] = {0, 1, 2,  0, 2, 3};
	// int indices_uv[] = {0, 1, 2,  0, 2, 3};
	// if(flip_h){
	// 	int newIndices[] = {1, 0, 3,  1, 3, 2};
	// 	memcpy(&indices_uv, &newIndices, sizeof(indices_uv));
	// }
	// if(flip_v){
	// 	int newIndices[] = {indices_uv[5], indices_uv[4], indices_uv[1], indices_uv[5], indices_uv[1], indices_uv[0]};
	// 	memcpy(&indices_uv, &newIndices, sizeof(indices_uv));
	// }
	// if(rotate){
	// 	while(rotate < 0){
	// 		rotate += 4;
	// 	}
	// 	for(int i=0; i < rotate; i++){
	// 		int newIndices[] = {indices_uv[5], indices_uv[0], indices_uv[1], indices_uv[5], indices_uv[1], indices_uv[2]};
	// 		memcpy(&indices_uv, &newIndices, sizeof(indices_uv));
	// 	}
	// }

	// if(rlCheckBufferLimit(6)) rlglDraw();
	// rlBegin(RL_TRIANGLES);
	// rlEnableTexture(texture->id);
	
	// rlColor4ub(tint.r, tint.g, tint.b, tint.a);
	// //rlNormal3f(0, 0, 1);
	// for(int i=5; i >= 0; i--){
	// 	rlTexCoord2f(vertUVs[indices_uv[i]*2] / texture->width, vertUVs[indices_uv[i]*2+1] / texture->height);
	// 	rlVertex2f(pos.x+vertsPos[indices_pos[i]*2], pos.y+vertsPos[indices_pos[i]*2+1]);
	// }
	// rlEnd();
	// rlDisableTexture();
}

void DrawTileEx(int id, Vector2 pos, float angle, float size, Color tint){
	Rectangle srcRec = TilemapSourceRect(id);
	DrawTexturePro(client.assets.tilemap, srcRec, (Rectangle){pos.x, pos.y, TILE_SIZE*size, TILE_SIZE*size}, Vector2Zero(), angle, tint);
}

void DrawPixmap(Pixmap_t* pixmap, int pos_x, int pos_y){
	//DrawRectangleLines(TILE_SIZE*pos_x, TILE_SIZE*pos_y, TILE_SIZE*pixmap->width, TILE_SIZE*pixmap->height, RED);
	for(int iy=0; iy < pixmap->height; iy++)
	for(int ix=0; ix < pixmap->width; ix++){
		int offset = PixmapOffset(pixmap, ix, iy);
		int tileId = pixmap->tileId[offset];
		if(tileId){
			DrawTile(tileId, pos_x+ix, pos_y+iy);
		}
	}
}

void DrawPixmapEx(Pixmap_t* pixmap, Vector2 pos, Color tint){
	for(int iy=0; iy < pixmap->height; iy++)
	for(int ix=0; ix < pixmap->width; ix++){
		int offset = PixmapOffset(pixmap, ix, iy);
		int tileId = pixmap->tileId[offset];
		if(tileId){
			DrawTileEx(tileId, (Vector2){pos.x+TILE_SIZE*ix, pos.y+TILE_SIZE*iy}, 0, 1, tint);
		}
	}
}
