#pragma once
#include "stdbool.h"

#ifndef DEDICATED_SERVER

#include "raylib.h"

#define TILE_SIZE 16
#define TILE_MARGIN 1
#define TILES_PER_LINE 32

typedef struct{
    Texture2D tilemap;
    Sound sfx_step[4];
    Sound sfx_sword[4];
    Sound sfx_magic[4];
    Sound sfx_arrow[4];
} ClientAssets_t;

#endif

bool LoadAssets();
