#pragma once

#include "raylib.h"
#include "assets.h"
#include "graphics.h"
#include "world.h"
#include "gameinterface.h"
#include "units.h"
#include "players.h"
#include "capacities.h"
#include "particles.h"

#define GAME_TITLE "Three Knights of Light"
#define GET_GAME_TICKRATE() app.config.tickrate
#define GET_WALL_TICKRATE() 20
#define DEFAULT_TICKRATE 20
#define CHARGE_MULTIPLIER 2

typedef enum{
    STATE_LOBBY,
	STATE_UNINITIALIZED,
	STATE_STARTING,
	STATE_PLAYING,
	STATE_ENDED
} GameState_e;

typedef struct{
	int initializedOnce;
	int frame;
	int timer;
	GameState_e state;
    World_t worlds[MAX_WORLDS];
    Unit_t units[MAX_UNITS];
    Player_t players[MAX_PLAYERS];
    int devilPlayerId;
    int devilRevealed;
    int devilHasWon;
} GameState_t;

typedef struct{
	int fullscreen;
	int vsync;
	int resolution[2];
	int windowPos[2];
    float soundVolume;
    float musicVolume;
    int mouseEdgeSlide;

    int tickrate;
} AppConfig_t;

typedef struct{
	char configPath[64];
	AppConfig_t config;
	int shouldRestartRound;
	int gamesPlayed;
	int playersOn;
} AppState_t;

extern AppState_t app;
extern GameState_t game;

#ifndef DEDICATED_SERVER

typedef enum{
	SCREEN_MAINMENU,
	SCREEN_GAME
} ClientScreen_e;

typedef struct{
    ClientAssets_t assets;
	ClientScreen_e screen;
	int isPaused;
	int width, height;
	int halfWidth, halfHeight;
	float appTime;
    float deltaTime;
	Camera2D cam;
    Vector2 camVelocity;
	int viewBounds[4];
	int localPlayerId;
    int currentWorldId;
	Texture2D worldMinimaps[MAX_WORLDS];
    char worldCellHighlight[MAX_WORLD_SIZE][MAX_WORLD_SIZE];
	ParticleSystemState_t particles;
	ClientUI_t ui;
	int shouldClose;
} ClientState_t;

extern ClientState_t client;

#endif

void LoadSettings();
void SaveSettings();
void InitializeApp();
void MainLoop();
void StartGame();
void ResetGame();
void CloseGame();

inline World_t* GetWorld(int id){
    return &game.worlds[id];
}

inline Unit_t* GetUnit(int id){
    return &game.units[id];
}

inline Player_t* GetPlayer(int id){
    return &game.players[id];
}

#define randf() ((float)rand() / RAND_MAX)
