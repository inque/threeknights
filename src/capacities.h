#pragma once

#define MAX_WORLDS 8
#define MAX_PLAYERS 8
#define MAX_UNITS 4096
#define MAX_WORLD_SIZE 128
#define MAX_BUILDINGS 1024
