#include "buildings.h"
#include "gameapp.h"
#include "graphics.h"
#include "pixmaps.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

static const Pixmap_t PIXMAP_CASTLE = {
	.width = 5,
	.height = 4,
	.tileSolid = {
		1, 0, 0, 0, 1,
		1, 1, 1, 1, 1,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0
	},
	.tileId = {
		615, 354, 273, 354, 615,
		554, 555, 517, 554, 554,
		16,  16,  16,  16,  16,
		16,  16,  16,  16,  16
	}
};

static const Pixmap_t PIXMAP_MINE = {
	.width = 2,
	.height = 2,
	.tileSolid = {
		1, 1,
		1, 1
	},
	.tileId = {
		431, 160,
		131, 400
	}
};

static const Pixmap_t PIXMAP_BARRACKS = {
	.width = 7,
	.height = 3,
	.tileSolid = {
		0, 1, 1, 1, 1, 0, 0,
		1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1
	},
	.tileId = {
		22,  175, 176, 355, 177, 0, 0,
		554, 489, 587, 386, 554, 642, 642,
		555, 554, 131, 129, 586, 642, 642
	}
};

static const Pixmap_t PIXMAP_ACADEMY = {
	.width = 5,
	.height = 4,
	.tileSolid = {
		0, 0, 0, 0, 1,
		1, 1, 1, 1, 1,
		1, 1, 1, 1, 1,
		1, 1, 1, 1, 1
	},
	.tileId = {
		0,   0,   0,   0,   655,
		645, 645, 354, 655, 687,
		645, 616, 417, 386, 386,
		97,  99,  424, 363, 416
	}
};

static const Pixmap_t PIXMAP_WORKSHOP = {
	.width = 5,
	.height = 3,
	.tileSolid = {
		1, 1, 1, 1, 1,
		1, 1, 1, 1, 1,
		1, 1, 1, 1, 1
	},
	.tileId = {
		165, 165, 129, 165, 355,
		554, 196, 398, 453, 387,
		554, 366, 235, 555, 419
	}
};

static const Pixmap_t PIXMAP_ORACLE = {
    .width = 5,
    .height = 3,
    .tileSolid = {
        1, 1, 1, 1, 0,
        1, 1, 1, 0, 0,
        1, 1, 1, 0, 1
    },
    .tileId = {
        70,  370, 372, 654, 204,
        761, 417, 554, 205, 205,
        225, 416, 557, 342, 374
    }
};

static const Pixmap_t PIXMAP_FACTORY = {
    .width = 8,
    .height = 4,
    .tileSolid = {
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1
    },
    .tileId = {
        79,  80,  358, 357, 359, 359, 359, 115,
        396, 554, 555, 416, 555, 554, 486, 554,
        428, 325, 327, 554, 554, 392, 395, 554,
        129, 129, 132, 130, 129, 128, 129, 129
    }
};

const BuildingType_t BuildingTypes[] = {
	[BUILDING_CASTLE] = {
		.name = "The castle",
		.pixmap = &PIXMAP_CASTLE,
		.providesFood = 7,
		.fortificationLimit = 15,
		.maxHealth = 500,
		.description = "\n Your home.",
		.iconTileId = 614,
		.isUnique = 1,
		.attackCharge = 10*CHARGE_MULTIPLIER,
		.attackDamage = 4,
		.attackRange = 12
	},
	[BUILDING_HOUSE] = {
		.name = "House",
		.singleTile = 646,
		.priceGold = 10,
		.providesFood = 1,
		.fortificationLimit = 1,
		.farmProductionDuration = 30*10,
		.maxHealth = 50,
		.buildEffort = 20,
		.description = "\n House. \n +1 food. \n -10 gold. \n Provides products every 30 seconds.",
		.iconTileId = 646
	},
	[BUILDING_MINE] = {
		.name = "Mine",
		.pixmap = &PIXMAP_MINE,
		.priceGold = 50,
		.providesFood = -7,
		.farmProductionDuration = 10*10,
		.fortificationLimit = 6,
		.maxHealth = 100,
		.buildEffort = 55,
		.description = "\n Mine. \n -7 food. \n -50 gold. \n Provides gems every 10 seconds.",
		.iconTileId = 875
	},
	[BUILDING_BARRACKS] = {
		.name = "Barracks",
		.pixmap = &PIXMAP_BARRACKS,
		.priceGold = 60,
		.providesFood = -10,
		.fortificationLimit = 8,
		.maxHealth = 225,
		.buildEffort = 60,
		.description = "\n Barracks. \n -10 food. \n -60 gold \n Allows to hire warriors.",
		.iconTileId = 970
	},
	[BUILDING_ACADEMY] = {
		.name = "The academy",
		.pixmap = &PIXMAP_ACADEMY,
		.priceGold = 75,
		.providesFood = -10,
		.fortificationLimit = 8,
		.maxHealth = 120,
		.buildEffort = 85,
		.description = "\n The academy. \n -10 food. \n -75 gold. \n Allows to hire mages \n and counduct research.",
		.iconTileId = 877,
		.isUnique = 1
	},
	[BUILDING_WORKSHOP] = {
		.name = "The workshop",
		.pixmap = &PIXMAP_WORKSHOP,
		.priceGold = 80,
		.providesFood = -5,
		.fortificationLimit = 10,
		.maxHealth = 180,
		.buildEffort = 70,
		.description = "\n The workshop. \n -5 food. \n -80 gold. \n Allows military advancement \n and mine construction.",
		.iconTileId = 388,
		.isUnique = 1
	},
	[BUILDING_OUTPOST] = {
		.name = "Outpost",
		.singleTile = 610,
		.priceGold = 35,
		.fortificationLimit = 2,
		.maxHealth = 40,
		.buildEffort = 35,
		.description = "\n Outpost. \n -35 gold. \n A basic defensive structure \n that will fire arrows at enemies.",
		.iconTileId = 610,
		.attackCharge = 10*CHARGE_MULTIPLIER,
		.attackDamage = 4,
		.attackRange = 10
	},
	[BUILDING_TOWER] = {
		.name = "Tower",
		.singleTile = 612,
		.priceGold = 50,
        .providesFood = -2,
		.fortificationLimit = 5,
		.maxHealth = 60,
		.buildEffort = 35,
		.description = "\n Tower. \n -50 gold. \n -2 food. \n A more serious defense building. \n Requires 'DEFENSIVE TACTICS'.",
		.iconTileId = 612,
		.attackCharge = 14*CHARGE_MULTIPLIER,
		.attackDamage = 9,
		.attackRange = 8
	},
    [BUILDING_ORACLE] = {
        .name = "The Oracle's hut",
        .pixmap = &PIXMAP_ORACLE,
        .priceGold = 75,
        .fortificationLimit = 1,
        .maxHealth = 45,
        .buildEffort = 50,
        .description = "\n The Oracle's hut. \n -75 gold. \n Allows to hire druids and cast 'METEORITE RAIN'. \n Requires 'ADVANCED MAGIC'",
        .iconTileId = 814,
        .isUnique = 1
    },
    [BUILDING_FACTORY] = {
        .name = "Factory",
        .pixmap = &PIXMAP_FACTORY,
        .priceGold = 100,
        .providesFood = 150,
        .fortificationLimit = 6,
        .maxHealth = 300,
        .buildEffort = 85,
        .description = "\n Factory. \n +150 food. \n -100 gold.",
        .iconTileId = 203
    },
    [BUILDING_BONFIRE] = {
        .name = "The place",
        .singleTile = 334,
        .maxHealth = 1,
        .description = "\n Probably of some use.",
        .iconTileId = 204
    }
};

bool CanBuildAtLocation(int worldId, int playerId, BuildingType_e type, int pos_x, int pos_y){
	World_t* world = GetWorld(worldId);
	Player_t* player = GetPlayer(playerId);
	BuildingType_t* buildingType = &BuildingTypes[type];
	if(player->ownWorldId != worldId) return false;

	if(pos_y < 2) return false;
	if((pos_y + ((buildingType->pixmap) ? buildingType->pixmap->height : 1)) > (world->height - 10)) return false;

	if(buildingType->pixmap){
		for(int iy=0; iy < buildingType->pixmap->height; iy++)
		for(int ix=0; ix < buildingType->pixmap->width; ix++){
			if(buildingType->pixmap->tileSolid[PixmapOffset(buildingType->pixmap, ix, iy)]){
				if(!IsTileWalkable(worldId, pos_x+ix, pos_y+iy, true)) return false;
			}
		}
	}else{
		return IsTileWalkable(worldId, pos_x, pos_y, true);
	}

	return true;
}

static void OnBuildingBuilt(Building_t* building){
	BuildingType_t* buildingType = &BuildingTypes[building->type];
	building->health = buildingType->maxHealth;
	building->isBuilt = 1;
	building->buildProgress = buildingType->buildEffort;
	if(building->playerId != -1){
		//provide resources
		Player_t* player = GetPlayer(building->playerId);
		player->stats.buildingCountByType[building->type]++;
		if(buildingType->providesFood > 0){
			player->stats.food += buildingType->providesFood;
		}
	}
}

int CreateBuilding(int worldId, int playerId, BuildingType_e type, int pos_x, int pos_y, bool prebuilt){
	World_t* world = GetWorld(worldId);
	BuildingType_t* buildingType = &BuildingTypes[type];
	for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
		Building_t* building = &world->buildings[buildingId];
		if(building->active) continue;
		
		building->active = 1;
		building->type = type;
		building->pixmap = buildingType->pixmap;
		building->playerId = playerId;
		building->healthRestoreTimer = 0;
		building->attackChargeTimer = 0;
		building->attackTargetUnitId = -1;
		building->lookForEnemiesTimer = 0;
		memset(&building->hiringQueue, 0, sizeof(building->hiringQueue));
		building->hiringTotal = 0;
		building->hiringUnitId = -1;
		if(prebuilt){
			OnBuildingBuilt(building);
		}else{
			building->health = CONSTRUCTION_SITE_HEALTH;
			building->isBuilt = 0;
			building->buildProgress = 0;
		}
		building->unitsFortified = 0;
		building->farmProductionTimer = buildingType->farmProductionDuration;
		building->pos[0] = pos_x;
		building->pos[1] = pos_y;
		if(building->pixmap){
			for(int iy=0; iy < building->pixmap->height; iy++)
			for(int ix=0; ix < building->pixmap->width; ix++){
				world->ground[TileOffset(world, pos_x+ix, pos_y+iy)] = GND_EMPTY;
			}
			building->size[0] = building->pixmap->width;
			building->size[1] = building->pixmap->height;
		}else{
			world->ground[TileOffset(world, pos_x, pos_y)] = GND_EMPTY;
			building->size[0] = 1;
			building->size[1] = 1;
		}
		if(playerId != -1){
			//consume resources
			Player_t* player = GetPlayer(playerId);
			player->stats.buildingCount++;
			player->stats.buildingCountByTypeCountingUnbuilt[type]++;
			if(buildingType->providesFood < 0){
				player->stats.food += buildingType->providesFood;
			}

			//send someone over to build

			int bestFoundUnitId = -1;
			bool bestFoundUnitIsBusy = false;
			for(int unitId=0; unitId < MAX_UNITS; unitId++){
				Unit_t* unit = GetUnit(unitId);
				if(!unit->active) continue;
				if(unit->type != UNIT_PEASANT) continue;
				if((unit->playerId != playerId) || (unit->worldId != worldId)) continue;
				bool isBusy = !((unit->action.type == UNIT_ACTION_WHATEVER) || (unit->action.type == UNIT_ACTION_IDLE) || (unit->action.type == UNIT_ACTION_WALKING));
				if((unit->action.type == UNIT_ACTION_FORTIFY) && (unit->fortifiedBuildingId != -1)){
					Building_t* unitBuilding = &world->buildings[unit->fortifiedBuildingId];
					BuildingType_t* unitBuildingType = &BuildingTypes[unitBuilding->type];
					if(!unitBuildingType->farmProductionDuration) isBusy = false;
				}
				
				if((bestFoundUnitId == -1) || (bestFoundUnitIsBusy && (!isBusy))){
					bestFoundUnitId = unitId;
					if(isBusy){
						bestFoundUnitIsBusy = true;
					}else{
						break;
					}
				}
			}
			if(bestFoundUnitId != -1){
				Unit_t* unit = GetUnit(bestFoundUnitId);
				unit->action.type = UNIT_ACTION_FORTIFY;
				unit->action.fortify.worldId = worldId;
				unit->action.fortify.buildingId = buildingId;
			}
			
		}
		return buildingId;
	}
	return -1;
}

void RemoveBuilding(int worldId, int buildingId){
	World_t* world = GetWorld(worldId);
	Building_t* building = &world->buildings[buildingId];
	BuildingType_t* buildingType = &BuildingTypes[building->type];
	if(building->playerId != -1){
		Player_t* player = GetPlayer(building->playerId);
		player->stats.buildingCount--;
		player->stats.buildingCountByTypeCountingUnbuilt[building->type]--;
		if(building->isBuilt){
			player->stats.buildingCountByType[building->type]--;
		}
		player->stats.food -= buildingType->providesFood;
	}
	if(building->unitsFortified){
		for(int unitId=0; unitId < MAX_UNITS; unitId++){
			Unit_t* unit = GetUnit(unitId);
			if(!unit->active) continue;
			if(unit->worldId != worldId) continue;
			if(unit->fortifiedBuildingId != buildingId) continue;

			unit->fortifiedBuildingId = -1;
			if((unit->action.type == UNIT_ACTION_FORTIFY) && (unit->action.fortify.buildingId == buildingId)){
				unit->action.type = UNIT_ACTION_WHATEVER;
			}
		}

	}
	building->active = 0;
}

void BuildingTakeDamage(int worldId, int buildingId, int dmg){
	World_t* world = GetWorld(worldId);
	Building_t* building = &world->buildings[buildingId];
	if(!building->active) return;

	SpawnParticles(EFFECT_BUILDING_DAMAGE, worldId, (Vector2){(building->pos[0]+0.5f*building->size[0])*TILE_SIZE, (building->pos[1]+0.5f*building->size[1])*TILE_SIZE}, Vector2Zero(), dmg);

	building->health -= dmg;
	if(building->health <= 0){
		RemoveBuilding(worldId, buildingId);
		for(int iy=0; iy < building->size[1]; iy++)
		for(int ix=0; ix < building->size[0]; ix++){
			world->ground[TileOffset(world, building->pos[0]+ix, building->pos[1]+iy)] = GND_RUBBLE;
		}
	}
}

void BuildingConstruct(int worldId, int buildingId, int progress){
	World_t* world = GetWorld(worldId);
	Building_t* building = &world->buildings[buildingId];
	BuildingType_t* buildingType = &BuildingTypes[building->type];
	if(!building->active) return;
	if(building->isBuilt) return;

	int oldVal = building->buildProgress;
	building->buildProgress += progress;

	if(building->buildProgress >= buildingType->buildEffort){
		OnBuildingBuilt(building);
	}

	if((oldVal / 10) != (building->buildProgress / 10)){
		SpawnParticles(EFFECT_BUILDING_DAMAGE, worldId, (Vector2){(building->pos[0]+0.5f*building->size[0])*TILE_SIZE, (building->pos[1]+0.5f*building->size[1])*TILE_SIZE}, Vector2Zero(), 1);
	}
}

void UpdateBuildings(){
	for(int worldId=0; worldId < MAX_WORLDS; worldId++){
		World_t* world = GetWorld(worldId);
		if(!world->active) continue;

		for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
			Building_t* building = &world->buildings[buildingId];
			BuildingType_t* buildingType = &BuildingTypes[building->type];
			if(!building->active) continue;
			if(!building->isBuilt) continue;

			if(building->health < buildingType->maxHealth){
				if(!building->healthRestoreTimer){
                    int defenseBonus = (building->unitsFortified * 8) + ((building->unitsFortified / (float)buildingType->fortificationLimit) * 40);
                    if(defenseBonus > 50) defenseBonus = 50;
					building->healthRestoreTimer = 100 - defenseBonus;
                    building->health++;
				}else{
					building->healthRestoreTimer--;
				}
			}

			if(buildingType->attackDamage){
				if(building->attackTargetUnitId == -1){
					if(building->lookForEnemiesTimer){
						building->lookForEnemiesTimer--;
					}else{
						building->lookForEnemiesTimer = 10;
						
						int bestFoundUnitId = -1;
						int bestFoundUnitScore;
						for(int unitId=0; unitId < MAX_UNITS; unitId++){
							Unit_t* unit = GetUnit(unitId);
							if(!unit->active) continue;
							if(unit->worldId != worldId) continue;
							if(unit->playerId == building->playerId) continue;
							if(unit->fortifiedBuildingId != -1) continue;
                            if((unit->playerId != -1) && (building->playerId != -1)){
                                Player_t* player = GetPlayer(building->playerId);
                                if(!player->stats.autoAttackPlayer[unit->playerId]){
                                    continue;
                                }
                            }
							int dist = abs(unit->pos[0]-building->pos[0]) + abs(unit->pos[1]-building->pos[1]);
							if(dist > buildingType->attackRange) continue;

							int score = dist + (unit->health/5);
							if((bestFoundUnitId == -1) || (score < bestFoundUnitScore)){
								bestFoundUnitId = unitId;
								bestFoundUnitScore = score;
							}
						}
						if(bestFoundUnitId != -1){
							building->attackTargetUnitId = bestFoundUnitId;
						}
					}
				}else{
					Unit_t* unit = GetUnit(building->attackTargetUnitId);
					if(unit->active){
						int dist = abs(unit->pos[0]-building->pos[0]) + abs(unit->pos[1]-building->pos[1]);
						if(dist <= buildingType->attackRange){
							if(building->attackChargeTimer){
								building->attackChargeTimer--;
							}else{
								building->attackChargeTimer = buildingType->attackCharge;
                                Vector2 pos = {building->pos[0]*TILE_SIZE, building->pos[1]*TILE_SIZE};
								SpawnParticles(EFFECT_ARROWS, worldId, pos, (Vector2){unit->pos[0]-building->pos[0], unit->pos[1]-building->pos[1]}, 0);
                                PlayAudioEffect(SFX_ARROW, worldId, pos);
								UnitTakeDamage(building->attackTargetUnitId, buildingType->attackDamage);
							}

						}else{
							building->attackTargetUnitId = -1;
						}
					}else{
						building->attackTargetUnitId = -1;
					}
				}
			}

			if(building->hiringTotal && (building->unitsFortified < buildingType->fortificationLimit)){
				//first see if we have any fortified candidates
				if(building->unitsFortified){
					for(int unitId=0; unitId < MAX_UNITS; unitId++){
						Unit_t* unit = GetUnit(unitId);
						if(!unit->active) continue;
						if(unit->type != UNIT_PEASANT) continue;
						if(unit->fortifiedBuildingId != buildingId) continue;
						if(unit->playerId != building->playerId) continue;
						if(unit->worldId != worldId) continue;
						
						building->hiringUnitId = unitId;
						break;
					}
				}

				//then call someone over
				if(building->hiringUnitId == -1){
					int bestFoundUnitId = -1;
					bool bestFoundUnitHasOtherTasks = true;
					int peasantsCounted = 0;
					for(int unitId=0; unitId < MAX_UNITS; unitId++){
						Unit_t* unit = GetUnit(unitId);
						if(!unit->active) continue;
						if(unit->type != UNIT_PEASANT) continue;
						if(unit->playerId != building->playerId) continue;
						if(unit->worldId != worldId) continue;

						peasantsCounted++;
						bool haveTasks = (unit->action.type != UNIT_ACTION_WHATEVER);
						if((bestFoundUnitId == -1) || (bestFoundUnitHasOtherTasks && (!haveTasks))){
							bestFoundUnitId = unitId;
							bestFoundUnitHasOtherTasks = haveTasks;
						}
					}
					if((bestFoundUnitId != -1) && (peasantsCounted > 1)){
						Unit_t* unit = GetUnit(bestFoundUnitId);
						unit->action.type = UNIT_ACTION_FORTIFY;
						unit->action.fortify.worldId = worldId;
						unit->action.fortify.buildingId = buildingId;
						building->hiringUnitId = bestFoundUnitId;
					}
				}else{
					Unit_t* unit = GetUnit(building->hiringUnitId);
					if( (!unit->active)
					 || (unit->worldId != worldId)
					 || (unit->action.type != UNIT_ACTION_FORTIFY)
					 || (unit->action.fortify.worldId != worldId)
					 || (unit->action.fortify.buildingId != buildingId) ){

						 building->hiringUnitId = -1;
					}else{
						//actually hire
						for(int unitTypeId=0; unitTypeId < _UNIT_TYPE_COUNT; unitTypeId++){
							if(!building->hiringQueue[unitTypeId]) continue;
							
							SpawnParticles(EFFECT_ICON, worldId, (Vector2){building->pos[0]*TILE_SIZE, building->pos[1]*TILE_SIZE}, Vector2Zero(), 839);
							SpawnUnit(unitTypeId, building->playerId, worldId, unit->pos[0], unit->pos[1]);
							RemoveUnit(building->hiringUnitId);
							building->hiringQueue[unitTypeId]--;
							building->hiringTotal--;
							building->hiringUnitId = -1;
							break;
						}
					}
				}
			}

			if(buildingType->farmProductionDuration){
				if(!building->farmProductionTimer){
					for(int unitId=0; unitId < MAX_UNITS; unitId++){
						Unit_t* unit = GetUnit(unitId);
						if(unit->active && (unit->fortifiedBuildingId == buildingId)){
							switch(building->type){
								case BUILDING_HOUSE: {
									unit->carryingItem = ITEM_FRUIT;
									unit->action.type = UNIT_ACTION_WALKING;
									unit->action.walking.dest[0] = unit->pos[0];
									unit->action.walking.dest[1] = 1;

									if(!(rand()&3)){
										int farmPos[]={(rand()&1)*2-1, (rand()&1)*2-1};
										if(!(farmPos[0] && farmPos[1])) farmPos[0]=-1;
										int tileOffset = TileOffset(world, building->pos[0]+farmPos[0], building->pos[1]+farmPos[1]);
										if(IsGroundTypeWalkable(world->ground[tileOffset])){
											world->ground[tileOffset] = GND_FARMLAND;
										}
									}

								} break;
								case BUILDING_MINE: {
									unit->carryingItem = ITEM_RUBY;
									unit->action.type = UNIT_ACTION_WALKING;
									unit->action.walking.dest[0] = unit->pos[0];
									unit->action.walking.dest[1] = world->height-2;
								} break;
							}
							break;
						}
					}
					building->farmProductionTimer = buildingType->farmProductionDuration;
				}else{
					building->farmProductionTimer--;
				}
			}
		}
	}
}

void DrawBuildings(){
	char text[128];
	World_t* world = GetWorld(client.currentWorldId);
	for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
		Building_t* building = &world->buildings[buildingId];
		BuildingType_t* buildingType = &BuildingTypes[building->type];
		if(!building->active) continue;
		if(!RectInView(building->pos[0], building->pos[1], 1, 1)) continue;
		
		Color tint;
		if(building->playerId == -1){
			tint = WHITE;
		}else{
			Player_t* player = GetPlayer(building->playerId);
			tint = player->color;
		}

		if(!building->isBuilt){
			tint.r = (80+tint.r)/2;
			tint.g = (80+tint.g)/2;
			tint.b = (80+tint.b)/2;
			if(building->pixmap){
				int rubble[] = {193, 368, 424, 459, 366};
				for(int iy=0; iy < building->size[1]; iy++)
				for(int ix=0; ix < building->size[0]; ix++){
					DrawTile(rubble[((ix*5)+(iy<<2)+ix)%5], building->pos[0]+ix, building->pos[1]+iy);
				}
			}else{
				DrawTile(368, building->pos[0], building->pos[1]);
			}
		}
		if(building->pixmap){
			//DrawPixmap(building->pixmap, building->pos[0], building->pos[1]);
			DrawPixmapEx(building->pixmap, (Vector2){building->pos[0]*TILE_SIZE, building->pos[1]*TILE_SIZE}, tint);
		}else{
			DrawTilePro(buildingType->singleTile, building->pos[0], building->pos[1], 0, false, false, tint);
		}

		if(building->unitsFortified){
			Vector2 fortIconPos = {(building->pos[0]+building->size[0]-1.75f)*TILE_SIZE + 20, building->pos[1]*TILE_SIZE - 5};
			Color fortIconColor = Fade(YELLOW, 0.8f);
			DrawTileEx(612, fortIconPos, 0, 0.8f, fortIconColor);
			sprintf(text, "%i", building->unitsFortified);
			DrawText(text, fortIconPos.x + 13, fortIconPos.y - 2, 10, fortIconColor);
		}

		if(building->hiringTotal){
			Vector2 hiringIconPos = {(building->pos[0]+building->size[0]-2.25f)*TILE_SIZE + 20, building->pos[1]*TILE_SIZE - 5};
			Color hiringIconColor = Fade(BLUE, 0.8f);
			DrawTileEx(776, hiringIconPos, 0, 0.8f, hiringIconColor);
            sprintf(text, "%i", building->hiringTotal);
            DrawText(text, hiringIconPos.x + 13, hiringIconPos.y - 2, 10, hiringIconColor);
		}


		int maxHealth = (building->isBuilt) ? buildingType->maxHealth : CONSTRUCTION_SITE_HEALTH;
		if(building->health < maxHealth){
			DrawRectangle(building->pos[0]*TILE_SIZE-10, building->pos[1]*TILE_SIZE-10, building->size[0]*TILE_SIZE+20, 4, DARKGRAY);
			DrawRectangle(building->pos[0]*TILE_SIZE-8, building->pos[1]*TILE_SIZE-8, (int)(((float)building->health / maxHealth)*(building->size[0]*TILE_SIZE+16)), 2, RED);
		}

		if(!building->isBuilt){
			DrawRectangle(building->pos[0]*TILE_SIZE-10, building->pos[1]*TILE_SIZE-20, building->size[0]*TILE_SIZE+20, 4, DARKGRAY);
			DrawRectangle(building->pos[0]*TILE_SIZE-8, building->pos[1]*TILE_SIZE-18, (int)(((float)building->buildProgress / buildingType->buildEffort)*(building->size[0]*TILE_SIZE+16)), 2, GREEN);
		}
		
	}

}
