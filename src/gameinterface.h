#pragma once
#include "raymath.h"
#include "stdbool.h"

typedef struct{
    int isPlacingBuilding;
    int typeOfPlacedBuilding;
    int buildingJustPlaced;
	bool isDragging;
	Vector2 dragOrigin;
    int debugMenu;

    char bottomText[256];
    float bottomTextTimer;
    float bottomTextFrames;
    int lastDeckTotal;
} ClientUI_t;

void DrawUI();
