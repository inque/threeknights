#include "gameplay.h"
#include "gameapp.h"
#include "stdio.h"
#include "stdlib.h"

void GameplayLogicTick(){
	switch(game.state){
		case STATE_UNINITIALIZED: {
			game.state = STATE_STARTING;
			game.timer = 3*GET_WALL_TICKRATE();

			for(int i=0; i < 3; i++){
				int bot = CreatePlayer(1);
			}

			for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
				Player_t* player = GetPlayer(playerId);
				if(!player->active) continue;

				player->stats.gold = 50;
				if(player->isBot){
					player->stats.gold = 350;
				}
				player->ownWorldId = GenerateWorld(playerId);
			}

			game.devilPlayerId = (rand() % 4);
			game.devilRevealed = 0;

		} break;
		case STATE_STARTING: {
			game.timer--;
			if(!game.timer){
				int gameTimeMinutes = 30;
				game.state = STATE_PLAYING;
				game.timer = gameTimeMinutes*GET_WALL_TICKRATE()*60;
			}
		} break;
		case STATE_PLAYING: {

			int playersOn = 0;
			int playersOnNotDevil = 0;
			for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
				Player_t* player = GetPlayer(playerId);
				World_t* world = GetWorld(player->ownWorldId);
				if(!player->active) continue;
				if(player->hasLost){
					if(playerId == client.localPlayerId){
						game.timer = 1;
					}
					continue;
				}
				playersOn++;
				if(playerId != game.devilPlayerId) playersOnNotDevil++;

				if(game.timer < (4*60*GET_WALL_TICKRATE())){
					if(!game.devilRevealed){
						RegisterDevilReveal();
					}
					if((!(game.frame&15)) && (playerId != game.devilPlayerId)){
						SpawnUnit(UNIT_MONSTER_SCORPION+(rand()%3), -1, player->ownWorldId, 2+(rand() % (world->width-4)), (world->height/3-5)+(rand()%20));
					}
				}
			}

			if((!playersOn) || (!playersOnNotDevil) || (game.players[game.devilPlayerId].hasLost)) game.timer = 1;

			game.timer--;
			if(!game.timer){
				if(!game.players[game.devilPlayerId].hasLost){
					game.devilHasWon = 1;
				}else{
					game.devilHasWon = 0;
				}
				game.state = STATE_ENDED;
				game.timer = 30*GET_WALL_TICKRATE();
			}
		} break;
		case STATE_ENDED: {
			
			game.timer--;
			if(!game.timer){
				app.shouldRestartRound = 1;
			}
		} break;
	}
}
