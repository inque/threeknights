#include "gameinterface.h"
#include "gameapp.h"
#include "raylib.h"
#include "raymath.h"
#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
#include "stdbool.h"
#include "string.h"

const int rightPanelWidth = 180;
const int topBarHeight = 60;


static void DisplayBottomText(){
	client.ui.bottomTextTimer = 100;
	client.ui.bottomTextFrames = 0;
}

static const char* tooltipText = NULL;

static bool IsMouseInRect(Rectangle bounds){
	Vector2 mousePos = GetMousePosition();
	if(mousePos.x < bounds.x) return false;
	if(mousePos.y < bounds.y) return false;
	if(mousePos.x > (bounds.x + bounds.width)) return false;
	if(mousePos.y > (bounds.y + bounds.height)) return false;
	return true;
}

static void SetTooltip(Rectangle bounds, const char* text){
	if(!IsMouseInRect(bounds)) return;
	tooltipText = text;
}

static void DrawTooltip(){
	int fontSize = 15;
	if(tooltipText){
		Vector2 pos = GetMousePosition();
		Vector2 size = MeasureTextEx(GetFontDefault(), tooltipText, fontSize, 1.0);
		Rectangle bounds = {pos.x, pos.y, size.x+10, size.y+20};
		if((bounds.x + size.x) > (client.width - 80)) bounds.x -= (bounds.width + 30);
		if((bounds.y + size.y) > (client.height - 30)) bounds.y -= bounds.height;
		bounds.x += 30;

		DrawRectangleRec(bounds, (Color){0,0,0, 160});
		DrawText(tooltipText, bounds.x + 5, bounds.y + 5, fontSize, WHITE);

		tooltipText = NULL;
	}
}


static bool GuiButtonTile(Rectangle bounds, int tileId, const char* tooltip, Color tint, bool disabled){
	bool res;
	if(disabled){
		DrawRectangleRec(bounds, Fade(DARKGRAY, 0.5f));
		DrawRectangleLinesEx(bounds, 2, (Color){100, 0, 0, 255});
		res = false;
	}else{
		res = GuiButton(bounds, "");
	}
	 Vector2 pos = { bounds.x+0.5f*bounds.width - TILE_SIZE,
	 				 bounds.y+0.5f*bounds.height - TILE_SIZE };
	DrawTileEx(tileId, pos, 0, 2, tint);
	if(tooltip) SetTooltip(bounds, tooltip);
	return res;
	//return GuiImageButtonEx(bounds, "", client.assets.tilemap, TilemapSourceRect(tileId));
}




static void myUpdateCamera(bool isHoveringUI){
	Player_t* player = GetPlayer(client.localPlayerId);
	static Vector2 lastMousePos;
	Vector2 mousePos = GetMousePosition();
	Vector2 mouseDelta = Vector2Subtract(mousePos, lastMousePos);
	int mouseCamMoveZone = 140;
	lastMousePos = mousePos;

	float speed = 15.0f * client.deltaTime;
	if(IsKeyDown(KEY_LEFT_SHIFT)) speed *= 2;
	if(IsKeyDown(KEY_A) || IsKeyDown(KEY_LEFT)) client.camVelocity.x -= speed;
	if(IsKeyDown(KEY_D) || IsKeyDown(KEY_RIGHT)) client.camVelocity.x += speed;
	if(IsKeyDown(KEY_W) || IsKeyDown(KEY_UP)) client.camVelocity.y -= speed;
	if(IsKeyDown(KEY_S) || IsKeyDown(KEY_DOWN)) client.camVelocity.y += speed;
	if(!isHoveringUI){
		if(IsMouseButtonDown(MOUSE_MIDDLE_BUTTON) || ((player->selection == SEL_NONE) && IsMouseButtonDown(MOUSE_RIGHT_BUTTON))){
			//client.camVelocity = Vector2Add(client.camVelocity, Vector2Scale(mouseDelta, speed));
			client.cam.target = Vector2Subtract(client.cam.target, Vector2Scale(mouseDelta, 1 / client.cam.zoom));
		}else if(app.config.mouseEdgeSlide){
			Vector2 mouseSlide = {0,0};
			if(mousePos.x < mouseCamMoveZone){
				mouseSlide.x -= ((mouseCamMoveZone - mousePos.x) / (float)mouseCamMoveZone);
			}
			int rightEdge = (client.width - rightPanelWidth - mouseCamMoveZone);
			bool noRightCornerDeadZone = (mousePos.y < (client.halfHeight - 75)) || (mousePos.y > (client.height - 100));
			if((mousePos.x > rightEdge) && noRightCornerDeadZone){
				mouseSlide.x += (mousePos.x - rightEdge) / (float)mouseCamMoveZone;
			}
			int upperEdge = (topBarHeight + mouseCamMoveZone);
			bool noUpperCornerDeadZone = (mousePos.x < (client.halfWidth-120)) || (mousePos.x > (client.halfWidth + 120));
			if((mousePos.y < upperEdge) && noUpperCornerDeadZone){
				mouseSlide.y -= (upperEdge - mousePos.y) / (float)mouseCamMoveZone;
			}
			bool noBottomEdgeDeadZone = (!player->stats.unitsInDeckTotal) || ( (mousePos.x < client.halfWidth - 175) || (mousePos.x > client.halfWidth + 100) );
			if((mousePos.y > (client.height - mouseCamMoveZone)) && noBottomEdgeDeadZone){
				mouseSlide.y += (mousePos.y - (client.height - mouseCamMoveZone)) / (float)mouseCamMoveZone;
			}
			mouseSlide.x = powf(mouseSlide.x, 3);
			mouseSlide.y = powf(mouseSlide.y, 3);
			bool topLeftDeadZone = (mousePos.x < 265) && (mousePos.y < 100);
			if(topLeftDeadZone){
				mouseSlide = Vector2Zero();
			}
			client.camVelocity = Vector2Add(client.camVelocity, Vector2Scale(mouseSlide, 0.0275f));
		}
	}
	World_t* world = GetWorld(client.currentWorldId);
	Vector2 topLeft = GetScreenToWorld2D(Vector2Zero(), client.cam);
	Vector2 bottomRight = GetScreenToWorld2D((Vector2){client.width, client.height}, client.cam);

	Vector2 outOfBoundsCorrect = {0,0};
	if(topLeft.x < -300) outOfBoundsCorrect.x += 1;
	if(topLeft.y < -300) outOfBoundsCorrect.y += 1;
	if(bottomRight.x > (300+TILE_SIZE*world->width)) outOfBoundsCorrect.x += -1;
	if(bottomRight.y > (300+TILE_SIZE*world->height)) outOfBoundsCorrect.y += -1;
	if((outOfBoundsCorrect.x != 0) || (outOfBoundsCorrect.y != 0)){
		client.camVelocity = outOfBoundsCorrect;
	}

	int mouseWheel = GetMouseWheelMove();
	float zoomFactor = 0.25f;
	if((mouseWheel > 0) && (client.cam.zoom < 4)){
		client.cam.zoom += zoomFactor;
	}else if((mouseWheel < 0) && (client.cam.zoom > 1)){
		client.cam.zoom -= zoomFactor;
	}
	
	client.camVelocity = Vector2Scale(client.camVelocity, 0.972f);
	client.cam.target = Vector2Add(client.cam.target, client.camVelocity);
}

static void TryHiring(int unitTypeId, int worldId, int buildingId){
	Player_t* player = GetPlayer(client.localPlayerId);
	UnitType_t* unitType = &UnitTypes[unitTypeId];
	if(player->stats.gold < unitType->priceGold){
		strcpy(client.ui.bottomText, "Not enough gold.");
		DisplayBottomText();
		return;
	}
	player->command.type = COM_HIRE;
	player->command.hire.worldId = worldId;
	player->command.hire.buildingId = buildingId;
	player->command.hire.unitType = unitTypeId;
}

static void TryPlacingBuilding(int buildingTypeId){
	Player_t* player = GetPlayer(client.localPlayerId);
	BuildingType_t* buildingType = &BuildingTypes[buildingTypeId];
	if(player->stats.gold < buildingType->priceGold){
		strcpy(client.ui.bottomText, "Not enough gold.");
		DisplayBottomText();
		return;
	}
	if( (player->stats.food + buildingType->providesFood) < 0 ){
		strcpy(client.ui.bottomText, "Not enough food.");
		DisplayBottomText();
		return;
	}
	client.ui.isPlacingBuilding = 1;
	client.ui.typeOfPlacedBuilding = buildingTypeId;
}


static void DrawRightPanel(){
	Player_t* player = GetPlayer(client.localPlayerId);
	char text[4096];
	const char* descriptionHeader = "";
	const char* descriptionText = "";
	int fontSize = 16;
	int smallFontSize = 12;
	Color textColor = BLACK;
	int margins = 4;
	Rectangle rightPanel = {client.width - rightPanelWidth, 0, rightPanelWidth, client.height};
	Rectangle resourceBox = {rightPanel.x+margins, topBarHeight+rightPanel.y+margins, rightPanel.width-margins*2, 80};
	Rectangle descriptionBox = {rightPanel.x+margins, resourceBox.y+resourceBox.height+margins, rightPanel.width-margins*2, 140};
	Rectangle actionButtonBox = {rightPanel.x+margins, descriptionBox.y+descriptionBox.height+margins, rightPanel.width-margins*2, 0};
	actionButtonBox.height = client.height - actionButtonBox.y - margins;

	DrawRectangleRec(rightPanel, GRAY);
	DrawRectangleLinesEx(resourceBox, 1, DARKGRAY);
	DrawRectangleLinesEx(descriptionBox, 1, DARKGRAY);
	DrawRectangleLinesEx(actionButtonBox, 1, DARKGRAY);

	//resources
	DrawTileEx(841, (Vector2){resourceBox.x+22, resourceBox.y+1}, 0, 2, DARKGRAY);
	DrawTileEx(841, (Vector2){resourceBox.x+25, resourceBox.y+3}, 0, 2, WHITE);
	DrawTileEx(943, (Vector2){resourceBox.x+88, resourceBox.y+1}, 0, 2, DARKGRAY);
	DrawTileEx(943, (Vector2){resourceBox.x+90, resourceBox.y+3}, 0, 2, WHITE);
	sprintf(text, "%02i", player->stats.gold);
	DrawText(text, resourceBox.x+50, resourceBox.y+5, 16, BLACK);
	sprintf(text, "%02i", player->stats.food);
	DrawText(text, resourceBox.x+120, resourceBox.y+5, 16, BLACK);
	SetTooltip((Rectangle){resourceBox.x+15, resourceBox.y+3, 55, 40}, "\n Gold. \n You get it from peasants trading products.");
	SetTooltip((Rectangle){resourceBox.x+90, resourceBox.y+3, 55, 40}, "\n Food. \n Basically your population cap.");

	if(game.devilPlayerId == client.localPlayerId){
		DrawTileEx(218, (Vector2){resourceBox.x+88, resourceBox.y+30}, 0, 2, DARKGRAY);
		DrawTileEx(218, (Vector2){resourceBox.x+90, resourceBox.y+32}, 0, 2, WHITE);
		sprintf(text, "%02i", player->stats.food);
		DrawText(text, resourceBox.x+120, resourceBox.y+30, 16, BLACK);
		SetTooltip((Rectangle){resourceBox.x+90, resourceBox.y+30, 55, 40}, "\n Your minions.");
	}


	Vector2 descriptionHeaderPos = {descriptionBox.x + margins, descriptionBox.y + margins};
	Vector2 descriptionTextPos = {descriptionBox.x + margins*2, descriptionHeaderPos.y + fontSize + 4};
	Rectangle actionButtonBounds[3]; //3 columns!
	for(int i=0; i < 3; i++){
		actionButtonBounds[i] = (Rectangle){actionButtonBox.x+margins, actionButtonBox.y+margins, 45, 45};
		actionButtonBounds[i].x += i * (actionButtonBounds[i].width+margins);
	}

	switch(player->selection){
		case SEL_BUILDING: {
			World_t* world = GetWorld(player->selectedBuldingWorld);
			Building_t* building = &world->buildings[player->selectedBuildingId];
			BuildingType_t* buildingType = &BuildingTypes[building->type];

			descriptionHeader = (const char*)&buildingType->name;
			descriptionText = (const char*)&buildingType->description;

			if(building->isBuilt && buildingType->farmProductionDuration){
				sprintf(text, "Production: %i%%", (int)((1 - building->farmProductionTimer / (float)buildingType->farmProductionDuration) * 100));
				DrawText(text, descriptionBox.x+margins, descriptionBox.y+descriptionBox.height-20, fontSize, textColor);
			}

			sprintf(text, "HP: %i/%i", building->health, buildingType->maxHealth);
			DrawText(text, descriptionBox.x+margins+30, descriptionBox.y+23, fontSize, textColor);
			

			if(GuiButtonTile(actionButtonBounds[2], 382, "Demolish.", (Color){100,0,0,255}, false)){
				player->command.type = COM_DEMOLISH;
				player->command.demolish.worldId = player->selectedBuldingWorld;
				player->command.demolish.buildingId = player->selectedBuildingId;
			}
			actionButtonBounds[2].y += actionButtonBounds[0].height+margins;

			if(building->hiringTotal){
				DrawText("Hiring:", actionButtonBounds[0].x, actionButtonBounds[0].y, fontSize, textColor);
				Rectangle cancelHireBounds = {actionButtonBounds[0].x, actionButtonBounds[0].y + 25, actionButtonBounds[0].width+10, 20};
				for(int unitTypeId=0; unitTypeId < _UNIT_TYPE_COUNT; unitTypeId++){
					UnitType_t* unitType = &UnitTypes[unitTypeId];
					if(building->hiringQueue[unitTypeId]){
						sprintf(text, "%s (%i)", unitType->name, building->hiringQueue[unitTypeId]);
						if(GuiButton(cancelHireBounds, text)){
							player->command.type = COM_CANCEL_HIRE;
							player->command.cancelHire.worldId = player->selectedBuldingWorld;
							player->command.cancelHire.buildingId = player->selectedBuildingId;
							player->command.cancelHire.unitType = unitTypeId;
						}
						cancelHireBounds.y += cancelHireBounds.height+margins;
					}
				}
			}
			
			DrawLine(actionButtonBox.x+margins, actionButtonBox.y+60, actionButtonBox.x+actionButtonBox.width-margins, actionButtonBox.y+60, DARKGRAY);
			if(buildingType->fortificationLimit){
				sprintf(text, "Units inside: %i/%i", building->unitsFortified, buildingType->fortificationLimit);
				DrawText(text, actionButtonBox.x+margins, actionButtonBox.y+60+margins, fontSize, textColor);
			}
			if(building->unitsFortified){
				for(int i=0; i < 3; i++){
					actionButtonBounds[i].y = actionButtonBox.y+75+margins*2;
				}
				int unitsFounds = 0;
				for(int unitId=0; unitId < MAX_UNITS; unitId++){
					Unit_t* unit = GetUnit(unitId);
					if(!unit->active) continue;
					if(unit->worldId != player->selectedBuldingWorld) continue;
					if(unit->fortifiedBuildingId != player->selectedBuildingId) continue;
					
					UnitType_t* unitType = &UnitTypes[unit->type];
					if(GuiButtonTile(actionButtonBounds[unitsFounds%3], unitType->iconTileId, (const char*)&unitType->name, BLACK, false)){
						player->command.type = COM_UNIT_UNFORTIFY;
						player->command.unitUnfortify.unitId = unitId;
					}
					actionButtonBounds[unitsFounds%3].y += actionButtonBounds[0].height+margins;
					unitsFounds++;
					if(unitsFounds == building->unitsFortified) break;
					if(unitsFounds >= 8){
						sprintf(text, "+%i more", (building->unitsFortified - unitsFounds));
						DrawText(text, actionButtonBounds[2].x+4, actionButtonBounds[2].y+10, fontSize, textColor);
						break;
					}
				}
			}

		} break;
		case SEL_UNITS: {

			if(GuiButtonTile(actionButtonBounds[1], 805, "Hold your ground!", BLACK, false)){
				player->command.type = COM_UNITS_DEFEND;
			}
			actionButtonBounds[1].y += actionButtonBounds[0].height+margins;

			if(GuiButtonTile(actionButtonBounds[2], 826, "Disband unit. \n Can't be undone!", (Color){128,0,0,255}, false)){
				player->command.type = COM_UNITS_DISBAND;
			}
			actionButtonBounds[2].y += actionButtonBounds[0].height+margins;

			if(player->selectedUnitCount == 1){
				Unit_t* unit = GetUnit(player->selectedUnitIds[0]);
				UnitType_t* unitType = &UnitTypes[unit->type];
				if(!unit->active) break;
				descriptionHeader = (const char*)&unitType->name;

				sprintf(text, "HP: %i/%i", unit->health, unitType->maxHealth);
				DrawText(text, descriptionTextPos.x, descriptionTextPos.y, fontSize, textColor);
				
				if(unitType->attackDamage){
					sprintf(text, " Attack dmg: %i \n Recharge: %i \n Range: %i-%i \n Accuracy: %i", unitType->attackDamage, unitType->attackCharge, unitType->attackMinDistance, unitType->attackMaxDistance, unitType->attackAccuracy);
					DrawText(text, descriptionTextPos.x - 6, descriptionTextPos.y + fontSize + 6, smallFontSize, textColor);
				}
			}else{
				sprintf(text, "%i units", player->selectedUnitCount);
				descriptionHeader = (const char*)&text;

				//deselection box
				DrawLine(actionButtonBox.x+margins, actionButtonBox.y+100, actionButtonBox.x+actionButtonBox.width-margins, actionButtonBox.y+100, DARKGRAY);
				for(int i=0; i < 3; i++){
					actionButtonBounds[i].y = actionButtonBox.y+100+margins;
				}
				for(int i=0; i < player->selectedUnitCount; i++){
					Unit_t* unit = GetUnit(player->selectedUnitIds[i]);
					UnitType_t* unitType = &UnitTypes[unit->type];
					if(GuiButtonTile(actionButtonBounds[i%3], unitType->iconTileId, "deselect", BLACK, false)){
						player->command.type = COM_DESELECT_UNIT;
						player->command.deselectUnit.unitId = player->selectedUnitIds[i];
					}
					actionButtonBounds[i%3].y += actionButtonBounds[0].height+margins;
					if(i >= 8){
						DrawText("...", actionButtonBounds[2].x+10, actionButtonBounds[1].y+10, fontSize, textColor);
						break;
					}
				}
			}

		} break;
		case SEL_NONE: {
			Rectangle hireBox = {actionButtonBox.x+margins, actionButtonBox.y+margins, actionButtonBox.width-margins*2, 115};
			Rectangle buildBox = {actionButtonBox.x+margins, hireBox.y+hireBox.height+margins, actionButtonBox.width-margins*2, 160};
			Rectangle researchBox = {actionButtonBox.x+margins, buildBox.y+buildBox.height+margins, actionButtonBox.width-margins*2, 125};
			const UnitType_t* unitType;
			const BuildingType_t* buildingType;
			const char* hireTooltip;
			const char* researchTooltip;
			bool mayHire;
			bool mayBuild;
			bool mayResearch;
			int iconTileId;
			int techId;

			//================ description box =============

			if(game.devilPlayerId == client.localPlayerId){
				sprintf(text, " You are the devil. \n Your opponents are \n  not aware of that for now. \n Your mission is to destroy them. \n Hail Satan.");
			}else{
				sprintf(text, " There's four kings in this land. \n But one of them is the devil. \n You must find out who before \n the Ragnarok will ensue!");
			}
			DrawText(text, descriptionHeaderPos.x-5, descriptionHeaderPos.y, 10, textColor);

			if(game.devilPlayerId == client.localPlayerId){
				
				Rectangle bounds = {descriptionBox.x+margins, descriptionBox.y+descriptionBox.height-36, 32, 32};
				if(!game.devilRevealed){
					if(GuiButtonTile(bounds, 704, "Reveal that you are the devil. \n NOT REVERSIBLE.", RED, false)){
						player->command.type = COM_REVEAL_IDENTITY;
					}
				}else{
					GuiButtonTile(bounds, 853, "WHAT'S DONE iS DONE, YEAH?", RED, true);
				}

			}

			//================= hire =======================

			DrawText("Hire:", hireBox.x+30, hireBox.y+2, fontSize, textColor);
			for(int i=0; i < 3; i++){
				actionButtonBounds[i].y = hireBox.y+20;
			}

			//warrior
			unitType = &UnitTypes[UNIT_WARRIOR];
			mayHire = player->stats.buildingCountByType[BUILDING_BARRACKS];
			hireTooltip = unitType->description;
			if(GuiButtonTile(actionButtonBounds[0], unitType->iconTileId, hireTooltip, BLACK, !mayHire)){
				TryHiring(UNIT_WARRIOR, -1, -1);
			}
			actionButtonBounds[0].y += actionButtonBounds[0].height+margins;

			//mage
			unitType = &UnitTypes[UNIT_MAGE];
			mayHire = player->stats.buildingCountByType[BUILDING_ACADEMY];
			hireTooltip = unitType->description;
			if(GuiButtonTile(actionButtonBounds[1], unitType->iconTileId, hireTooltip, BLACK, !mayHire)){
				TryHiring(UNIT_MAGE, -1, -1);
			}
			actionButtonBounds[1].y += actionButtonBounds[0].height+margins;

			//knight
			unitType = &UnitTypes[UNIT_KNIGHT];
			mayHire = player->stats.buildingCountByType[BUILDING_BARRACKS]
				   && player->stats.technologiesResearched[TECH_IRON_ARMOR];
			hireTooltip = unitType->description;
			if(GuiButtonTile(actionButtonBounds[0], unitType->iconTileId, hireTooltip, BLACK, !mayHire)){
				TryHiring(UNIT_KNIGHT, -1, -1);
			}
			actionButtonBounds[0].y += actionButtonBounds[0].height+margins;

			//druid
			unitType = &UnitTypes[UNIT_DRUID];
			mayHire = player->stats.buildingCountByType[BUILDING_ORACLE]
				   && player->stats.technologiesResearched[TECH_ADVANCED_MAGIC];
			hireTooltip = unitType->description;
			if(GuiButtonTile(actionButtonBounds[1], unitType->iconTileId, hireTooltip, BLACK, !mayHire)){
				TryHiring(UNIT_DRUID, -1, -1);
			}
			actionButtonBounds[1].y += actionButtonBounds[0].height+margins;

			//================= build =======================

			DrawText("Build:", buildBox.x+30, buildBox.y+2, fontSize, textColor);
			for(int i=0; i < 3; i++){
				actionButtonBounds[i].y = buildBox.y+20;
			}

			//house
			buildingType = &BuildingTypes[BUILDING_HOUSE];
			mayBuild = true;
			if(GuiButtonTile(actionButtonBounds[0], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_HOUSE);
			}
			actionButtonBounds[0].y += actionButtonBounds[0].height+margins;
			
			//mine
			buildingType = &BuildingTypes[BUILDING_MINE];
			mayBuild = true;
			if(GuiButtonTile(actionButtonBounds[1], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_MINE);
			}
			actionButtonBounds[1].y += actionButtonBounds[0].height+margins;

			//barracks
			buildingType = &BuildingTypes[BUILDING_BARRACKS];
			mayBuild = true;
			if(GuiButtonTile(actionButtonBounds[2], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_BARRACKS);
			}
			actionButtonBounds[2].y += actionButtonBounds[0].height+margins;

			//outpost
			buildingType = &BuildingTypes[BUILDING_OUTPOST];
			mayBuild = true;
			if(GuiButtonTile(actionButtonBounds[0], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_OUTPOST);
			}
			actionButtonBounds[0].y += actionButtonBounds[0].height+margins;

			//the workshop
			buildingType = &BuildingTypes[BUILDING_WORKSHOP];
			mayBuild = true;
			if(GuiButtonTile(actionButtonBounds[1], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_WORKSHOP);
			}
			actionButtonBounds[1].y += actionButtonBounds[0].height+margins;

			//the academy
			buildingType = &BuildingTypes[BUILDING_ACADEMY];
			mayBuild = true;
			if(GuiButtonTile(actionButtonBounds[2], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_ACADEMY);
			}
			actionButtonBounds[2].y += actionButtonBounds[0].height+margins;

			//tower
			buildingType = &BuildingTypes[BUILDING_TOWER];
			mayBuild = player->stats.technologiesResearched[TECH_DEFENSIVE_TACTICS];
			if(GuiButtonTile(actionButtonBounds[0], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_TOWER);
			}
			actionButtonBounds[0].y += actionButtonBounds[0].height+margins;

			//oracle
			buildingType = &BuildingTypes[BUILDING_ORACLE];
			mayBuild = player->stats.technologiesResearched[TECH_ADVANCED_MAGIC];
			if(GuiButtonTile(actionButtonBounds[1], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_ORACLE);
			}
			actionButtonBounds[1].y += actionButtonBounds[0].height+margins;

			//factory
			buildingType = &BuildingTypes[BUILDING_FACTORY];
			mayBuild = player->stats.technologiesResearched[TECH_ADVANCED_TECHNOLOGY];
			if(GuiButtonTile(actionButtonBounds[2], buildingType->iconTileId, buildingType->description, BLACK, !mayBuild)){
				TryPlacingBuilding(BUILDING_FACTORY);
			}
			actionButtonBounds[2].y += actionButtonBounds[0].height+margins;

			//==================== research ========================

			DrawText("Research:", researchBox.x+30, researchBox.y+2, fontSize, textColor);
			for(int i=0; i < 3; i++){
				actionButtonBounds[i].y = researchBox.y+20;
			}

			DrawLineEx( (Vector2){actionButtonBounds[0].x+actionButtonBounds[0].width-5, actionButtonBounds[0].y + 16},
						(Vector2){actionButtonBounds[1].x+5, actionButtonBounds[1].y + 16},
						2, DARKGRAY);

			//defensive tactics
			techId = TECH_DEFENSIVE_TACTICS;
			iconTileId = 645;
			researchTooltip = "\n Defensive tactics. \n Allows building towers. \n Requires workshop. \n Not compatible with 'IRON ARMOR'";
			if(player->stats.technologiesResearched[TECH_IRON_ARMOR] || player->stats.technologiesProgress[TECH_IRON_ARMOR]){
				mayResearch = false;
			}else if(player->stats.technologiesResearched[techId]){
				mayResearch = false;
			}else if(player->stats.technologiesProgress[techId]){
				mayResearch = false;
				float progress = player->stats.technologiesProgress[techId] / (float)(TECH_RESEARCH_DURATION);
				DrawRectangle(actionButtonBounds[0].x, actionButtonBounds[0].y, (int)(progress*(float)actionButtonBounds[0].width), actionButtonBounds[0].height, GREEN);
			}else{
				mayResearch = (player->stats.buildingCountByType[BUILDING_WORKSHOP]);
			}
			if(GuiButtonTile(actionButtonBounds[0], iconTileId, researchTooltip, SKYBLUE, !mayResearch)){
				player->command.type = COM_RESEARCH;
				player->command.research.techId = techId;
			}
			if(player->stats.technologiesResearched[techId]){
				DrawRectangleLinesEx(actionButtonBounds[0], 2, GREEN);
			}
			actionButtonBounds[0].y += actionButtonBounds[0].height+margins;

			//iron armor
			techId = TECH_IRON_ARMOR;
			iconTileId = 740;
			researchTooltip = "\n Iron armor. \n Allows hiring knights. \n Requires workshop. \n Not compatible with 'DEFENSIVE TACTICS'.";
			if(player->stats.technologiesResearched[TECH_DEFENSIVE_TACTICS] || player->stats.technologiesProgress[TECH_DEFENSIVE_TACTICS]){
				mayResearch = false;
			}else if(player->stats.technologiesResearched[techId]){
				mayResearch = false;
			}else if(player->stats.technologiesProgress[techId]){
				mayResearch = false;
				float progress = player->stats.technologiesProgress[techId] / (float)(TECH_RESEARCH_DURATION);
				DrawRectangle(actionButtonBounds[1].x, actionButtonBounds[1].y, (int)(progress*(float)actionButtonBounds[1].width), actionButtonBounds[1].height, GREEN);
			}else{
				mayResearch = (player->stats.buildingCountByType[BUILDING_WORKSHOP]);
			}
			if(GuiButtonTile(actionButtonBounds[1], iconTileId, researchTooltip, SKYBLUE, !mayResearch)){
				player->command.type = COM_RESEARCH;
				player->command.research.techId = techId;
			}
			if(player->stats.technologiesResearched[techId]){
				DrawRectangleLinesEx(actionButtonBounds[1], 2, GREEN);
			}
			actionButtonBounds[1].y += actionButtonBounds[0].height+margins;

			DrawLineEx( (Vector2){actionButtonBounds[0].x+actionButtonBounds[0].width-5, actionButtonBounds[0].y + 16},
						(Vector2){actionButtonBounds[1].x+5, actionButtonBounds[1].y + 16},
						2, DARKGRAY);

			//advanced magic
			techId = TECH_ADVANCED_MAGIC;
			iconTileId = 816;
			researchTooltip = "\n Advanced magic. \n Allows to build the Oracle's Hut \n to hire druids and cast 'meteorite rain'. \n Requires academy. \n Not compatible with 'ADVANCED TECHNOLOGY'";
			if(player->stats.technologiesResearched[TECH_ADVANCED_TECHNOLOGY] || player->stats.technologiesProgress[TECH_ADVANCED_TECHNOLOGY]){
				mayResearch = false;
			}else if(player->stats.technologiesResearched[techId]){
				mayResearch = false;
			}else if(player->stats.technologiesProgress[techId]){
				mayResearch = false;
				float progress = player->stats.technologiesProgress[techId] / (float)(TECH_RESEARCH_DURATION);
				DrawRectangle(actionButtonBounds[0].x, actionButtonBounds[0].y, (int)(progress*(float)actionButtonBounds[0].width), actionButtonBounds[0].height, GREEN);
			}else{
				mayResearch = (player->stats.buildingCountByType[BUILDING_ACADEMY]);
			}
			if(GuiButtonTile(actionButtonBounds[0], iconTileId, researchTooltip, SKYBLUE, !mayResearch)){
				player->command.type = COM_RESEARCH;
				player->command.research.techId = techId;
			}
			if(player->stats.technologiesResearched[techId]){
				DrawRectangleLinesEx(actionButtonBounds[0], 2, GREEN);
			}
			actionButtonBounds[0].y += actionButtonBounds[0].height+margins;

			//advanced technology
			techId = TECH_ADVANCED_TECHNOLOGY;
			iconTileId = 1009;
			researchTooltip = "\n Advanced technology. \n Allows to build the factory \n and move much faster on water. \n Requires academy. \n Not compatible with 'ADVANCED MAGIC'.";
			if(player->stats.technologiesResearched[TECH_ADVANCED_MAGIC] || player->stats.technologiesProgress[TECH_ADVANCED_MAGIC]){
				mayResearch = false;
			}else if(player->stats.technologiesResearched[techId]){
				mayResearch = false;
			}else if(player->stats.technologiesProgress[techId]){
				mayResearch = false;
				float progress = player->stats.technologiesProgress[techId] / (float)(TECH_RESEARCH_DURATION);
				DrawRectangle(actionButtonBounds[1].x, actionButtonBounds[1].y, (int)(progress*(float)actionButtonBounds[1].width), actionButtonBounds[1].height, GREEN);
			}else{
				mayResearch = (player->stats.buildingCountByType[BUILDING_ACADEMY]);
			}
			if(GuiButtonTile(actionButtonBounds[1], iconTileId, researchTooltip, SKYBLUE, !mayResearch)){
				player->command.type = COM_RESEARCH;
				player->command.research.techId = techId;
			}
			if(player->stats.technologiesResearched[techId]){
				DrawRectangleLinesEx(actionButtonBounds[1], 2, GREEN);
			}
			actionButtonBounds[1].y += actionButtonBounds[0].height+margins;


		} break;
	}

	DrawText(descriptionHeader, descriptionHeaderPos.x, descriptionHeaderPos.y, fontSize, textColor);
	DrawText(descriptionText, descriptionTextPos.x, descriptionTextPos.y, smallFontSize, textColor);

}

static void HandleSelection(bool isHoveringUI){
	Vector2 actionButtonsAt = { client.width - 170, client.halfHeight + 50 };
	Rectangle buttonBounds = {actionButtonsAt.x + 5, actionButtonsAt.y + 5, 64, 64};
	Player_t* player = GetPlayer(client.localPlayerId);
	World_t* world = GetWorld(client.currentWorldId);
	Color selectionColor = (Color){200, 145, 80, 210};

	if(client.ui.isPlacingBuilding) return;
	if(client.ui.buildingJustPlaced){
		if(IsMouseButtonDown(MOUSE_LEFT_BUTTON) || IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
			return;
		}else{
			client.ui.buildingJustPlaced = 0;
			return;
		}
	}

	bool lmbPressed = IsMouseButtonPressed(MOUSE_LEFT_BUTTON);
	bool lmbUp = IsMouseButtonReleased(MOUSE_LEFT_BUTTON);
	bool click = ((!isHoveringUI) && lmbUp);
	Vector2 mouseScreenPos = GetMousePosition();
	Vector2 cursor = GetScreenToWorld2D(mouseScreenPos, client.cam);
	if(lmbPressed){
		client.ui.isDragging = true;
		client.ui.dragOrigin = cursor;
	}else if(lmbUp){
		client.ui.isDragging = false;
	}
	Rectangle selection = { client.ui.dragOrigin.x,
							client.ui.dragOrigin.y,
							cursor.x - client.ui.dragOrigin.x,
							cursor.y - client.ui.dragOrigin.y };
	if(selection.width < 0){
		selection.width *= -1;
		selection.x -= selection.width;
	}
	if(selection.height < 0){
		selection.height *= -1;
		selection.y -= selection.height;
	}
	
	int selectionBounds[] = {
		(int)(selection.x / TILE_SIZE),
		(int)(selection.y / TILE_SIZE),
		(int)ceilf(selection.width / TILE_SIZE),
		(int)ceilf(selection.height / TILE_SIZE)
	};
	if(selectionBounds[2] < 1) selectionBounds[2] = 1;
	if(selectionBounds[3] < 1) selectionBounds[3] = 1;

	if(client.ui.isDragging){
		BeginMode2D(client.cam);
		DrawRectangleLinesEx(selection, 1, selectionColor);
		EndMode2D();
	}

	switch(player->selection){
		case SEL_BUILDING: {
			World_t* world = GetWorld(player->selectedBuldingWorld);
			Building_t* building = &world->buildings[player->selectedBuildingId];
			BuildingType_t* buildingType = &BuildingTypes[building->type];
			
			if(!building->active){
				player->selection = SEL_NONE;
				break;
			}
			
			if(player->selectedBuldingWorld == client.currentWorldId){
				BeginMode2D(client.cam);
				DrawRectangleLinesEx((Rectangle){
					building->pos[0]*TILE_SIZE,
					(0.5f + building->pos[1])*TILE_SIZE,
					(building->pixmap ? building->pixmap->width : 1)*TILE_SIZE,
					(building->pixmap ? (building->pixmap->height-1)*TILE_SIZE : 6)
				}, 2, selectionColor);
				EndMode2D();
			}
		} break;
		case SEL_UNITS: {
			
			int fortifyingOffset = 0;
			BeginMode2D(client.cam);
			for(int i=0; i < player->selectedUnitCount; i++){
				int unitId = player->selectedUnitIds[i];
				Unit_t* unit = GetUnit(unitId);
				if(!unit->active) continue;
				if(unit->worldId != client.currentWorldId) continue;

				if(unit->fortifiedBuildingId == -1){
					// DrawRectangleLinesEx((Rectangle){
					// 	unit->pos[0]*TILE_SIZE,
					// 	(unit->pos[1]+0.75f)*TILE_SIZE,
					// 	TILE_SIZE,
					// 	6
					// }, 2, selectionColor);
					DrawTilePro(788, unit->pos[0], unit->pos[1], 0, 0, 0, selectionColor);

				}else{
					Building_t* building = &world->buildings[unit->fortifiedBuildingId];
					Vector2 iconPos = {building->pos[0]*TILE_SIZE + fortifyingOffset, building->pos[1]*TILE_SIZE};
					DrawTileEx(611, iconPos, 0, 0.8f, selectionColor);
					fortifyingOffset += 3;
				}

			}
			EndMode2D();

			if(IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)){
				int dest[2] = { (int)(cursor.x / TILE_SIZE),
								(int)(cursor.y / TILE_SIZE) };
				player->command.type = COM_UNITS_MOVE;
				player->command.moveUnits.worldId = client.currentWorldId;
				player->command.moveUnits.dest[0] = dest[0];
				player->command.moveUnits.dest[1] = dest[1];
				SpawnParticles(EFFECT_ICON, client.currentWorldId, cursor, Vector2Zero(), 745);
				char* highlight = &client.worldCellHighlight[dest[0]][dest[1]];
				*highlight = (((int)*highlight+100) > 255) ? 255 : (*highlight+100);
			}
		} break;
	}

	if(click){
		player->command.type = COM_SELECT;
		memcpy(&player->command.select.selectionBounds, selectionBounds, sizeof(selectionBounds));
		player->command.select.worldId = client.currentWorldId;
	}

}

static void DrawTopBar(){
	char text[2048];
	Player_t* localPlayer = GetPlayer(client.localPlayerId);

	DrawRectangle(0, 0, client.width, topBarHeight, LIGHTGRAY);

	if(GuiButtonTile((Rectangle){10, 5, 40, 40}, 787, "Toggle fullscreen", BROWN, false)){
		ToggleFullscreen();
		app.config.fullscreen = 1 - app.config.fullscreen;
	}

	Rectangle volumeSlider = {110, 5, 80, 30};
	if(IsMouseInRect(volumeSlider)){
		app.config.soundVolume = GuiSlider(volumeSlider, "", "", app.config.soundVolume, 0, 1);
		tooltipText = "Audio volume";
	}else{
		DrawTileEx(918, (Vector2){volumeSlider.x, volumeSlider.y}, 0, 2, BROWN);
	}

	if(GuiButtonTile((Rectangle){60, 5, 40, 40}, app.config.mouseEdgeSlide ? 691 : 824, "Slide cam on screen edges", BROWN, false)){
		app.config.mouseEdgeSlide = 1 - app.config.mouseEdgeSlide;
	}

	if(GuiButtonTile((Rectangle){200, 3, 40, 40}, 927, "Zoom out (-)", BROWN, false) || IsKeyPressed(KEY_MINUS) || IsKeyPressed(KEY_KP_SUBTRACT)){
		client.cam.zoom -= 0.25f;
	}
	if(GuiButtonTile((Rectangle){245, 3, 40, 40}, 926, "Zoom in (+)", BROWN, false) || IsKeyPressed(KEY_EQUAL) || IsKeyPressed(KEY_KP_ADD)){
		client.cam.zoom += 0.25f;
	}

	//world list
	Rectangle worldButtonBounds = {client.halfWidth - 100, 5, 50, 50};
	DrawText("Realms:", worldButtonBounds.x - 65, 10, 16, DARKGRAY);
	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = GetPlayer(playerId);
		if(!player->active) continue;
		if(player->ownWorldId == -1) continue;
		World_t* world = GetWorld(player->ownWorldId);
		if(!world->active) continue;

		bool isTheDevil = game.devilRevealed && (playerId == game.devilPlayerId);

		float scale = 1.25f;
		worldButtonBounds.width = world->width * scale;
		worldButtonBounds.height = world->height * scale;

		Rectangle actualButtonBounds = {worldButtonBounds.x, worldButtonBounds.y+25, worldButtonBounds.width, worldButtonBounds.height-25};
		if( GuiButtonTile(actualButtonBounds, 0, "Click to switch to this world", WHITE, false)
		 || (IsKeyPressed(KEY_ONE + player->ownWorldId)) ){

			client.currentWorldId = player->ownWorldId;
		}

		DrawRectangleRec((Rectangle){worldButtonBounds.x-2, worldButtonBounds.y-2, worldButtonBounds.width, topBarHeight-worldButtonBounds.y+2}, isTheDevil ? RED : DARKGRAY);
		DrawTextureEx(client.worldMinimaps[player->ownWorldId], (Vector2){worldButtonBounds.x, worldButtonBounds.y}, 0, scale, WHITE);
		
		DrawEllipse(worldButtonBounds.x + 10, worldButtonBounds.y + 7, 20, 18, LIGHTGRAY);
		int avatarTileId = 346;
		if(isTheDevil){
			avatarTileId = 281;
		}
		if(player->hasLost){
			avatarTileId = 0;
		}
		DrawTileEx(avatarTileId, (Vector2){worldButtonBounds.x-3, worldButtonBounds.y-4}, 0, 1.8f, GRAY);
		if(player->ownWorldId == client.currentWorldId){
			DrawRectangleLines(worldButtonBounds.x-1, worldButtonBounds.y-1, worldButtonBounds.width+2, worldButtonBounds.height+2, WHITE);
		}
		DrawTileEx(avatarTileId, (Vector2){worldButtonBounds.x-2, worldButtonBounds.y-3}, 0, 1.8f, player->color);
		sprintf(text, "%i", player->ownWorldId+1);
		DrawText(text, worldButtonBounds.x-2, worldButtonBounds.y-3, 12, BLACK);

		DrawRectangle(worldButtonBounds.x, worldButtonBounds.y+worldButtonBounds.height-20, worldButtonBounds.width, 10, BLACK);
		DrawText(player->name, worldButtonBounds.x+5, worldButtonBounds.y+worldButtonBounds.height-20, 10, BEIGE);
		const char* bottomText = NULL;
		if(player->hasLost){
			bottomText = "(Dead)";
		}else if(playerId == client.localPlayerId){
			bottomText = "(You)";
		}else if(isTheDevil){
			bottomText = "DEVIL";
		}
		if(bottomText){
			DrawRectangle(worldButtonBounds.x, worldButtonBounds.y+worldButtonBounds.height-10, 45, 10, BLACK);
			DrawText(bottomText, worldButtonBounds.x+5, worldButtonBounds.y+worldButtonBounds.height-10, 10, LIGHTGRAY);
		}

		if(playerId != client.localPlayerId){
			//Vector2 relationsIconPos = {worldButtonBounds.x+worldButtonBounds.width-28, worldButtonBounds.y+worldButtonBounds.height-5};
			Vector2 relationsIconPos = {worldButtonBounds.x+worldButtonBounds.width-22, worldButtonBounds.y+3};
			//DrawEllipse(relationsIconPos.x, relationsIconPos.y, 25, 25, LIGHTGRAY);
			int buttonTileId;
			Color buttonColor;
			const char* buttonTooltip;
			int autoAttack = localPlayer->stats.autoAttackPlayer[playerId];
			if(autoAttack){
				buttonTileId = 758;
				buttonColor = RED;
				buttonTooltip = "Attack on sight";
			}else{
				buttonTileId = 727;
				buttonColor = WHITE;
				buttonTooltip = "Friendly";
			}
			if(GuiButtonTile((Rectangle){relationsIconPos.x, relationsIconPos.y, 20, 20}, buttonTileId, buttonTooltip, buttonColor, false)){
				localPlayer->command.type = COM_TOGGLE_AUTOATTACK;
				localPlayer->command.toggleAutoAttack.otherPlayerId = playerId;
			}
		}

		worldButtonBounds.x += worldButtonBounds.width + 5;
	}


	//status
	// switch(game.state){
	// }
	int ticksPerMinute = 3600 / ((60 / GET_WALL_TICKRATE()));
	int minutes = (game.timer / ticksPerMinute);
	int seconds = (game.timer - (minutes * ticksPerMinute)) / GET_WALL_TICKRATE();
	int textlen = sprintf(text, "%02i:%02i", minutes, seconds);
	DrawText(text, (client.width - 20 - textlen*16), 16, 24, BLACK);
}

static bool HandleBuildingPlacement(bool isHoveringMap, int cursor[2]){
	if(!client.ui.isPlacingBuilding) return false;
	Player_t* player = GetPlayer(client.localPlayerId);
	bool lmb = IsMouseButtonPressed(MOUSE_LEFT_BUTTON);
	if(IsKeyPressed(KEY_ESCAPE) || (lmb && (!isHoveringMap) || IsMouseButtonPressed(MOUSE_RIGHT_BUTTON))){
		client.ui.isPlacingBuilding = 0;
		return false;
	}
	BeginMode2D(client.cam);
	const BuildingType_t* buildingType = &BuildingTypes[client.ui.typeOfPlacedBuilding];
	bool placementPossible = CanBuildAtLocation(client.currentWorldId, client.localPlayerId, client.ui.typeOfPlacedBuilding, cursor[0], cursor[1]);
	Color ghostClr = placementPossible ? (Color){0,255,0,100} : (Color){255,0,0,100};
	Vector2 ghostPos = (Vector2){TILE_SIZE*cursor[0], TILE_SIZE*cursor[1]};
	if(buildingType->pixmap){
		DrawPixmapEx(buildingType->pixmap, ghostPos, ghostClr);
	}else{
		DrawTileEx(buildingType->singleTile, ghostPos, 0, 1, ghostClr);
	}
	EndMode2D();
	if(lmb){
		if(!placementPossible){
			strcpy(client.ui.bottomText, "Can't build here.");
			DisplayBottomText();
		}else{
			player->command.type = COM_BUILD;
			player->command.build.buildingType = client.ui.typeOfPlacedBuilding;
			player->command.build.worldId = client.currentWorldId;
			player->command.build.pos[0] = cursor[0];
			player->command.build.pos[1] = cursor[1];
			client.ui.isPlacingBuilding = 0;
			return true;
		}
	}
	return false;
}

static void DrawRolodex(){
	char text[1024];
	Player_t* player = GetPlayer(client.localPlayerId);
	Vector2 pos = {client.halfWidth-150, client.height-8};
	if(!player->stats.unitsInDeckTotal) return;
	DrawRectangle(pos.x, pos.y, 200, 5, GRAY);
	// for(int i=0;i < 5;i++){
	// 	DrawTileEx(532+i, (Vector2){pos.x+5+i*30, pos.y-32}, 0, 2, WHITE);
	// }
	Rectangle bounds = {pos.x, pos.y - 50, 40, 40};
	for(int unitTypeId=0; unitTypeId < _UNIT_TYPE_COUNT; unitTypeId++){
		UnitType_t* unitType = &UnitTypes[unitTypeId];
		if(!player->stats.unitsInDeckCount[unitTypeId]) continue;

		const char* tooltip;
		if(client.currentWorldId == player->ownWorldId){
			tooltip = "Send unit to this world. \n (will appear on top)";
		}else{
			tooltip = "Send unit to this world. \n (will appear at the bottom)";
		}
		if(GuiButtonTile(bounds, unitType->iconTileId, tooltip, BLACK, false)){
			player->command.type = COM_SUMMON_FROM_DECK;
			player->command.summonFromDeck.worldId = client.currentWorldId;
			player->command.summonFromDeck.unitType = unitTypeId;
		}
		int len = sprintf(text, "%i", player->stats.unitsInDeckCount[unitTypeId]);
		DrawText(text, bounds.x+3, bounds.y+3, 12, WHITE);
		DrawText(text, bounds.x+4, bounds.y+4, 12, BLACK);
		bounds.x += bounds.width + 5;
	}

	if(player->stats.unitsInDeckTotal > client.ui.lastDeckTotal){
		SpawnParticles(EFFECT_CARDS, -1, GetScreenToWorld2D((Vector2){client.halfWidth, client.height-40}, client.cam), Vector2Zero(), 3);
	}
	client.ui.lastDeckTotal = player->stats.unitsInDeckTotal;
}



static void DrawDebugMenu(){
	char text[8192];
	int fontSize = 12;
	Vector2 pos = {50, 200};

	sprintf(text, "%i.", game.devilPlayerId);
	DrawText(text, pos.x, pos.y, fontSize, WHITE);
	pos.y += fontSize;

	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = GetPlayer(playerId);
		if(!player->active) continue;

		sprintf(text, "%i. %s.  b=%i", playerId, player->name, player->isBot);
		DrawText(text, pos.x, pos.y, fontSize, WHITE);
		pos.y += fontSize;
		if(player->isBot){
			for(int otherPlayerId=0; otherPlayerId < MAX_PLAYERS; otherPlayerId++){
				AI_Perception_t* perc = &player->ai.perception[otherPlayerId];
				sprintf(text, "perc -> %i:  rel: %i. att: %i. def: %i. tres: %i", otherPlayerId, perc->relation, perc->attackPower, perc->defensePower, perc->trespassing);
				DrawText(text, pos.x + 20, pos.y, fontSize, WHITE);
				pos.y += fontSize;
				if(pos.y > client.height){
					pos.y = 200;
					pos.x += 200;
				}
			}

		}
	}

}

static void DrawEndScreen(){
	DrawRectangle(0, 0, client.width, client.height, BLACK);

	const char* text = NULL;
	if(client.localPlayerId == game.devilPlayerId){
		if(game.devilHasWon){
			text = "   Victory.\n\n The world is yours.";
		}else{
			text = "   Defeat.\n\n You evil plans have failed and you go back to hell.";
		}
	}else{
		if(game.devilHasWon){
			text = "    Victory!\n\n You have successfully stopped \n the forces of evil!";
		}else{
			text = "   Defeat.\n\n As you were not able to defeat the devil \n the whole world is now destroyed or something.";
		}
	}
	

	if(!text) return;
	Vector2 size = MeasureTextEx(GetFontDefault(), text, 30, 1.0f);
	DrawText(text, client.halfWidth-size.x/2, client.halfHeight-size.y/2, 30, WHITE);
}

void DrawUI(){
	World_t* world = GetWorld(client.currentWorldId);
	Vector2 mousePos = GetMousePosition();
	Vector2 worldPos = GetScreenToWorld2D(mousePos, client.cam);
	int cursor[] = {(int)(worldPos.x / TILE_SIZE), (int)(worldPos.y / TILE_SIZE)};
	bool isHoveringMap = TileInBounds(world, cursor[0], cursor[1]);
	bool isHoveringUI = false;

	if(IsKeyDown(KEY_F4)){
		app.config.tickrate = DEFAULT_TICKRATE*10;
	}else if(IsKeyReleased(KEY_F4)){
		app.config.tickrate = DEFAULT_TICKRATE;
	}

	if(game.state == STATE_ENDED){
		DrawEndScreen();
		return;
	}

	if(IsKeyPressed(KEY_F3)){
		client.ui.debugMenu = 1 - client.ui.debugMenu;
	}
	if(client.ui.debugMenu){
		DrawDebugMenu();
	}

	if(client.ui.bottomTextTimer){
		int x = client.width / 10;
		int y = client.height - (client.height / 10) - 50;
		float opacity = 1.0f;
		float delta = 20 * client.deltaTime;
		if(client.ui.bottomTextFrames < 5) opacity *= client.ui.bottomTextFrames / 5.0f;
		else if(client.ui.bottomTextTimer < 60) opacity *= client.ui.bottomTextTimer / 60.0f;
		
		DrawText(client.ui.bottomText, x-1, y-1, 20, Fade(BLACK, opacity));
		DrawText(client.ui.bottomText, x, y, 20, Fade(RED, opacity));
		
		client.ui.bottomTextFrames += delta;
		client.ui.bottomTextTimer -= delta;
	}

	//right panel
	if(mousePos.x >= (client.width - rightPanelWidth)){
		isHoveringMap = false;
		isHoveringUI = true;
	}
	//top bar
	if(mousePos.y <= topBarHeight){
		isHoveringMap = false;
		isHoveringUI = true;
	}

	myUpdateCamera(isHoveringUI);

	// char text[1024];
	// sprintf(text, "cursor: %i %i", cursor[0], cursor[1]);
	// DrawText(text, 20, 60, 20, WHITE);

	if(IsKeyPressed(KEY_F5)){
		app.shouldRestartRound = 1;
	}

	HandleSelection(isHoveringUI);
	if(HandleBuildingPlacement(isHoveringMap, cursor)){
		client.ui.buildingJustPlaced = 1;
	}

	float fade = 0;
	if(game.state == STATE_STARTING){
		fade = (game.timer > 30) ? 1 : (game.timer / 30.0f);
	}
	if(fade != 0){
		DrawRectangle(0, 0, client.width, client.height, (Color){0, 0, 0, (char)((fade) * 255)});
	}

	//right panel
	DrawRightPanel();

	DrawTopBar();

	//the card stack
	DrawRolodex();

	DrawTooltip();

}

