#pragma once
#include "capacities.h"

typedef enum{
	AI_STATE_CALM,
	AI_STATE_DEFENSE,
	AI_STATE_ATTACK
} AI_State_e;

typedef enum{
	RELATION_NEUTRAL,
	RELATION_ENEMY
} AI_Relation_e;

typedef struct{
	AI_Relation_e relation;
	int attackPower;
	int defensePower;
	int trespassing;
} AI_Perception_t;

typedef struct{
	int initialized;
	AI_Perception_t perception[MAX_PLAYERS];
	int perceptionUpdateTimer;
	int tacticsUpdateTimer;
	AI_State_e state;
	int mainEnemyPlayerId;
} PlayerBotState_t;

void PlayerAI(int playerId);
