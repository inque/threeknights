#include "players.h"
#include "gameapp.h"
#include "string.h"
#include "stdlib.h"
#include "playerbotlogic.h"

static const char* PlayerNames[] = {
	"Kadum",
	"Rodip",
	"Nalbey",
	"Depohol",
	"Zilodzip",
	"Deyonvog",
	"Nudrep",
	"Olkyss"
};

int CreatePlayer(int isBot){

	Color PlayerColors[] = {
		PINK,
		GOLD,
		LIME,
		SKYBLUE,
		VIOLET,
		MAROON,
		DARKGRAY,
		BEIGE
	};

	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = GetPlayer(playerId);
		if(player->active) continue;

		player->active = 1;
		player->isBot = isBot;
		if(isBot){
			memset(&player->ai, 0, sizeof(player->ai));
		}
		strncpy(player->name, PlayerNames[(playerId + app.gamesPlayed) % 8], sizeof(player->name));
		player->color = PlayerColors[(playerId + app.gamesPlayed) % 8];
		player->color.r = (255*2+player->color.r)/3;
		player->color.g = (255*2+player->color.g)/3;
		player->color.b = (255*2+player->color.b)/3;
		ClearPlayerStats(playerId);
		return playerId;
	}
	return -1;
}

void ClearPlayerStats(int playerId){
	Player_t* player = GetPlayer(playerId);

	player->hasLost = 0;
	player->selection = SEL_NONE;
	player->command.type = COM_NONE;
	memset(&player->stats, 0, sizeof(player->stats));
}

void RemovePlayer(int playerId){
	Player_t* player = GetPlayer(playerId);
	player->active = 0;
}

void RemoveBots(){
	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = GetPlayer(playerId);
		if(!player->active) continue;
		if(player->isBot){
			RemovePlayer(playerId);
		}
	}
}

void RegisterPlayerAttack(int srcPlayerId, int dstPlayerId){
	Player_t* srcPlayer = GetPlayer(srcPlayerId);
	Player_t* dstPlayer = GetPlayer(dstPlayerId);
	if(srcPlayerId == dstPlayerId) return;
	if(dstPlayerId == -1) return;
	if(srcPlayerId != -1){
		dstPlayer->stats.autoAttackPlayer[srcPlayerId] = 1;
		srcPlayer->stats.autoAttackPlayer[dstPlayerId] = 1;
		if(dstPlayer->isBot){
			dstPlayer->ai.perception[srcPlayerId].relation = RELATION_ENEMY;
			if(dstPlayer->ai.mainEnemyPlayerId == -1){
				dstPlayer->ai.mainEnemyPlayerId = srcPlayerId;
			}
		}
	}
}

void RegisterDevilReveal(){
	game.devilRevealed = 1;
	Player_t* devilPlayer = GetPlayer(game.devilPlayerId);
	devilPlayer->color = RED;
	for(int unitId = 0; unitId < MAX_UNITS; unitId++){
		Unit_t* unit = GetUnit(unitId);
		if(!unit->active) continue;
		if(unit->playerId != game.devilPlayerId) continue;
		if(unit->type != UNIT_KING) continue;
		
		SpawnUnit(UNIT_DEVIL, game.devilPlayerId, unit->worldId, unit->pos[0], unit->pos[1]);
		RemoveUnit(unitId);
		break;
	}

	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = GetPlayer(playerId);
		if((!player->active) || (player->hasLost)) continue;

		if(playerId == game.devilPlayerId){
			for(int i=0; i < MAX_PLAYERS; i++){
				player->stats.autoAttackPlayer[i] = 1;
			}
		}else{
			for(int i=0; i < MAX_PLAYERS; i++){
				player->stats.autoAttackPlayer[i] = (i == game.devilPlayerId);
				if(player->isBot){
					player->ai.perception->relation = (i == game.devilPlayerId) ? RELATION_ENEMY : RELATION_NEUTRAL;
				}
			}
			if(player->isBot){
				player->ai.mainEnemyPlayerId = game.devilPlayerId;
				player->ai.state = AI_STATE_ATTACK;	
			}
		}
	}


}

static bool MayBuildingHire(BuildingType_e building, UnitType_e unit){
	//if(building == BUILDING_CASTLE) return true;
	switch(unit){
		case UNIT_MAGE:
			return (building == BUILDING_ACADEMY);
		case UNIT_DRUID:
			return (building == BUILDING_ORACLE);
		case UNIT_WARRIOR:
		case UNIT_KNIGHT:
			return (building == BUILDING_BARRACKS);
	}
	return false;
}

bool MayPlayerResearch(int playerId, int techId){
	Player_t* player = GetPlayer(playerId);
	if(player->stats.technologiesResearched[techId]) return false;
	if(player->stats.technologiesProgress[techId]) return false;

	bool allow = false;
	switch(techId){
		case TECH_DEFENSIVE_TACTICS:
			allow =    (!player->stats.technologiesResearched[TECH_IRON_ARMOR])
					&& (!player->stats.technologiesProgress[TECH_IRON_ARMOR])
					&& player->stats.buildingCountByType[BUILDING_WORKSHOP];
			break;
		case TECH_IRON_ARMOR:
			allow =    (!player->stats.technologiesResearched[TECH_DEFENSIVE_TACTICS])
					&& (!player->stats.technologiesProgress[TECH_DEFENSIVE_TACTICS])
					&& player->stats.buildingCountByType[BUILDING_WORKSHOP];
			break;
		case TECH_ADVANCED_MAGIC:
			allow =    (!player->stats.technologiesResearched[TECH_ADVANCED_TECHNOLOGY])
					&& (!player->stats.technologiesProgress[TECH_ADVANCED_TECHNOLOGY])
					&& player->stats.buildingCountByType[BUILDING_ACADEMY];
			break;
		case TECH_ADVANCED_TECHNOLOGY:
			allow =    (!player->stats.technologiesResearched[TECH_ADVANCED_MAGIC])
					&& (!player->stats.technologiesProgress[TECH_ADVANCED_MAGIC])
					&& player->stats.buildingCountByType[BUILDING_ACADEMY];
			break;
	}
	return allow;
}

void UpdatePlayers(){
	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = GetPlayer(playerId);
		World_t* world = GetWorld(player->ownWorldId);
		if(!player->active) continue;
		if(player->hasLost) continue;

		if(player->isBot){
			PlayerAI(playerId);
		}

		if(playerId == game.devilPlayerId){
			if(player->stats.minionSpawnTimer){
				player->stats.minionSpawnTimer--;
			}else{
				player->stats.minionSpawnTimer = 30*GET_WALL_TICKRATE();
			}
			player->stats.minionCount++;
		}

		if((player->stats.unitCount < player->stats.food) && (player->stats.unitCount < MAX_UNITS_PER_PLAYER)){
			if(!player->stats.spawnPeasantTimer){
				int x = (rand() % (world->width - 4)) + 2;
				int peasantId = SpawnUnit(UNIT_PEASANT, playerId, player->ownWorldId, x, 2);
				player->stats.spawnPeasantTimer = PEASANT_SPAWN_RATE;
			}else{
				player->stats.spawnPeasantTimer--;
			}
		}

		for(int i=0; i < _TECH_COUNT; i++){
			if(player->stats.technologiesProgress[i]){
				if(++player->stats.technologiesProgress[i] > TECH_RESEARCH_DURATION){
					player->stats.technologiesProgress[i] = 0;
					player->stats.technologiesResearched[i] = 1;
				}
			}
		}

		switch(player->command.type){
			case COM_BUILD: {
				BuildingType_t* buildingType = &BuildingTypes[player->command.build.buildingType];
				if(buildingType->isUnique && player->stats.buildingCountByTypeCountingUnbuilt[player->command.build.buildingType]){
					break;
				}
				if(CanBuildAtLocation(player->command.build.worldId, playerId,
									  player->command.build.buildingType,
									  player->command.build.pos[0],
									  player->command.build.pos[1])){
					int newBuildingId = CreateBuilding(player->command.build.worldId, playerId,
													   player->command.build.buildingType,
													   player->command.build.pos[0],
													   player->command.build.pos[1], false);
					if(newBuildingId != -1){
						player->stats.gold -= buildingType->priceGold;
					}
				}
			} break;
			case COM_SELECT: {
				int selectionBounds[4];
				memcpy(&selectionBounds, player->command.select.selectionBounds, sizeof(selectionBounds));
				World_t* targetWorld = GetWorld(player->command.select.worldId);

				player->selection = SEL_NONE;
				player->selectedUnitCount = 0;
				
				for(int unitId=0; unitId < MAX_UNITS; unitId++){
					Unit_t* unit = GetUnit(unitId);
					if((!unit->active) || (unit->playerId != client.localPlayerId)) continue;
					if(unit->worldId != player->command.select.worldId) continue;
					if(unit->fortifiedBuildingId != -1) continue;

					if((selectionBounds[0]+selectionBounds[2]) <= unit->pos[0]) continue;
					if((selectionBounds[1]+selectionBounds[3]) <= unit->pos[1]) continue;
					if(selectionBounds[0] > unit->pos[0]) continue;
					if(selectionBounds[1] > unit->pos[1]) continue;

					player->selection = SEL_UNITS;
					player->selectedUnitIds[player->selectedUnitCount++] = unitId;
				}
				if(player->selection == SEL_UNITS) return;

				for(int buildingId = 0; buildingId < MAX_BUILDINGS; buildingId++){
					Building_t* building = &targetWorld->buildings[buildingId];
					if(!building->active) continue;
					if(building->playerId != playerId) continue;
					if((selectionBounds[0]+selectionBounds[2]) < building->pos[0]) continue;
					if((selectionBounds[1]+selectionBounds[3]) < building->pos[1]) continue;
					if(building->pixmap){
						if((selectionBounds[0]) >= building->pixmap->width+building->pos[0]) continue;
						if((selectionBounds[1]) >= building->pixmap->height+building->pos[1]) continue;
					}else{
						if((selectionBounds[0]) > building->pos[0]) continue;
						if((selectionBounds[1]) > building->pos[1]) continue;
					}
					
					player->selection = SEL_BUILDING;
					player->selectedBuildingId = buildingId;
					player->selectedBuldingWorld = player->command.select.worldId;
					break;
				}
			} break;
			case COM_AI_MOVE_UNIT: {
				int targetUnitId = player->command.aiMoveUnit.unitId;
				Unit_t* targetUnit = GetUnit(targetUnitId);
				if((!targetUnit->active) || (targetUnit->playerId != playerId)) break;

				if(player->command.aiMoveUnit.softly){
					targetUnit->attentionTimer = 51;
					targetUnit->walkingDest[0] = player->command.aiMoveUnit.dest[0];
					targetUnit->walkingDest[1] = player->command.aiMoveUnit.dest[1];
					break;
				}else{
					int worldId = player->command.aiMoveUnit.worldId;
					int dest[2];
					memcpy(&dest, player->command.aiMoveUnit.dest, sizeof(dest));
					
					player->selection = SEL_UNITS;
					player->selectedUnitCount = 1;
					player->selectedUnitIds[0] = targetUnitId;
					player->command.type = COM_UNITS_MOVE;
					player->command.moveUnits.worldId = worldId;
					memcpy(&player->command.moveUnits.dest, &dest, sizeof(dest));
					//...
				}
			}
			case COM_UNITS_MOVE: {
				World_t* world = GetWorld(player->command.moveUnits.worldId);
				int buildingId = GetBuildingOnTile(world, player->command.moveUnits.dest[0], player->command.moveUnits.dest[1], false);
				int unitOnTileId = GetUnitOnTile(player->command.moveUnits.worldId, player->command.moveUnits.dest[0], player->command.moveUnits.dest[1]);

				for(int i=0; i < player->selectedUnitCount; i++){
					int unitId = player->selectedUnitIds[i];
					Unit_t* unit = GetUnit(unitId);
					UnitType_t* unitType = &UnitTypes[unit->type];
					bool sameWorld = (unit->worldId == player->command.moveUnits.worldId);
					
					if(!unit->active) continue;
					if(sameWorld){
						if(buildingId == -1){
							if(unitOnTileId == -1){
								unit->action.type = UNIT_ACTION_WALKING;
								unit->action.walking.dest[0] = player->command.moveUnits.dest[0];
								unit->action.walking.dest[1] = player->command.moveUnits.dest[1];
							}else{
								Unit_t* unitOnTile = GetUnit(unitOnTileId);
								if(unitOnTile->playerId != playerId){
									if(!unitType->attackDamage) continue;
									unit->action.type = UNIT_ACTION_ATTACK_UNIT;
									unit->action.attackUnit.targetUnitId = unitOnTileId;
								}else{
									unit->action.type = UNIT_ACTION_WALKING;
									unit->action.walking.dest[0] = player->command.moveUnits.dest[0];
									unit->action.walking.dest[1] = player->command.moveUnits.dest[1];
								}
							}
						}else{
							Building_t* building = &world->buildings[buildingId];
							if(building->playerId == playerId){
								if((!building->isBuilt) && (!unitType->canConstructBuildings)) continue;
								unit->action.type = UNIT_ACTION_FORTIFY;
								unit->action.fortify.worldId = player->command.moveUnits.worldId;
								unit->action.fortify.buildingId = buildingId;
							}else{
								if(!unitType->attackDamage) continue;
								unit->action.type = UNIT_ACTION_ATTACK_BUILDING;
								unit->action.attackBuilding.worldId = player->command.moveUnits.worldId;
								unit->action.attackBuilding.buildingId = buildingId;
							}
						}
					}else{
						World_t* unitWorld = GetWorld(unit->worldId);
						unit->action.type = UNIT_ACTION_WALKING;
						unit->action.walking.dest[0] = unit->pos[0];
						unit->action.walking.dest[1] = unitWorld->height - 2;
					}
				}
			} break;
			case COM_SUMMON_FROM_DECK: {
				for(int unitId=0; unitId < MAX_UNITS; unitId++){
					Unit_t* unit = GetUnit(unitId);
					int targetWorldId = player->command.summonFromDeck.worldId;
					World_t* targetWorld = GetWorld(targetWorldId);
					if(!unit->active) continue;
					if(unit->playerId != playerId) continue;
					if(unit->worldId != -1) continue;
					if(unit->type != player->command.summonFromDeck.unitType) continue;
					if(unit->type == UNIT_KING) continue;

					int y = (targetWorldId == player->ownWorldId) ? 2 : targetWorld->height-3;
					int x = (rand()%targetWorld->width);
					if(!IsTileWalkable(targetWorldId, x, y, false)){
						for(int ix=0; ix < world->width; ix++){
							if(IsTileWalkable(targetWorldId, ix, y, false)){
								x = ix;
								break;
							}
						}
					}

					player->stats.unitsInDeckTotal--;
					player->stats.unitsInDeckCount[unit->type]--;
					unit->worldId = player->command.summonFromDeck.worldId;
					unit->pos[0] = x;
					unit->pos[1] = y;
					unit->action.type = UNIT_ACTION_WALKING;
					unit->action.walking.dest[0] = unit->pos[0];
					unit->action.walking.dest[1] = unit->pos[1]-2;
					SpawnParticles(EFFECT_BIG_RINGS, unit->worldId, (Vector2){unit->pos[0]*TILE_SIZE, unit->pos[1]*TILE_SIZE}, Vector2Zero(), 0);
					break;
				}
			} break;
			case COM_DEMOLISH: {
				int worldId = player->command.demolish.worldId;
				int buildingId = player->command.demolish.buildingId;
				World_t* targetWorld = GetWorld(worldId);
				Building_t* building = &targetWorld->buildings[buildingId];
				if( (!targetWorld->active) || (!building->active) ) break;
				if(building->playerId != playerId) break;

				if(building->type == BUILDING_CASTLE) break;
				if(building->type == BUILDING_BONFIRE) break;

				BuildingTakeDamage(worldId, buildingId, building->health);
				if( (player->selection == SEL_BUILDING)
				 && (player->selectedBuldingWorld == worldId)
				 && (player->selectedBuildingId == buildingId) ){
					
					player->selection = SEL_NONE;
				}
			} break;
			case COM_UNIT_UNFORTIFY: {
				Unit_t* unit = GetUnit(player->command.unitUnfortify.unitId);
				World_t* targetWorld = GetWorld(unit->worldId);
				Building_t* building = &targetWorld->buildings[unit->fortifiedBuildingId];

				if(unit->playerId != playerId) break;
				if(unit->fortifiedBuildingId == -1) break;
				unit->fortifiedBuildingId = -1;
				building->unitsFortified--;
				unit->action.type = UNIT_ACTION_WALKING;
				// if(building->pixmap){
				// }else{
					unit->action.walking.dest[0] = unit->pos[0] + ((rand()&1)*2-1);
					unit->action.walking.dest[1] = unit->pos[1] + ((rand()&1)*2-1);
				//}
			} break;
			case COM_DESELECT_UNIT: {
				if(player->selection != SEL_UNITS) break;
				if(player->selectedUnitCount == 1){
					player->selection = SEL_NONE;
					break;
				}
				for(int i=0; i < player->selectedUnitCount; i++){
					if(player->selectedUnitIds[i] == player->command.deselectUnit.unitId){
						int unitsAbove = (--player->selectedUnitCount) - i;
						if(unitsAbove){
							memmove(&player->selectedUnitIds[i], &player->selectedUnitIds[i+1], unitsAbove*sizeof(int));
						}
						break;
					}
				}
			} break;
			case COM_HIRE: {
				int targetWorldId = player->command.hire.worldId;
				int buildingId = player->command.hire.buildingId;
				World_t* targetWorld = GetWorld(targetWorldId);
				Building_t* building = &targetWorld->buildings[buildingId];
				UnitType_t* unitType = &UnitTypes[player->command.hire.unitType];

				if(player->stats.gold < unitType->priceGold) break;

				if((targetWorldId == -1) || (buildingId == -1)){
					targetWorldId = player->ownWorldId;
					targetWorld = GetWorld(targetWorldId);
					for(int newBuildingId=0; newBuildingId < MAX_BUILDINGS; newBuildingId++){
						building = &targetWorld->buildings[newBuildingId];
						if(!building->active) continue;
						if(building->playerId != playerId) continue;
						if(!MayBuildingHire(building->type, player->command.hire.unitType)) continue;

						buildingId = newBuildingId;
						break;
					}
					if(buildingId == -1) break;
				}else{
					if(!MayBuildingHire(building->type, player->command.hire.unitType)) break;
				}
				if((!world->active) || (!building->active)) break;
				if(!building->isBuilt) break;
				if(building->hiringTotal >= 100) break;

				player->stats.gold -= unitType->priceGold;
				building->hiringQueue[player->command.hire.unitType]++;
				building->hiringTotal++;
			} break;
			case COM_CANCEL_HIRE: {
				World_t* targetWorld = GetWorld(player->command.cancelHire.worldId);
				Building_t* building = &targetWorld->buildings[player->command.cancelHire.buildingId];
				UnitType_t* unitType = &UnitTypes[player->command.cancelHire.unitType];
				if((!targetWorld->active) || (!building->active)) break;
				if(!building->hiringQueue[player->command.cancelHire.unitType]) continue;
				
				player->stats.gold += unitType->priceGold;

				building->hiringQueue[player->command.cancelHire.unitType]--;
				building->hiringTotal--;
				building->hiringUnitId = -1;
			} break;
			case COM_RESEARCH: {
				int techId = player->command.research.techId;
				if(!MayPlayerResearch(playerId, techId)) break;

				player->stats.technologiesProgress[techId] = 1;
			} break;
			case COM_UNITS_DEFEND: {
				if(player->selection != SEL_UNITS) break;
				for(int i=0; i < player->selectedUnitCount; i++){
					Unit_t* unit = GetUnit(player->selectedUnitIds[i]);
					if((!unit->active) || (unit->playerId != playerId)) continue;
					if(unit->fortifiedBuildingId != -1) continue;

					unit->action.type = UNIT_ACTION_IDLE;
					unit->attentionTimer = 30*10;
				}
			} break;
			case COM_UNITS_DISBAND: {
				if(player->selection != SEL_UNITS) break;
				int disbandedCount = 0;
				for(int i=0; i < player->selectedUnitCount; i++){
					Unit_t* unit = GetUnit(player->selectedUnitIds[i]);
					if((!unit->active) || (unit->playerId != playerId)) continue;
					if(unit->fortifiedBuildingId != -1) continue;
					if(unit->type == UNIT_KING) continue;
					if(unit->type == UNIT_PEASANT) continue;
					if(unit->beingTransportedByUnitId != -1) continue;
					if(unit->transportingUnitId != -1) continue;
					
					unit->playerId = -1;
					unit->action.type = UNIT_ACTION_WHATEVER;
					disbandedCount++;
				}
				if(disbandedCount){
					player->selection = SEL_NONE;
				}
			} break;
			case COM_TOGGLE_AUTOATTACK: {
				int* autoAttack = &player->stats.autoAttackPlayer[player->command.toggleAutoAttack.otherPlayerId];
				*autoAttack = 1 - *autoAttack;
			} break;
			case COM_REVEAL_IDENTITY: {
				if(game.devilRevealed) break;
				if(playerId == game.devilPlayerId){
					RegisterDevilReveal();
				}
			} break;
		}
		player->command.type = player->command.type = COM_NONE;
	}
}
