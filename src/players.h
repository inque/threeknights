#pragma once
#include "buildings.h"
#include "capacities.h"
#include "raylib.h"
#include "units.h"
#include "buildings.h"
#include "playerbotlogic.h"

#define MAX_UNITS_PER_PLAYER (MAX_UNITS / MAX_PLAYERS)
#define TECH_RESEARCH_DURATION (90*10)
#define PEASANT_SPAWN_RATE (6*30)

typedef enum{
	COM_NONE,
	COM_BUILD,
	COM_SELECT,
	COM_UNITS_MOVE,
	COM_SUMMON_FROM_DECK,
	COM_DEMOLISH,
	COM_UNIT_UNFORTIFY,
	COM_DESELECT_UNIT,
	COM_HIRE,
	COM_CANCEL_HIRE,
	COM_RESEARCH,
	COM_UNITS_DEFEND,
	COM_UNITS_DISBAND,
	COM_TOGGLE_AUTOATTACK,
	COM_AI_MOVE_UNIT,
	COM_REVEAL_IDENTITY
} PlayerCommand_e;

typedef enum{
	SEL_NONE,
	SEL_BUILDING,
	SEL_UNITS
} PlayerSelectionTarget_e;

typedef enum{
	TECH_DEFENSIVE_TACTICS,
	TECH_IRON_ARMOR,
	TECH_ADVANCED_MAGIC,
	TECH_ADVANCED_TECHNOLOGY,
	_TECH_COUNT
} Technologies_e;

typedef struct{
	PlayerCommand_e type;
	union{
		struct{
			BuildingType_e buildingType;
			int worldId;
			int pos[2];
		} build;
		struct{
			int selectionBounds[4];
			int worldId;
		} select;
		struct{
			int worldId;
			int dest[2];
		} moveUnits;
		struct{
			int unitType;
			int worldId;
		} summonFromDeck;
		struct{
			int worldId;
			int buildingId;
		} demolish;
		struct{
			int unitId;
		} unitUnfortify;
		struct{
			int unitId;
		} deselectUnit;
		struct{
			int worldId;
			int buildingId;
			int unitType;
		} hire;
		struct{
			int worldId;
			int buildingId;
			int unitType;
		} cancelHire;
		struct{
			int techId;
		} research;
		struct{
			int otherPlayerId;
		} toggleAutoAttack;
		struct{
			int unitId;
			int softly;
			int worldId;
			int dest[2];
		} aiMoveUnit;
	};
} PlayerCommand_t;





typedef struct{
	int active;
	char name[64];
	Color color;
	int hasLost;
	int ownWorldId;
	PlayerCommand_t command;
	PlayerSelectionTarget_e selection;
	int selectedBuldingWorld;
	int selectedBuildingId;
	int selectedUnitCount;
	int selectedUnitIds[MAX_UNITS];
	struct{
		int gold;
		int food;
		int buildingCount; //under construction counted
		int buildingCountByType[_BUILDING_TYPE_COUNT];
		int buildingCountByTypeCountingUnbuilt[_BUILDING_TYPE_COUNT];
		int unitCount;
		int unitsInDeckTotal; 
		int unitsInDeckCount[_UNIT_TYPE_COUNT];
		int spawnPeasantTimer;
		int technologiesResearched[_TECH_COUNT];
		int technologiesProgress[_TECH_COUNT];
		int autoAttackPlayer[MAX_PLAYERS];
		int minionCount;
		int minionSpawnTimer;
	} stats;
	int isBot;
	PlayerBotState_t ai;
} Player_t;

int CreatePlayer(int isBot);
void ClearPlayerStats(int playerId);
void RemovePlayer(int playerId);
void RemoveBots();
void RegisterPlayerAttack(int srcPlayerId, int dstPlayerId);
void RegisterDevilReveal();
bool MayPlayerResearch(int playerId, int techId);
void UpdatePlayers();
