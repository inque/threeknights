#pragma once
#include "pixmaps.h"
#include "units.h"

#define CONSTRUCTION_SITE_HEALTH 15

typedef enum{
    BUILDING_CASTLE,
    BUILDING_HOUSE,
	BUILDING_MINE,
    BUILDING_BARRACKS,
    BUILDING_ACADEMY,
    BUILDING_WORKSHOP,
    BUILDING_OUTPOST,
    BUILDING_TOWER,
    BUILDING_ORACLE,
    BUILDING_FACTORY,
    BUILDING_BONFIRE,
	_BUILDING_TYPE_COUNT
} BuildingType_e;

typedef struct{
	char name[32];
    char description[128];
	Pixmap_t* pixmap;
	int singleTile; //(if pixmap is NULL)
	int priceGold;
	int providesFood;
	int fortificationLimit;
    int farmProductionDuration;
	int maxHealth;
	int buildEffort;
    int iconTileId;
    int isUnique;
    int attackCharge;
    int attackDamage;
    int attackRange;
} BuildingType_t;

typedef struct{
	int active;
	BuildingType_e type;
    Pixmap_t* pixmap;
	int size[2];
	int playerId;
	int pos[2];
	int health;
	int isBuilt;
	int buildProgress;
	int unitsFortified;
    int farmProductionTimer;
    int healthRestoreTimer;
    int hiringQueue[_UNIT_TYPE_COUNT];
    int hiringTotal;
    int hiringUnitId;
    int attackChargeTimer;
    int attackTargetUnitId;
    int lookForEnemiesTimer;
} Building_t;

extern const BuildingType_t BuildingTypes[_BUILDING_TYPE_COUNT];

bool CanBuildAtLocation(int worldId, int playerId, BuildingType_e type, int pos_x, int pos_y);
int CreateBuilding(int worldId, int playerId, BuildingType_e type, int pos_x, int pos_y, bool prebuilt);
void RemoveBuilding(int worldId, int buildingId);
void BuildingTakeDamage(int worldId, int buildingId, int dmg);
void BuildingConstruct(int worldId, int buildingId, int progress);
void UpdateBuildings();
void DrawBuildings();
