#pragma once
#include "stdbool.h"

#define PIXMAP_MAX_SIZE 32

typedef struct{
	int width, height;
	int tileSolid[PIXMAP_MAX_SIZE * PIXMAP_MAX_SIZE];
	int tileId[PIXMAP_MAX_SIZE * PIXMAP_MAX_SIZE];
} Pixmap_t;

inline int PixmapOffset(Pixmap_t* pixmap, int x, int y){
	return (x % pixmap->width) + (y * pixmap->width);
}

inline bool PixmapSolid(Pixmap_t* pixmap, int x, int y){
	return pixmap->tileSolid[PixmapOffset(pixmap, x, y)];
}

