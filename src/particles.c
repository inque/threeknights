#include "particles.h"
#include "gameapp.h"
#include "raylib.h"
#include "stdlib.h"

static Particle_t* AddParticle(ParticleSystemState_t* state, ParticleType_e type, Vector2 pos, float randomPosRadius, Vector2 minVelocity, Vector2 maxVelocity){
	int iterateTo = (state->maxActiveId+1);
	if(iterateTo >= (MAX_PARTICLES - 1)) iterateTo = (MAX_PARTICLES - 1);
	Particle_t* particle = &state->particles[0];
	for(int particleId = state->nextMinFreeId; particleId <= iterateTo; particleId++){
		particle = &state->particles[particleId];
		if(particle->type == PART_INACTIVE){
			if(state->maxActiveId < particleId) state->maxActiveId = particleId;
			if(state->nextMinFreeId == particleId) state->nextMinFreeId++;
			particle->type = type;
			particle->creationFrame = game.frame;
			if(randomPosRadius){
				particle->pos.x = pos.x + randomPosRadius * (0.5f - randf());
				particle->pos.y = pos.y + randomPosRadius * (0.5f - randf());
				particle->velocity.x = minVelocity.x + (maxVelocity.x - minVelocity.x) * randf();
				particle->velocity.y = minVelocity.y + (maxVelocity.y - minVelocity.y) * randf();
			}else{
				particle->pos = pos;
				particle->velocity.x = 0;
				particle->velocity.y = 0;
			}
		}
	}
	return particle;
}

static void RemoveParticle(ParticleSystemState_t* state, int particleId){
	if(state->maxActiveId == particleId){
		state->maxActiveId = 0;
		for(int i=particleId - 1; i >= 0; i--){
			if(state->particles[i].type != PART_INACTIVE){
				state->maxActiveId = i;
				break;
			}
		}
	}
	if(state->nextMinFreeId > particleId) state->nextMinFreeId = particleId;
	state->particles[particleId].type = PART_INACTIVE;
}

void PlayAudioEffect(AudioEffect_e type, int worldId, Vector2 pos){
	if(worldId == client.currentWorldId){
		Sound snd;
		float volume = 1;

		switch(type){
			case SFX_STEP:
				snd = client.assets.sfx_step[rand()%4];
				volume = 0.5f;
				break;
			case SFX_SWORD:
				snd = client.assets.sfx_sword[rand()%4];
				volume = 0.5f;
				break;
			case SFX_MAGIC:
				snd = client.assets.sfx_magic[rand()%4];
				volume = 0.5f;
				break;
			case SFX_ARROW:
				snd = client.assets.sfx_arrow[rand()%4];
				volume = 0.5f;
				break;
		}

		float dist = Vector2Distance(pos, client.cam.target);
		if((dist > 0) && (dist < 800.0f)){
			SetSoundVolume(snd, app.config.soundVolume * volume * (0.25f + 0.75f / dist));
			SetSoundPitch(snd, 1 + 0.025f * (pos.x - client.cam.target.x));
			PlaySound(snd);
		}
	}

}

void SpawnParticles(ParticleEffectType_e type, int worldId, Vector2 pos, Vector2 velocity, int extra){
	ParticleSystemState_t* state = &client.particles;
	Particle_t* particle;
	if((worldId == client.currentWorldId) || (worldId == -1)){
		switch(type){
			case EFFECT_ATTACK: {
				AddParticle(state, PART_MELEE_ATTACK, pos, 0.5f, Vector2Scale(velocity, 0.03f), Vector2Scale(velocity, 0.1f));
			} break;
			case EFFECT_DAMAGE: {
				for(int i=0; i < 1+(extra / 5)+(rand()&1); i++){
					AddParticle(state, PART_BLOOD, pos, TILE_SIZE, (Vector2){-20, -4}, (Vector2){20, 2});
				}
			} break;
			case EFFECT_ICON: {
				particle = AddParticle(state, PART_ICON, pos, 0.1f, (Vector2){-0.5f, -3.25f}, (Vector2){0.5f, -1});
				particle->tileId = extra;
			} break;
			case EFFECT_BUILDING_DAMAGE: {
				for(int i=0; i < 3+(extra/4)*2; i++){
					AddParticle(state, PART_DUSTCLOUD, pos, 20.0f, (Vector2){-3, -5}, (Vector2){3, -2});
				}
			} break;
			case EFFECT_ARROWS: {
				float angle = atan2f(velocity.x, -velocity.y);
				for(int i=0; i < 1+(extra / 5)+(rand()&1); i++){
					particle = AddParticle(state, PART_ARROW, pos, TILE_SIZE, Vector2Scale(velocity, 1.8f), Vector2Scale(velocity, 2.5f));
					particle->angle = angle * RAD2DEG;
				}
				AddParticle(state, PART_MELEE_ATTACK, pos, 0.5f, Vector2Scale(velocity, 0.03f), Vector2Scale(velocity, 0.1f));
			} break;
			case EFFECT_BIG_RINGS: {
				for(int i=0; i < 5; i++){
					AddParticle(state, PART_RING, pos, 20.0f, (Vector2){-5, -5}, (Vector2){5, 5});
				}
			} break;
            case EFFECT_CARDS: {
                for(int i=0; i < extra; i++){
                    AddParticle(state, PART_CARD, pos, 18.0f, (Vector2){-16, -18}, (Vector2){16, -3});
                }
            } break;
		}
	}
}

void UpdateParticles(){
	ParticleSystemState_t* state = &client.particles;
	for(int particleId=0; particleId <= state->maxActiveId; particleId++){
		Particle_t* particle = &state->particles[particleId];
		if(particle->type == PART_INACTIVE) continue;
		particle->pos = Vector2Add(particle->pos, Vector2Scale(particle->velocity, 25 * client.deltaTime));
		int lifespan = 10;
		int elapsed = (game.frame - particle->creationFrame);
		switch(particle->type){
			case PART_MELEE_ATTACK:
				particle->angle += 75.0f*client.deltaTime;
				break;
			case PART_BLOOD:
				lifespan = 4 + (particleId % 6)*2;
				particle->velocity.x *= 0.985f;
				particle->velocity.y *= 0.995f;
				particle->velocity.y += 10.0f * client.deltaTime;
				break;
			case PART_ICON:
				lifespan = 15;
				break;
			case PART_DUSTCLOUD:
				lifespan = 20;
				break;
			case PART_ARROW:
				lifespan = 10 + (particleId & 3);
				break;
			case PART_RING:
				lifespan = 20;
				break;
            case PART_CARD:
                lifespan = 90;
                particle->angle += 15.0f*client.deltaTime*( -3.0f*((particleId*3)%7) );
				particle->velocity.y *= 0.995f;
				particle->velocity.y += 12.0f * client.deltaTime;
                break;
		}
		if(elapsed > lifespan){
			RemoveParticle(state, particleId);
		}
	}
}

void DrawParticles(){
	ParticleSystemState_t* state = &client.particles;
	for(int particleId=0; particleId <= state->maxActiveId; particleId++){
		Particle_t* particle = &state->particles[particleId];
		if(particle->type == PART_INACTIVE) continue;
		int elapsed = (game.frame - particle->creationFrame);
		switch(particle->type){
			case PART_MELEE_ATTACK: {
				float fade = 1 - (elapsed / 10.0f);
				float size = (elapsed < 5) ? 2 : 2-powf(0.1f*elapsed, 2);
				DrawTileEx(377, particle->pos, particle->angle, size, (Color){255,255,255,(char)(fade*150)} );
			} break;
			case PART_BLOOD: {
				Vector2 endPoint = Vector2Subtract(particle->pos, Vector2Scale(particle->velocity, 3.5f));
				DrawLineEx(particle->pos, endPoint, 3.0f, (Color){247, 40, 43, 190});
			} break;
			case PART_ICON: {
				float fade = (elapsed < 5) ? 1 : (1.0f-((elapsed-5) / 10.0f));
				float size = fade * 1.2f;
				DrawTileEx(particle->tileId, (Vector2){particle->pos.x+0.5f*size, particle->pos.y+0.5f*size}, 0, size, Fade(BEIGE, fade) );
			} break;
			case PART_DUSTCLOUD: {
				float fade = (elapsed < 5) ? 1 : (1.0f-((elapsed-5) / 10.0f));
					  fade *= fade*fade;
				int size = 5+(particleId & 7)*2;
				DrawRectangle(particle->pos.x, particle->pos.y, size, size, Fade(GRAY, fade));
			} break;
			case PART_ARROW: {
				float fade = (elapsed < 5) ? 1 : (1.0f-((elapsed-5) / 13.0f));
					  fade *= fade*fade;
				int size = 0.5f + fade*0.25f + (particleId & 7)*0.2f;
				DrawTileEx(872, particle->pos, -45 + particle->angle + 5*fade, size, WHITE);
			} break;
			case PART_RING: {
				float fade = (elapsed / 20.0f);
				Color colors[] = {BLUE, LIGHTGRAY, SKYBLUE, DARKBLUE};
				float size = (float)((particleId+3) % 7) * (fade * fade) * 30.0f;
				DrawRectangleLinesEx((Rectangle){particle->pos.x-size*0.5f, particle->pos.y-size*0.5f, size, size}, 4, Fade(colors[particleId%4], 0.5f*(1-fade)));
			} break;
            case PART_CARD: {
                int col = (particleId*7)%12;
                int row = ((particleId<<2)+particleId*2)&3;
                float size = 1.8f+((particleId%3)*0.3f);
                Vector2 pos = particle->pos;
                pos.x += size*0.5f*sinf(particle->angle * DEG2RAD);
                pos.y += size*-0.5f*cosf(particle->angle * DEG2RAD);
                DrawTileEx( 532+row*32+col, particle->pos, particle->angle, size, WHITE);
            } break;
		}
	}
}

