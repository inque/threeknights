#include "world.h"
#include "gameapp.h"
#include "raylib.h"
#include "raymath.h"
#include "stdlib.h"
#include "stdio.h"
#define _USE_MATH_DEFINES
#include "math.h"
#include "stdbool.h"

bool IsBorderTile(World_t* world, int ix, int iy){
	return ((!ix) || (!iy) || (ix == (world->width-1)) || (iy == (world->height-1)));
}

bool IsGroundTypeWalkable(GroundTileType_e ground){
	switch(ground){
		case GND_ROCK:
		case GND_WATER:
		case GND_WORLD_BORDER:
			return false;
		default:
			return true;
	}
}

int GetBuildingOnTile(World_t* world, int x, int y, bool clear){
	for(int buildingId=0; buildingId < MAX_BUILDINGS; buildingId++){
		Building_t* building = &world->buildings[buildingId];
		if(!building->active) continue;
		if(building->pixmap){
			if((x < building->pos[0]) || (y < building->pos[1])) continue;
			if((x >= (building->pos[0]+building->pixmap->width)) || (y >= (building->pos[1]+building->pixmap->height))) continue;
			if(clear) return false;
			int offset = PixmapOffset(building->pixmap, x-building->pos[0], y-building->pos[1]);
			if(building->pixmap->tileSolid[offset]){
				return buildingId;
			}else{
				break;
			}
		}else{
			if((building->pos[0] == x) && (building->pos[1] == y)){
				return buildingId;
			}
		}
	}
	return -1;
}

int GetUnitOnTile(int worldId, int x, int y){
	for(int unitId=0; unitId < MAX_UNITS; unitId++){
		Unit_t* unit = GetUnit(unitId);
		if(!unit->active) continue;
		if(unit->worldId != worldId) continue;

		if((unit->pos[0] == x) && (unit->pos[1] == y)){
			return unitId;
		}
	}
	return -1;
}

bool IsTileWalkable(int worldId, int x, int y, bool clear){
	World_t* world = GetWorld(worldId);
	if(!TileInBounds(world, x, y)) return false;
	int tileOffset = TileOffset(world, x, y);
	GroundTileType_e ground = (GroundTileType_e)world->ground[tileOffset];
	if(!IsGroundTypeWalkable(ground)) return false;
	int buildingId = GetBuildingOnTile(world, x, y, clear);
	if(buildingId != -1) return false;
	int unitOnTileId = GetUnitOnTile(worldId, x, y);
	if(unitOnTileId != -1) return false;
	return true;
}

static void GenerateWorldMinimap(int worldId){
  #ifndef DEDICATED_SERVER
	World_t* world = GetWorld(worldId);
	Color pixels[MAX_WORLD_SIZE * MAX_WORLD_SIZE];
	for(int iy=0; iy < world->height; iy++)
	for(int ix=0; ix < world->width; ix++){
		int worldOffset = TileOffset(world, ix, iy);
		int pixelOffset = (ix % world->width) + (iy * world->width);
		switch(world->ground[worldOffset]){
			case GND_EMPTY:
				pixels[pixelOffset] = BROWN;
				break;
			case GND_GRASS:
				pixels[pixelOffset] = DARKGREEN;
				break;
			case GND_TREE:
				//pixels[pixelOffset] = GREEN;
				pixels[pixelOffset] = DARKGREEN;
				break;
			case GND_WATER:
				for(int i=0; i < 3; i++)
					pixels[pixelOffset+i-1] = BLUE;
				break;
			case GND_WORLD_BORDER:
				pixels[pixelOffset] = BLACK;
				break;
			default:
				pixels[pixelOffset] = DARKBROWN;
				break;
		}
	}
	Image img = {
		.format = UNCOMPRESSED_R8G8B8A8,
		.width = world->width,
		.height = world->height,
		.mipmaps = 1,
		.data = &pixels
	};
	client.worldMinimaps[worldId] = LoadTextureFromImage(img);
  #endif
}

int GenerateWorld(int playerId){
	for(int worldId=0; worldId < MAX_WORLDS; worldId++){
		World_t* world = GetWorld(worldId);
		if(world->active) continue;

		world->active = 1;
		world->width = 50;
		world->height = 70;

		for(int iy=0; iy < world->height; iy++)
		for(int ix=0; ix < world->width; ix++){
			int tileOffset = TileOffset(world, ix, iy);
			if(IsBorderTile(world, ix, iy)){
				continue;
			}

			//ground
			int val = (rand() % (4+18)) - 18;
			if(val < 0) val = 0;
			world->ground[tileOffset] = val;

			if(!(rand()&31)) world->ground[tileOffset] = GND_ROCK;
		}

		//grow forest
		for(int iy=0; iy < world->height; iy++)
		for(int ix=0; ix < world->width; ix++){
			int tileOffset = TileOffset(world, ix, iy);

			if(world->ground[tileOffset] == GND_TREE){
				if(rand()&1) continue;
				int neighbour = TileOffset(world, ix+((rand()&1)*2-1), iy+1);
				world->ground[neighbour] = GND_TREE;
			}else if(world->ground[tileOffset] == GND_ROCK){
				if(rand()&3) continue;
				int neighbour = TileOffset(world, ix+1, iy+((rand()&1)*2-1));
				world->ground[neighbour] = GND_ROCK;
			}
		}

		//rivers
		for(int river=0; river < 3; river++){
			float angle = randf() * 2 * 3.14f;
			float length = randf() * 100 + 4;
			Vector2 center = {randf()*world->width, randf()*world->height};
			Vector2 riverStart = {center.x+length*sinf(angle), center.y+length*cosf(angle)};
			Vector2 riverEnd   = {center.x-length*sinf(angle), center.y-length*cosf(angle)};
			int pointer[] = {(int)riverStart.x, (int)riverStart.y};
			int dest[] = {(int)riverEnd.x, (int)riverEnd.y};
			while(true){
				bool doTurn = ((rand() & 3) == 0);
				bool doFork = ((rand() & 3) == 0);
				int dist = abs(dest[0]-pointer[0])+abs(dest[1]-pointer[1]);
				if(dist > 15){
					dest[rand()&1] += ((rand()&1)*2-1)*3;
				}
				if(TileInBounds(world, pointer[0], pointer[1])){
					int tileOffset = TileOffset(world, pointer[0], pointer[1]);
					world->ground[tileOffset] = GND_WATER;
				}
				if(pointer[0] < dest[0]){
					if(doTurn) pointer[0]++;
				}else if(pointer[0] > dest[0]){
					if(doTurn) pointer[0]--;
				}
				if(pointer[1] < dest[1]){
					if(doTurn) pointer[1]++;
				}else if(pointer[1] > dest[1]){
					if(doTurn) pointer[1]--;
				}else if(pointer[0] == dest[0]){
					break;
				}
				if(doFork){
					int pointer2[] = {pointer[0], pointer[1]};
					for(int i=0; i < 20; i++){
						if(rand()&1){
							if(rand()&1){
								pointer2[0]++;
							}else{
								pointer2[0]--;
							}
						}else{
							if(rand()&1){
								pointer2[1]++;
							}else{
								pointer2[1]--;
							}
						}
						if(TileInBounds(world, pointer2[0], pointer2[1])){
							int tileOffset = TileOffset(world, pointer2[0], pointer2[1]);
							world->ground[tileOffset] = GND_WATER;
						}
						if((rand()&3)==0) break;
					}
				}
			}
		}

		int castlePos[] = {world->width/2, world->height/4};
		CreateBuilding(worldId, playerId, BUILDING_CASTLE, castlePos[0], castlePos[1], true);
		CreateBuilding(worldId, playerId, BUILDING_BONFIRE, castlePos[0]-4, castlePos[1]+1, true);
		SpawnUnit(UNIT_KING, playerId, worldId, castlePos[0]+8, castlePos[1]+2);
		SpawnUnit(UNIT_WARRIOR, playerId, worldId, castlePos[0]+2, castlePos[1]+7);
		SpawnUnit(UNIT_WARRIOR, playerId, worldId, castlePos[0]+4, castlePos[1]+8);
		//SpawnUnit(UNIT_MAGE, playerId, worldId, castlePos[0]+7, castlePos[1]+4);

		//post-cleanup
		for(int iy=0; iy < world->height; iy++)
		for(int ix=0; ix < world->width; ix++){
			int tileOffset = TileOffset(world, ix, iy);
			
			if(iy==1){
				world->ground[tileOffset] = GND_ZONE_TRADE_FOOD;
			}else if(iy==(world->height-3)){
				world->ground[tileOffset] = GND_ZONE_TRADE_ORE;
			}else if(iy==(world->height-2)){
				world->ground[tileOffset] = GND_ZONE_LEAVE;
			}

			if(IsBorderTile(world, ix, iy)){
				world->ground[tileOffset] = GND_WORLD_BORDER;	
				continue;
			}

			float distToCastle = Vector2Distance((Vector2){ix,iy}, (Vector2){castlePos[0],castlePos[1]});
			if((distToCastle < 12) || (iy > (world->height - 16)) || (iy < 14)){
				if(!IsTileWalkable(worldId, ix, iy, false)){
					world->ground[tileOffset] = GND_EMPTY;
				}
			}
		}

		GenerateWorldMinimap(worldId);

		return worldId;
	}
	return -1;
}

void DrawWorld(){
	World_t* world = GetWorld(client.currentWorldId);
	Color bgColor = {71, 45, 60, 255};
	DrawRectangle(0, 0, world->width*TILE_SIZE, world->height*TILE_SIZE, bgColor);
	//DrawRectangleGradientV(0, -50, world->width*TILE_SIZE, 50, BLACK, bgColor);
	DrawRectangleGradientV(0, 0, world->width*TILE_SIZE, 50, DARKGRAY, bgColor);
	DrawRectangleGradientV(0, world->height*TILE_SIZE, world->width*TILE_SIZE, 100, bgColor, BLACK);

	for(int iy=client.viewBounds[1]; iy < client.viewBounds[1]+client.viewBounds[3]; iy++)
	for(int ix=client.viewBounds[0]; ix < client.viewBounds[0]+client.viewBounds[2]; ix++){
		if(!TileInBounds(world, ix, iy)) continue;
		int tileOffset = TileOffset(world, ix, iy);
		int actualTileId;
		//int randomSeed = ((ix << 5) + ((iy+5) << 3)) & (16-1);
		int randomSeed = ((ix*5 & 439243) + (iy*7 & 352853) + ((ix*2+iy) >> 1)) & 15;
		switch(world->ground[tileOffset]){
			case GND_EMPTY:
				actualTileId = 0;
				break;
			case GND_DIRT:
				actualTileId = 1 + (randomSeed & 3);
				break;
			case GND_GRASS:{
				int grass = (randomSeed & 3);
				actualTileId = (grass == 3) ? 64 : 5+grass;
				break;
			}case GND_TREE:
				actualTileId = 32 + (randomSeed % 6);
				break;
			case GND_ROCK:
				if(randomSeed&3){
					actualTileId = 69;
				}else{
					actualTileId = 416;
				}
				break;
			case GND_FARMLAND:
				actualTileId = 205 + (randomSeed % 5);
				break;
			case GND_WATER:
				actualTileId = 168;
				break;
			case GND_GRAVE:
				actualTileId = ((randomSeed & 1) ? 449 : 480) + ((randomSeed & 2) >> 1);
				break;
			case GND_RUBBLE:{
				int rubble[] = {353, 400, 85, 2, 102, 178, 332, 96};
				actualTileId = rubble[randomSeed & 7];
				break;
			}default:
				actualTileId = 0;
				break;
		}
		char* highlight = &client.worldCellHighlight[ix][iy];
		if(*highlight){
			//DrawRectangle(ix*TILE_SIZE, iy*TILE_SIZE, TILE_SIZE, TILE_SIZE, Fade(ORANGE, 0.25f * *highlight / 100.0f));
			float fade = (*highlight / 255.0f);
			DrawRectanglePro((Rectangle){ix*TILE_SIZE, iy*TILE_SIZE, TILE_SIZE, TILE_SIZE}, (Vector2){5*fade, 10*fade}, 10*fade, Fade(ORANGE, fade));

			// if((*highlight > 100) || (!(rand()&31))){
			// 	int bang = *highlight / 6;
			// 	if((!(rand()&1)) && ix) client.worldCellHighlight[ix-1][iy] += bang;
			// 	if((!(rand()&1)) && iy) client.worldCellHighlight[ix][iy-1] += bang;
			// 	if((!(rand()&1)) && (ix < world->width)) client.worldCellHighlight[ix+1][iy] += bang;
			// 	if((!(rand()&1)) && (iy < world->height)) client.worldCellHighlight[ix][iy+1] += bang;
			// 	if((!(rand()&3)) && ix && iy) client.worldCellHighlight[ix-1][iy-1] += bang;
			// 	if((!(rand()&3)) && ix && (iy < world->height)) client.worldCellHighlight[ix-1][iy+1] += bang;
			// 	if((!(rand()&3)) && (ix < world->width) && iy) client.worldCellHighlight[ix+1][iy-1] += bang;
			// 	if((!(rand()&3)) && (ix < world->width) && (iy < world->height)) client.worldCellHighlight[ix+1][iy+1] += bang;
			// }
			if(*highlight > 70) *highlight -= 2;
			*highlight -= 1;
		}
		if(actualTileId){
			DrawTile(actualTileId, ix, iy);
		}
	}

	float scale = 2 - 1.0f / client.cam.zoom;
	Rectangle titleBox = {0.5f*world->width*TILE_SIZE - 30*scale, -65*scale, 100*scale, 20*scale};
	DrawRectangleRec(titleBox, DARKGRAY);
	DrawText("The highlands", titleBox.x + 20*scale, titleBox.y + 4*scale, 10*scale, BLACK);

	titleBox.y = world->height*TILE_SIZE + 20;
	DrawRectangleRec(titleBox, DARKGRAY);
	DrawText("To other lands", titleBox.x + 15*scale, titleBox.y + 4*scale, 10*scale, BLACK);

}

bool RectInView(int x, int y, int width, int height){
	return ((x >= client.viewBounds[0]) && (y >= client.viewBounds[1])
			&& ((x+width)  <= (client.viewBounds[0] + client.viewBounds[2])
			&& ((y+height) <= (client.viewBounds[1] + client.viewBounds[3]))));
}