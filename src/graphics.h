#pragma once
#include "raylib.h"
#include "pixmaps.h"

#define INT2VEC(a) (Vector2){(float)a[0], (float)a[1]}

void DrawTile(int id, int pos_x, int pos_y);
void DrawTilePro(int id, int pos_x, int pos_y, int rotate, bool flip_h, bool flip_v, Color tint);
void DrawTileEx(int id, Vector2 pos, float angle, float size, Color tint);
void DrawPixmap(Pixmap_t* pixmap, int pos_x, int pos_y);
void DrawPixmapEx(Pixmap_t* pixmap, Vector2 pos, Color tint);
Rectangle TilemapSourceRect(int id);

