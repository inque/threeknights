#include "pathfinding.h"
#include "gameapp.h"
#include "stdlib.h"

bool BuildUnitPath(int unitId, int goal[2]){
	Unit_t* unit = GetUnit(unitId);
	World_t* world = GetWorld(unit->worldId);
    int goalBuildingId = GetBuildingOnTile(world, goal[0], goal[1], true);
	char visited[MAX_WORLD_SIZE][MAX_WORLD_SIZE] = {0};
	int lookups = 0, checks = 0;
	if((!unit->active) || (unit->worldId == -1)) return false;
	
	unit->pathLength = 0;
	unit->pathProgress = 0;
	unit->path[0].x = unit->pos[0];
	unit->path[0].y = unit->pos[1];
	for(int i=0; i < MAX_PATHFIND_DEPTH; i++){
		PathNode_t* node = &unit->path[i];
		PathNode_t* nextNode = &unit->path[i+1];
		int bestNextDist = -1;
		int bestNextPos[2];
        GroundTileType_e currentGround = world->ground[TileOffset(world, node->x, node->y)];
		for(int j=0; j < 4; j++){
			int nextPos[2];
			nextPos[0] = node->x + ( ((j&2)>>1) ? 0 : ((j&1) ? -1 : 1 ) );
			nextPos[1] = node->y + ( ((j&2)>>1) ? ((j&1) ? -1 : 1 ) : 0 );

			checks++;
			if( (nextPos[0] < 0) || (nextPos[1] < 0) ) continue;
			if( (nextPos[0] >= world->width) || (nextPos[1] >= world->height) ) continue;
			if(visited[nextPos[0]][nextPos[1]]) continue;

			int dist = abs(goal[0]-nextPos[0])+abs(goal[1]-nextPos[1]);
                dist *= 2; //???
			if(dist){
				lookups++;
				GroundTileType_e ground = world->ground[TileOffset(world, nextPos[0], nextPos[1])];
				if((ground == GND_WATER) && (currentGround != GND_WATER)){
					dist += 3; //???
				}else if(!IsGroundTypeWalkable(ground)){
					continue;
				}
				int buildingOnTile = GetBuildingOnTile(world, nextPos[0], nextPos[1], false);
				if( (buildingOnTile != -1) && (goalBuildingId != buildingOnTile) ) continue;
			}

			bool diagonalCorrection = (dist == bestNextDist) && (i&3);
			if((bestNextDist == -1) || (dist < bestNextDist) || diagonalCorrection){
			//if((bestNextDist == -1) || (dist < bestNextDist)){
				bestNextPos[0] = nextPos[0];
				bestNextPos[1] = nextPos[1];
				bestNextDist = dist;
			}
		}
		bool tooManyChecks = (checks >= MAX_PATHFIND_CHECKS) || (lookups >= MAX_PATHFIND_LOOKUPS);
		if(bestNextDist == -1){
			if((i > 0) && (!tooManyChecks)){
				//backtrack
				i--;
				continue;
			}else{
				//fail out
				unit->pathLength = 0;
				return false;
			}
		}
		nextNode->x = bestNextPos[0];
		nextNode->y = bestNextPos[1];
		visited[nextNode->x][nextNode->y] = 1;
		if((bestNextDist == 0) || (i >= MAX_PATHFIND_DEPTH-1) || tooManyChecks){
			unit->pathLength = i+1;
			return true;
		}
	}
	return false;
}
