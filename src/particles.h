#pragma once
#include "raylib.h"

#define MAX_PARTICLES 1024

typedef enum{
    EFFECT_ATTACK,
    EFFECT_DAMAGE,
    EFFECT_ICON,
    EFFECT_BUILDING_DAMAGE,
    EFFECT_ARROWS,
    EFFECT_BIG_RINGS,
    EFFECT_CARDS
} ParticleEffectType_e;

typedef enum{
    PART_INACTIVE,
    PART_MELEE_ATTACK,
    PART_BLOOD,
    PART_ICON,
    PART_DUSTCLOUD,
    PART_ARROW,
    PART_RING,
    PART_CARD
} ParticleType_e;

typedef struct{
    ParticleType_e type;
    int creationFrame;
    Vector2 pos;
    Vector2 velocity;
    union{
        float angle;
        int tileId;
        Color color;
    };
} Particle_t;

typedef struct{
    int maxActiveId, nextMinFreeId;
    Particle_t particles[MAX_PARTICLES];
} ParticleSystemState_t;

typedef enum{
    SFX_STEP,
    SFX_SWORD,
    SFX_MAGIC,
    SFX_ARROW
} AudioEffect_e;

void PlayAudioEffect(AudioEffect_e type, int worldId, Vector2 pos);
void SpawnParticles(ParticleEffectType_e type, int worldId, Vector2 pos, Vector2 velocity, int extra);
void UpdateParticles();
void DrawParticles();
