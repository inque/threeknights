#include "utils.h"
#include "stdlib.h"
#include "time.h"
#ifdef _WIN32
	#include "windows.h"
#else
	#include "sys/stat.h"
	#include "utime.h"
	#include "unistd.h"
#endif


int mod2(int a, int b){
	int r = a % b;
	return r < 0 ? r + b : r;
}

int Millisecs(){
	#ifdef _WIN32
		return (int)clock();
	#else
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		static unsigned long long int baseTime;
		unsigned long long int time = (unsigned long long int)ts.tv_sec*1000000000LLU + (unsigned long long int)ts.tv_nsec;
		if(!baseTime){
			baseTime = time;
			return 0;
		}else{
			return (int)((time - baseTime)*1e-9 * 1000);  // Elapsed time since InitTimer()
		}
	#endif
}

void Delay(int ms){
	#ifdef _WIN32
		Sleep(ms);
	#elif defined(__EMSCRIPTEN__)
		return;
	#elif defined(__linux__)
		struct timespec req = { 0 };
		time_t sec = (int)(ms/1000.0f);
		ms -= (float)(sec*1000);
		req.tv_sec = sec;
		req.tv_nsec = ms*1000000L;

		while (nanosleep(&req, &req) == -1) continue;
	#elif defined(__APPLE__)
		usleep(ms*1000.0f);
	#endif
}

FileModTime_t FileModificationTime(const char* path){
	#ifdef _WIN32
		HANDLE fh = CreateFile(path, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
		FILETIME modtime;
		int res = GetFileTime(fh, NULL, NULL, &modtime);
		CloseHandle(fh);
		if(!res) return -1;
		return (FileModTime_t)(modtime.dwLowDateTime + ((FileModTime_t)modtime.dwHighDateTime << 32));
	#elif defined(__EMSCRIPTEN__)
		return 0;
	#else
		struct stat foo;
		if(stat(path, &foo) < 0) return -1;
		return (FileModTime_t)foo.st_mtime;
	#endif
}
