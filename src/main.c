#include "gameapp.h"
#include "raylib.h"
#include "string.h"
#include "stdio.h"

static void ParseArgs(int argc, char** argv){
	if(argc <= 1) return;
	for(int i = 1; i < argc; i++){
		char* arg = argv[i];
		arg += (*arg == '-');
		arg += (*arg == '-');
		switch(arg[0]){
			case 'c':
				i++;
				strncpy(app.configPath, argv[i], sizeof(app.configPath));
				break;
			case 'h':
				printf("Command line options: \n  --c [config path]\n\n");
				break;
		}
	}
}

int main(int argc, char** argv){
	game.initializedOnce = 0;
	ParseArgs(argc, argv);
	InitializeApp();
    if(LoadAssets()){
        printf("Couldn't load some assets.\n");
        return 1;
    }
	MainLoop();
	return 0;
}
