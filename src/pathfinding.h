#pragma once
#include "stdbool.h"

#define MAX_PATHFIND_DEPTH 128
#define MAX_PATHFIND_LOOKUPS 150
#define MAX_PATHFIND_CHECKS 750

#define PATHFIND_DELAY 5

typedef struct{
    short x, y;
} PathNode_t;

bool BuildUnitPath(int unitId, int goal[2]);
