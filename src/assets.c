#include "assets.h"
#include "gameapp.h"
#include "stdint.h"
#include "stdlib.h"

#ifndef DEDICATED_SERVER

static inline uint32_t* GetPixelPtr(Color* pixels, int width, int x, int y){
	return (uint32_t*)(&pixels[y*width + x]);
}

static Texture2D LoadMaskedTexture(const char* path){
	const uint32_t ALPHA = (0xFF << 24);
	const uint32_t COLOR = (0x01 << 24) - 1;

	Image img = LoadImage(path);
	uint32_t bgColor = ((uint32_t*)(img.data))[0] | ALPHA;
	Color* pixels;
	Color* pixelsOriginal;
	int originalWidth = img.width;
    pixels = GetImageData(img);
	for(int iy=0; iy < img.height; iy++)
	for(int ix=0; ix < originalWidth; ix++){
		uint32_t* dst = GetPixelPtr(pixels, img.width, ix, iy);
		if(*dst == bgColor){
			*dst &= COLOR;
		}
	}
	RL_FREE(img.data);
	img.data = pixels;
	img.format = UNCOMPRESSED_R8G8B8A8;
	img.mipmaps = 1;
	Texture2D tex = LoadTextureFromImage(img);
	
	UnloadImage(img);
	return tex;
}

bool LoadAssets(){
    client.assets.tilemap = LoadMaskedTexture("data/tileset.png");
    client.assets.sfx_step[0] = LoadSound("data/step1.ogg");
    client.assets.sfx_step[1] = LoadSound("data/step2.ogg");
    client.assets.sfx_step[2] = LoadSound("data/step3.ogg");
    client.assets.sfx_step[3] = LoadSound("data/step4.ogg");
    client.assets.sfx_sword[0] = LoadSound("data/sword1.ogg");
    client.assets.sfx_sword[1] = LoadSound("data/sword1.ogg");
    client.assets.sfx_sword[2] = LoadSound("data/sword1.ogg");
    client.assets.sfx_sword[3] = LoadSound("data/sword1.ogg");
    client.assets.sfx_magic[4] = LoadSound("data/magic1.ogg");
    client.assets.sfx_magic[1] = LoadSound("data/magic2.ogg");
    client.assets.sfx_magic[2] = LoadSound("data/magic3.ogg");
    client.assets.sfx_magic[3] = LoadSound("data/magic4.ogg");
    client.assets.sfx_arrow[0] = LoadSound("data/arrow1.ogg");
    client.assets.sfx_arrow[1] = LoadSound("data/arrow2.ogg");
    client.assets.sfx_arrow[2] = LoadSound("data/arrow3.ogg");
    client.assets.sfx_arrow[3] = LoadSound("data/arrow4.ogg");
    
    return false;
}

#else

bool LoadAssets(){
    return false;
}

#endif
